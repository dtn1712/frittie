Frittie
=======

Built, designed and created by: Dang Nguyen and Duc Vu<br>
Currently, the app is still in construction <br><br>


<h3> Quick Setup and Running in Linux (Ubuntu/Mint) </h3>
<ul>

	<li>Clone the project at https://github.com/dtn1712/Frittie.git</li>
	<br>
	<li>Install python, pip django, virtualenv, orbited:<br>
		<code>sudo apt-get install python && sudo apt-get install pip</code><br>
		<code>sudo pip install Django virtualenv orbited</code>
	</li>
	<br>
	<li>Create new virtualenv <br> 
		<code>virtualenv venv-test --distribute --no-site-packages</code>
	</li>
	<br>
	<li>Activate the virtualenv <br>
		<code>source venv-test/bin/activate</code>
		<br> Deactivate by <br>
		<code>deactivate</code>	
	</li>
	<br>
	<li>
		Install all the dependency listed in requirements.txt by using pip install
	</li>
	<br>
	<li>
		<code>python manage.py runserver</code> to run the main app
	</li>
	<br>
	<li>
		View the app in http://localhost:8000/
	</li>
</ul>