STAGE = 'dev'

WEBSITE_HOMEPAGE = "http://localhost:8000/"

DEBUG = True
TEMPLATE_DEBUG = DEBUG

USE_TZ = True

DATABASES = {
   'default': {
       'ENGINE': 'django.db.backends.sqlite3', 
       'NAME':  os.path.join(ROOT_PATH, "db/dev/frittie.db"),                    
       'USER': '',                     
       'PASSWORD': '',                  
       'HOST': '',                      
       'PORT': '',                     
   }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

# See: http://docs.celeryq.org/en/latest/configuration.html#celery-always-eager
CELERY_ALWAYS_EAGER = True

# See: http://docs.celeryproject.org/en/latest/configuration.html#celery-eager-propagates-exceptions
CELERY_EAGER_PROPAGATES_EXCEPTIONS = True

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
        'PATH': os.path.join(ROOT_PATH, 'db/dev/whoosh_index'),
        'INCLUDE_SPELLING': False,
    }
}

INSTALLED_APPS += (
    'debug_toolbar',
    'profiler'
)

INTERNAL_IPS = ('127.0.0.1',)

DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
}

MIDDLEWARE_CLASSES = (
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.gzip.GZipMiddleware',
    'htmlmin.middleware.HtmlMinifyMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django_mobile.middleware.MobileDetectionMiddleware',
    'django_mobile.middleware.SetFlavourMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'easy_timezones.middleware.EasyTimezoneMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    "profiler.middleware.ProfileMiddleware",
)

FACEBOOK_APP_ID = '544062939007678'
FACEBOOK_APP_SECRET = '2135474ee972466d2709f145c028994d'

STATIC_URL = '/static/'

BROKER_URL = 'redis://localhost:6379/0'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/0'

THUMBNAIL_DEBUG = True
