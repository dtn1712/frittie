import os
import djcelery  
djcelery.setup_loader()

from datetime import timedelta
from os.path import abspath, basename, dirname, join, normpath
from os import environ
from sys import path

from frittie.apps.app_settings import KEYWORDS_URL
from django.utils.translation import ugettext_lazy as _

ROOT_PATH = os.path.dirname(__file__)
path.append(ROOT_PATH)

SITE_NAME = os.path.basename(ROOT_PATH)
SITE_DOMAIN = SITE_NAME + ".com"

ADMINS = (
    ('Dang Nguyen', 'dangnguyen_1712@yahoo.com'),
    ("Duc Vu", "vthanhduc92@gmail.com"),
)

ADMIN_USERNAMES = (
    "dtn29",
    "dtn1712",
    "dtv26",
    "itamsvtd",
)

MANAGERS = ADMINS

# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
# EMAIL_HOST = 'smtp.gmail.com'

#MANDRILL_API_KEY = environ.get("MANDRILL_API_KEY") 
MANDRILL_API_KEY = "oaJXLVqsZPL4IYjftdtemA"
SERVER_EMAIL=environ.get('SERVER_EMAIL')

EMAIL_BACKEND = "djrill.mail.backends.djrill.DjrillBackend"
EMAIL_HOST = 'smtp.mandrillapp.com'
EMAIL_HOST_USER = environ.get('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = environ.get('EMAIL_HOST_PASSWORD')

EMAIL_PORT = 587
EMAIL_USE_TLS = True


# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = None

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGES = (
    ('vn', _('Vietnamese')),
    ('en', _('English')),
)

EXTRA_LANG_INFO = {
    'vn': {
        'bidi': False, # left-to-right
        'code': 'vn',
        'name': 'Vietnamese',
        'name_local': u'Vietnamese', #unicode codepoints here
    },
}

import django.conf.locale
from django.conf import global_settings

LANG_INFO = dict(django.conf.locale.LANG_INFO.items() + EXTRA_LANG_INFO.items())
django.conf.locale.LANG_INFO = LANG_INFO

# Languages using BiDi (right-to-left) layout
global_settings.LANGUAGES = global_settings.LANGUAGES + (("vn",'Vietnamese'))



SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = False

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(ROOT_PATH, 'assets/media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = "/media/"


STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'dajaxice.finders.DajaxiceFinder',
    # 'compressor.finders.CompressorFinder',
    # 'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = '2!(mk54t367e4(9_)z_69%we=7ric+=5%am8hw=g(49jdxx6=s'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django_mobile.loader.Loader',
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.csrf',
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages',
    "django.core.context_processors.request",
    'django_mobile.context_processors.flavour',
    'allauth.account.context_processors.account',
    "allauth.socialaccount.context_processors.socialaccount",
    "frittie.apps.main.context_processors.site_data",
    "frittie.apps.main.context_processors.global_data",
)

ROOT_URLCONF = '%s.urls' % SITE_NAME

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = '%s.wsgi.application' % SITE_NAME

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(ROOT_PATH, "assets/templates"),
    os.path.join(ROOT_PATH, "assets/templates/apps"),
    os.path.join(ROOT_PATH, "assets/templates/apps/auth"),
)

FIXTURE_DIRS = (
    os.path.join(ROOT_PATH, "fixtures"),
)

########## APP CONFIGURATION
DJANGO_APPS = (
    # Default Django apps:
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Useful template tags:
    'django.contrib.humanize',

    # Admin panel and documentation:
    'django.contrib.admin',
    'django.contrib.admindocs',
)


THIRD_PARTY_APPS = (
    # Allauth app
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.facebook',

    # Other app
    #"compressor",
    'easy_timezones',
    "cities_light",
    'django_mobile',
    'djcelery',
    'tastypie',
    'haystack',
    'dajaxice',
    'dajax',
    "kombu",
    'kombu.transport.django', 
    "gunicorn",
    'uni_form',
    #'user_streams.backends.user_streams_single_table_backend',
    #'user_streams.backends.user_streams_redis_backend',
    #'django_facebook',
    "south",
    "firebase_token_generator",
    "validate_email",
    "icalendar",
    "facebook",
    "djrill",
    'sorl.thumbnail'
)

LOCALE_PATH_DIR = os.path.abspath(__file__ + "/../../")

LOCALE_PATHS = (
    os.path.join(LOCALE_PATH_DIR, 'locale'),
)
#LOCALE_PATHS = (os.path.join(os.path.dirname(__file__), '../locale/')),

LOCAL_APPS = (
    'frittie.apps',
    'frittie.apps.main',
    'frittie.apps.about',
    'frittie.apps.auth',
    'frittie.apps.member',
    'frittie.apps.location',
    'frittie.apps.activity',
    'frittie.apps.notification',
    'frittie.apps.photo',
    'frittie.apps.message',
    "frittie.apps.search",
    #'frittie.apps.feed',
    'frittie.apps.watchlist',
    'frittie.apps.explore',
    "frittie.apps.friend",
    "frittie.apps.mailer",
    'frittie.apps.ticket',
)

INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console':{
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'logfile_frittie': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename':  os.path.join(ROOT_PATH, "logs/frittie.log"),
        }, 
        'logfile_dajaxice': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename':  os.path.join(ROOT_PATH, "logs/dajaxice.log"),
        }, 
        'logfile_facebook': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename':  os.path.join(ROOT_PATH, "logs/django_facebook.log"),
        },
        'logfile_tastypie': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename':  os.path.join(ROOT_PATH, "logs/django_tastypie.log"),
        }

    },
    'loggers': {
        'django': {
            'handlers': ['null'],
            'propagate': True,
            'level': 'INFO',
        },
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': False,
        },
        'dajaxice': {
            'handlers': ['logfile_dajaxice'],
            'level': 'ERROR',
        },
        'django_facebook':{
            'handlers': ['logfile_facebook'],
            'level': 'DEBUG',
        },
        'tastypie':{
            'handlers': ['logfile_tastypie'],
            'level': 'DEBUG',
        },
        'frittie': {
            'handlers': ['logfile_frittie'],
            'level': 'ERROR',
        },
    }, 
}


########## CELERY CONFIGURATION
CELERY_TASK_RESULT_EXPIRES = timedelta(minutes=30)

# See: http://docs.celeryproject.org/en/master/configuration.html#std:setting-CELERY_CHORD_PROPAGATES
CELERY_CHORD_PROPAGATES = True



AUTH_PROFILE_MODULE = 'main.UserProfile'

SERIALIZATION_MODULES = {
    'json': "django.core.serializers.json",
}

AUTHENTICATION_BACKENDS = (
    'allauth.account.auth_backends.AuthenticationBackend',
    'django.contrib.auth.backends.ModelBackend', 
)

ACCOUNT_ACTIVATION_DAYS = 7
ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_PASSWORD_MIN_LENGTH = 4
ACCOUNT_EMAIL_VERIFICATION = "optional"


HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'
HAYSTACK_DEFAULT_OPERATOR = 'OR'

LOGIN_URL = '/accounts/login'
LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'

ACCOUNT_LOGOUT_ON_GET = True
ACCOUNT_CONFIRM_EMAIL_ON_GET = True
ACCOUNT_EMAIL_CONFIRMATION_AUTHENTICATED_REDIRECT_URL = "/?action=confirm_email&result=success"
ACCOUNT_USERNAME_BLACKLIST = KEYWORDS_URL
ACCOUNT_EMAIL_SUBJECT_PREFIX = "[Frittie]"
ACCOUNT_ADAPTER = "frittie.apps.auth.adapters.CustomAccountAdapter"

SOCIALACCOUNT_PROVIDERS = {
    'facebook': {
        'SCOPE': ['email', 'publish_stream',"user_about_me","user_birthday","user_location","user_events","user_friends","read_friendlists"],
        'METHOD': 'js_sdk',
        'VERIFIED_EMAIL': False
    }
}

SOCIALACCOUNT_ADAPTER = "frittie.apps.auth.adapters.CustomSocialAccountAdapter"


MULTI_FILE_DELETE_URL = 'multi_delete'
MULTI_IMAGE_URL = 'multi_image'
MULTI_IMAGES_FOLDER = 'uploads' 

NORMAL_STREAM_LIMIT = 10
PAGE_STREAM_LIMIT = 20

CELERYBEAT_SCHEDULER = "djcelery.schedulers.DatabaseScheduler"  

FACEBOOK_PROVIDER = "facebook"
FACEBOOK_APP_NAME = "Frittie Facebook App"

# FACEBOOK_STORE_LIKES = False
# FACEBOOK_STORE_FRIENDS = True

# FACEBOOK_CELERY_STORE = True
# FACEBOOK_CELERY_TOKEN_EXTEND = True

# Allow all host headers
ALLOWED_HOSTS = ['*']

# Static assets configuration
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
VENV_DIR = os.path.abspath(__file__ + "/../../")

STATIC_ROOT = 'staticfiles'


STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'assets/static'),
    os.path.join(VENV_DIR, 'venv-frittie/lib/python2.7/site-packages/dajaxice'),
)

TASTYPIE_ALLOW_MISSING_SLASH = True
APPEND_SLASH = True
TASTYPIE_CANNED_ERROR = "Oops, we broke it!"

# COMPRESS_ENABLED = True
# COMPRESS_CSS_FILTERS = [
#     #creates absolute urls from relative ones
#     'compressor.filters.css_default.CssAbsoluteFilter',
    
#     #css minimizer
#     'compressor.filters.cssmin.CSSMinFilter'
# ]
# COMPRESS_JS_FILTERS = [
#     'compressor.filters.jsmin.JSMinFilter'
# ]

GEO_PATH_DIR = os.path.abspath(__file__ + "/../../")

GEOIP_PATH = os.path.join(GEO_PATH_DIR, "frittie/db/geolocation")  ### path for geoip dat

GEOIP_DATABASE = os.path.join(GEO_PATH_DIR, "frittie/db/geolocation/GeoLiteCity.dat")

CITIES_FILES = {
    'city': {
       'filename': 'cities1000.zip',
       'urls':     ['http://download.geonames.org/export/dump/'+'{filename}']
    },
}

OW_LY_API_KEY = "6sv891CJpDcuiz8eyRHfy"

SOCIAL_FRIENDS_USING_ALLAUTH = True

MAX_MANDRILL_EMAIL_ALLOW = 12000



