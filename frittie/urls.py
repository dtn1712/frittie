from django.conf.urls import patterns, include, url
from django.contrib import admin
from dajaxice.core import dajaxice_autodiscover, dajaxice_config
from django.conf import settings

from tastypie.api import Api
from frittie.apps.api.feedapi import FeedResource 
from frittie.apps.api.locationapi import LocationResource 
from frittie.apps.api.userapi import UserResource 
from frittie.apps.api.commentapi import CommentResource 
from frittie.apps.api.photoapi import PhotoResource 
from frittie.apps.api.activityapi import ActivityResource 

from frittie.apps.main.views import error_page

from djrill import DjrillAdminSite

from frittie import settings as frittie_settings

dajaxice_autodiscover()
admin.site = DjrillAdminSite()
admin.autodiscover()

frittie_api = Api(api_name='frittie')
frittie_api.register(LocationResource())
frittie_api.register(UserResource())
frittie_api.register(ActivityResource())
frittie_api.register(PhotoResource())
frittie_api.register(FeedResource())
frittie_api.register(CommentResource())

location_resource = LocationResource()

urlpatterns = patterns('',
    
    # Admin URL
    url(r'^admin/', include(admin.site.urls)),

    url(r'^accounts/', include("frittie.apps.auth.urls")),

    # Haystack app URL
    url(r'^search/', include("frittie.apps.search.urls")),

    # Frittie URL
    url(r'^$', include('frittie.apps.main.urls')),
    url(r'^settings/$','frittie.apps.member.views.settings_page',name="account_settings"),
    url(r'^people/update/picture/$',"frittie.apps.member.views.update_picture",name='account_update_picture'),

    url(r'^ticket/',include('frittie.apps.ticket.urls')),
    url(r'^explore/',include('frittie.apps.explore.urls')),
    url(r'^about/',include('frittie.apps.about.urls')),
    url(r'^watchlist/',include("frittie.apps.watchlist.urls")),
    url(r"^notification/",include("frittie.apps.notification.urls")),
    url(r'^messages/',include('frittie.apps.message.urls')),
    url(r'^location/',include('frittie.apps.location.urls')),
    url(r'^activity/',include('frittie.apps.activity.urls')),
    url(r'^feed/',include('frittie.apps.feed.urls')),
    url(r'^friends/',include('frittie.apps.friend.urls')),
    url(r'^people/(?P<username>\w+)/',include('frittie.apps.member.urls')),
    url(r'^photo/',include('frittie.apps.photo.urls')),
    url(r'^ticket/',include('frittie.apps.ticket.urls')),
    
    # API URL
    url(r'^api/', include(frittie_api.urls)),
    url(r'^api/(?P<username>\w+)/', include(location_resource.urls)),
    url(r'^interface/', include('frittie.apps.api-interface.urls')),

    # Error page
    url(r'^error/$',error_page),
                    
    # Dajaxice URL
    url(dajaxice_config.dajaxice_url, include('dajaxice.urls')),

    # Media URL
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',{'document_root': settings.MEDIA_ROOT}),

    url(r'^i18n/', include('django.conf.urls.i18n')),

)

handler403 = error_page
handler404 = error_page
handler500 = error_page
