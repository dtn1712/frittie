/**
 * @author Knightus
 */

$.each($('#watchlist_list .collapse'), function() {
	$(this).on('show.bs.collapse', function() {
		$('[href=#' + $(this).attr('id') + '] i').removeClass('fa-caret-right').addClass('fa-caret-down');
	}).on('hide.bs.collapse', function() {
		$('[href=#' + $(this).attr('id') + '] i').removeClass('fa-caret-down').addClass('fa-caret-right');
	});
});

$('#content').scrollspy({ target: '#watchlist_content' });

$('#watchlist_list>li>ul>li').on('click','a',function(){
	$('#watchlist_list').find('li').removeClass('active');
	$(this).closest('li').addClass('active');
});
