Welcome to {{activity.name}}

Your confirmation id for this activity is:
{{confirmation_id}}

To view more details, please visit:
{{url}}

You may need to use the confirmation id in some activity

Thanks,
The Frittie Team
