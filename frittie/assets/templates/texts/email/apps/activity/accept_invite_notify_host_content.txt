Hello {{activity.host_fullname}},

{{user_send.user_fullname}} just accepted your invitation to join in activity {{activity.name}}

To manage your guest, please visit:
{{url}}

Thanks,
The Frittie Team