function getNearbyActivityCallback(data) {
	hideAjaxLoadingIcon();
	if (data['results']['success']) {
		$(".overlay").remove();
		Dajaxice.frittie.apps.activity.get_activity_overlays(getActivityOverlaysCallback,{
	    	'data': data['results']['nearby_activity_data']
	    });
	    $("#content_area").html(data['results']['snippet_return']);
	    cropImages(".image-container");
	    addDotToLongTextWithElementAttr(".add-dot","data-html-maxlength");
	}
}