$(window).scroll(function(){
   	if  (isTriggerLoadingScroll()) {
      	loadMorePhoto();
   	}
}); 

/* ADD PHOTO HANDLER */
$(function() {
	var object_unique_id = $("#object_unique_id").val();
	var current_link = window.location.href;
	var redirect_link = "";

	if (current_link.search("activity") >= 0 ){
		redirect_link = "/activity/" + object_unique_id;
		$("#add_photo_form input[name='photo_type']").val("activity");
		$("#add_photo_form input[name='photo_type']").val("activity");
		$("#add_photo_form input[name='success_url']").val("/photo/activity/" + object_unique_id);
	}

	if (current_link.search("location") >= 0 ){
		redirect_link = "/location/" + object_unique_id;
		$("#add_photo_form input[name='photo_type']").val("location");
		$("#add_photo_form input[name='photo_type']").val("location");
		$("#add_photo_form input[name='success_url']").val("/photo/location/" + object_unique_id);
	}

    $("#add_photo_form input[name='unique_id']").val(object_unique_id);    
    $("#add_photo_form input[name='callback_url']").val(redirect_link);
})

/* NAVIGATION PHOTO HANDLER */
$(function() {
	$("#next_icon").click(function() {
		var current_photo_unique_id = $("#current_photo_unique_id").val();
		var current_photo_el = $(".photo-item[data-unique-id='" + current_photo_unique_id + "']")[0];
		var next_el = $(current_photo_el).next(".photo-item")[0];
		if (!isUndefined(next_el)) {
			var photo_user_post_username = $($(next_el).find(".photo-user-post-username")[0]).text();
			$("#current_photo_unique_id").val($(next_el).attr("data-unique-id"));
			$("#current_photo_image").attr("src",$($(next_el).find(".photo-image")[0]).attr("src"));
			$("#current_photo_user_post").attr("href","/people/" + photo_user_post_username);
			$("#current_photo_user_post").html($($(next_el).find(".photo-user-post-fullname")[0]).text());
			$("#current_photo_user_post_avatar").attr("src",$($(next_el).find(".photo-user-post-avatar")[0]).text());
			$("#current_photo_caption").html($($(next_el).find(".photo-caption")[0]).text());
			$("#current_photo_upload_date").html($($(next_el).find(".photo-upload-date")[0]).text());
			$("#previous_icon").removeClass("hide");
			if (isUndefined($(next_el).next(".photo-item")[0])) {
				$("#next_icon").addClass("hide");
			}
			if ($("#user_login_username").val() == photo_user_post_username) {
				$("#current_photo_action").removeClass("hide");
			} else {
				$("#current_photo_action").addClass("hide");
			}
		}
	})

	$("#previous_icon").click(function() {
		var current_photo_unique_id = $("#current_photo_unique_id").val();
		var current_photo_el = $(".photo-item[data-unique-id='" + current_photo_unique_id + "']")[0];
		var prev_el = $(current_photo_el).prev(".photo-item")[0];
		if (!isUndefined(prev_el)) {
			var photo_user_post_username = $($(prev_el).find(".photo-user-post-username")[0]).text();
			$("#current_photo_unique_id").val($(prev_el).attr("data-unique-id"));
			$("#current_photo_image").attr("src",$($(prev_el).find(".photo-image")[0]).attr("src"));
			$("#current_photo_user_post").attr("href","/people/" + photo_user_post_username);
			$("#current_photo_user_post").html($($(prev_el).find(".photo-user-post-fullname")[0]).text());
			$("#current_photo_user_post_avatar").attr("src",$($(prev_el).find(".photo-user-post-avatar")[0]).text());
			$("#current_photo_caption").html($($(prev_el).find(".photo-caption")[0]).text());
			$("#current_photo_upload_date").html($($(prev_el).find(".photo-upload-date")[0]).text());
			$("#next_icon").removeClass("hide");
			if (isUndefined($(prev_el).prev(".photo-item")[0])) {
				$("#previous_icon").addClass("hide");
			}
			if ($("#user_login_username").val() == photo_user_post_username) {
				$("#current_photo_action").removeClass("hide");
			} else {
				$("#current_photo_action").addClass("hide");
			}
		}	
	})	
})

/* CHANGE PHOTO CAPTION */
$(function() {
	$("#change_photo_caption_modal").on("show.bs.modal",function() {
		var current_photo_caption = stripWhitespace($("#current_photo_caption").text());
		var current_image_src = $("#current_photo_image").attr("src");
		$("#change_photo_caption_modal #new_photo_caption").val(current_photo_caption);
		$("#change_photo_caption_modal #display_current_change_photo_caption").attr("src",current_image_src);
		cropImages("div.image-container");
	})

	$("#submit_change_photo_caption_btn").click(function() {
		var new_caption = $("#change_photo_caption_modal #new_photo_caption").val();
		var photo_unique_id = $("#current_photo_unique_id").val();
		if (isUndefined(photo_unique_id) == false && isUndefined(new_caption) == false) {
			Dajaxice.frittie.apps.photo.change_photo_caption(changePhotoCaptionCallback, {
				"photo_unique_id": photo_unique_id,
				"new_caption":  new_caption,
			})
			showAjaxLoadingIcon();
		}	
		$("#change_photo_caption_modal").modal("hide");
	})
})

/* DELETE PHOTO */
$(function() {
	$("#delete_photo_modal").on("show.bs.modal",function() {
		$("form#delete_photo_form").attr("action","/photo/" + $("#current_photo_unique_id").val() + "/delete/");
		$("form#delete_photo_form input[name='callback_url']").val(location.pathname);
	})
})






