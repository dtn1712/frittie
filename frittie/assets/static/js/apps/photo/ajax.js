function loadMorePhotoCallback(data) {
	hideAjaxLoadingIcon();
	if (data['results']['success']) {
		var snippet_return = data['results']['snippet_return'];
		$("#photo_list .container").append(snippet_return);
		$("#num_photo_load").val(data['results']['num_photo_load']);
		cropImages(".image-container");
	} 
}

function changePhotoCaptionCallback(data) {
	hideAjaxLoadingIcon();
	if (data['results']['success']) {
		$("#current_photo_caption").html(data['results']['new_caption'])
	} else {
		showAlertMessage(data['results']['error_message'])
	}
}

function getDisplayPhotoCallback(data) {
	hideAjaxLoadingIcon();
	if (data['results']['success']) {
		$("#photo_details").html(data['results']['snippet_return']);
	} 
}