var total_photos = parseInt($("#total_photos").val());
var object_unique_id = $("#object_unique_id").val();
var object_type = $("#object_type").val();


function loadMorePhoto() {
	var num_photo_load = parseInt($("#num_photo_load").val());
	if (num_photo_load != -1 ) {
		if (num_photo_load < total_photos) {
			showAjaxLoadingIcon();
			Dajaxice.frittie.apps.photo.load_more_photo(loadMorePhotoCallback,{
				"num_photo_load": num_photo_load,
				"object_unique_id": object_unique_id,
				"object_type": object_type
			})
		}
	} 
}