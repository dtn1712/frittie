function editActivityNameCallback(data) {
  hideAjaxLoadingIcon();
  if (data['results']['success']) {
      var s = data['results']['snippet_return'];
      $(".detail-activity-name").html("<span>" + data['results']['new_activity_name'] + "</span>");

  } else {
      showAlertMessage(data['results']['error_message'])
      scrollPageTop();
  }  
}

function addCommentCallback(data) {
  hideAjaxLoadingIcon();
  if (data['results']['success']) {
      prependNewItem("#comment_tab ul#comment_list",data['results']['snippet_return'])
      $('#comment_textarea').val("")
      $(".comments-count").html(data['results']['num_comment'] + " Comments")
  } else {
      location.reload();
      //showAlertMessage(data['results']['error_message'])
  }  
}

function editCommentCallback(data) {
  hideAjaxLoadingIcon();
  if (data['results']['success']) {
      var comment_el = $("li.comment-item[data-unique-id=" + data['results']['comment_unique_id'] + "]");
      var comment_pos = $(comment_el).offset().top;   
      $(comment_el).replaceWith(data['results']['snippet_return']);
      var new_el = $("li.comment-item[data-unique-id=" + data['results']['comment_unique_id'] + "]");
      $(new_el).effect("highlight", {}, 3000);
      scrollPagePos(comment_pos);
      $("#comment_textarea").val("");
  } else {
      showAlertMessage(data['results']['error_message'])
  }
}

function deleteCommentCallback(data) {
  hideAjaxLoadingIcon();
  if (data['results']['success']) {
      var comment_unique_id = data['results']['comment_unique_id'];
      var comment_el = $("li.comment-item[data-unique-id=" + comment_unique_id + "]")[0]
      scrollPagePos($(comment_el).offset().top);
      $(comment_el).slideUp("normal", function() { $(this).remove(); } );
      $(".comments-count").html(data['results']['num_comment'] + " Comments")
  } else {
      showAlertMessage(data['results']['error_message'])
  }
}

function reportCommentCallback(data) {
  hideAjaxLoadingIcon();
  if (data['results']['success']) {
      showAlertMessage(data['results']['success_message'])
  } else {
      showAlertMessage(data['results']['error_message'])
  }
}

function reportActivityCallback(data) {
  hideAjaxLoadingIcon();
  if (data['results']['success']) {
      showAlertMessage(data['results']['success_message'])
  } else {
      showAlertMessage(data['results']['error_message'])
  }
}

function inviteGuestsByEmailCallback(data) {
  hideAjaxLoadingIcon();
  if (data['results']['success']) {
      showAlertMessage(data['results']['success_message'])
  } else {
      showAlertMessage(data['results']['error_message'])
  }
}

function inviteGuestsByUsernameCallback(data) {
  hideAjaxLoadingIcon();
  if (data['results']['success']) {
      showAlertMessage(data['results']['success_message'])
  } else {
      showAlertMessage(data['results']['error_message'])
  }
}

function checkConfirmationIdCallback(data) {
  hideAjaxLoadingIcon();
  if (data['results']['success']) {
    showAlertMessage(data['results']['confirm_message']);
  } else {
    showAlertMessage(data['results']['error_message']);
  }
}


function watchActivityCallback(data) {
  hideAjaxLoadingIcon();
  if (data['results']['success']) {
    $("#watchlist_handler_btn").val(data['results']['watch_value']);
    $("#watchlist_handler_btn").html('<i class="glyphicon glyphicon-ok"></i>&nbsp;&nbsp;Watching');
    $("#watchlist_handler_btn").addClass("btn-success");
    $("#watchlist_handler_btn").removeClass("btn-default");
  } else {
    showAlertMessage(data['results']['error_message']);
  }
}

function unwatchActivityCallback(data) {
  hideAjaxLoadingIcon();
  if (data['results']['success']) {
    $("#watchlist_handler_btn").val(data['results']['watch_value']);
    $("#watchlist_handler_btn").html('<i class="glyphicon glyphicon-bookmark"></i>&nbsp;&nbsp;Add to Watchlist');
    $("#watchlist_handler_btn").removeClass("btn-success btn-danger");
    $("#watchlist_handler_btn").addClass("btn-default");
  } else {
    showAlertMessage(data['results']['error_message']);
  }
}


function composeNewMessageCallback(data) {
  hideAjaxLoadingIcon();
  if (data['results']['success']) {

    // Clear previous data
    $("#find_people_input").val("");
    $('#new_message_textarea').val("")
    $("#user_chat_username").val("");

    // Show successfuly message and render the html 
    showAlertMessage(data['results']['success_message'])
    
  } else {
    showAlertMessage(data['results']['error_message'])
  }
}

function followUserCallback(data) {
    if (data['results']['success']) {
        $("#follow_host_btn").html("Following");
        $("#follow_host_btn").val(FOLLOWING);
        $("#follow_host_btn").removeClass("btn-default btn-danger");
        $("#follow_host_btn").addClass("btn-success");
        $("#host_num_followers").html(data['results']['user_view_num_followers'] + " followers")
    } else {
        showAlertMessage(data['results']['error_message']);
    }
}

function unfollowUserCallback(data) {
    if (data['results']['success']) {
        $("#follow_host_btn").html("Follow");
        $("#follow_host_btn").val(NOT_FOLLOW);
        $("#follow_host_btn").removeClass("btn-success btn-danger");
        $("#follow_host_btn").addClass("btn-default");
        $("#host_num_followers").html(data['results']['user_view_num_followers'] + " followers")
    } else {
        showAlertMessage(data['results']['error_message']);
    }
}

function decideJoinRequestCallback(data) {
    if (data['results']['success']) {
      var action = data['results']['action'];
      var username = data['results']['user_info']['username'];
      var going_type = data['results']['going_type']
      if (action == "accept_request") {
          $("#" + going_type + "_going_list").append(data['results']['snippet_return']);
          var num_going = parseInt($("#num_" + going_type + "_going").text()) + 1;
          $("#num_" + going_type + "_going").text(num_going);
      } 
      var num_join_requests = parseInt($("#num_join_requests").text()) - 1;
      $("#num_join_requests").text(num_join_requests); 
      $(".accept-request-btn[data-username='" + username + "']").closest("li").remove();
    } else {
      showAlertMessage(data['results']['error_message']);
    }
}

function decideInviteActivityCallback(data) {
    hideAjaxLoadingIcon();
    if (data['results']['success']) {
      $("#invite_btn_group").replaceWith(data['results']['snippet_return']);
      showAlertMessage(data['results']['success_message'])
    } else {
      showAlertMessage(data['results']['error_message'])
    }
}

function joinActivityHandlerCallback(data) {
    hideAjaxLoadingIcon();
    if (data['results']['success']) {
      var action = data['results']['action']
      if (action == "request_join") {
        $("#request_join_btn").val("waiting");
        $("#request_join_btn").html("Waiting for accepted");
      } else if (action == "cancel_attending") {
        $($("#request_join_btn").parent()).replaceWith(data['results']['snippet_return']);
      } else if (action == "cancel_request") {
        $("#request_join_btn").val("request_join");
        $("#request_join_btn").removeClass("btn-danger");
        $("#request_join_btn").html('<i class="glyphicon glyphicon-plus "></i>&nbsp;&nbsp;Request to Join');
        handleJoinRequestButtonHover();
      } 
      showAlertMessage(data['results']['success_message'])
    } else {
      showAlertMessage(data['results']['error_message'])
    }
}


function previewTicketPriceCallback(data) {
  hideAjaxLoadingIcon();
  if (data['results']['success']) {
    var s = data['results']['snippet_return'];
    $('#ticket_info_detail .panel-body').html(s);
  } 
}
