$(function() {
    var get_parameter = window.location.search.replace("?", "");
    if (get_parameter.indexOf("show_ticket=True") != -1) {
      $("#ticket_info_tab_link").tab("show");
    }
})

$(function() {

    var invite_users_tag = $('input#invite_users_input');
    
    if (isExist(invite_users_tag)) {
        var user_data = new Bloodhound({
            datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.label); },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: {
              url:"/search/autocomplete/people",
              ttl: 10 * 60 * 1000,
            }
        });

        user_data.initialize();
        
        invite_users_tag.tagsinput({
          itemValue: "value",
          itemText: "label"
        });
     
        invite_users_tag.tagsinput('input').typeahead(null, {
          displayKey: 'label',
          source: user_data.ttAdapter(),
          templates: {
              suggestion: Handlebars.compile([
                  '<div class="picture pull-left small-margin-right"><img src="{{picture}}"/></div>',
                  '<div class="name pull-left">{{label}}</div>'
              ].join(''))
          }
        }).bind('typeahead:selected', $.proxy(function (obj, datum) {  
          this.tagsinput('add', datum);
          $("#invite_user_modal .tt-input").val("");
        }, invite_users_tag));
    }

    $("#invite_users_input").on('itemAdded', function(event) {
        var users = $("#invite_users_input").tagsinput("items");
        if (users.length != 0) {
          $("#invite_email_modal .error-notice").addClass("hide");
          $("#invite_email_modal .bootstrap-tagsinput").removeClass("has-error");
        } else {
          $("#invite_email_modal .error-notice").removeClass("hide");
          $("#invite_email_modal .bootstrap-tagsinput").addClass("has-error");
        }
    });

    $("#invite_users_input").on('itemRemoved', function(event) {
        var users = $("#invite_users_input").tagsinput("items");
        if (users.length != 0) {
          $("#invite_email_modal .error-notice").addClass("hide");
          $("#invite_email_modal .bootstrap-tagsinput").removeClass("has-error");
        } else {
          $("#invite_email_modal .error-notice").removeClass("hide");
          $("#invite_email_modal .bootstrap-tagsinput").addClass("has-error");
        }
    });

    $("#invite_users_final_btn").click(function() {
        var users = $("#invite_users_input").tagsinput("items");
        if (users.length != 0) {
          var usernames = []
          for (var i = 0; i < users.length; i++) {
              usernames.push(users[i]['value']);
          }
          $("#invite_user_modal .error-notice").addClass("hide");
          var content = stripWhitespace($("#user_invitation_content").val());
          Dajaxice.frittie.apps.activity.invite_guests_by_username(inviteGuestsByUsernameCallback,{
            "unique_id": activity_unique_id,
            "content": content,
            "usernames": usernames
          })
          $("#invite_user_modal").modal("hide");
          showAjaxLoadingIcon();
        } else {
          $("#invite_user_modal .error-notice").removeClass("hide");
          $("#invite_user_modal .bootstrap-tagsinput").addClass("has-error");
        }
    })

})

$(function() {
    var current_url = location.href;            
    var qr_code = "https://chart.googleapis.com/chart?chs="+"250x250"+"&cht=qr&chl="+ current_url;
    $("#qr_picture").attr("src", qr_code);
})


$(function(){
    $("#add_photo_form input[name='photo_type']").val("activity")
    $("#add_photo_form input[name='callback_url']").val(redirect_link);
    $("#add_photo_form input[name='success_url']").val("/photo/activity/" + activity_unique_id);
    $("#add_photo_form input[name='unique_id']").val(activity_unique_id);


    // if ($("#is_activate_message").val() == "True") {
    //   $("#main_section").css("margin-top","42px");
    // }

    
    $("#watchlist_handler_btn").hover(
      function(){
        if ($(this).val() == "watching") {
          $(this).html("Remove from watchlist");
          $(this).addClass("btn-danger");
        }
      },
      function(){
        if ($(this).val() == "watching") {
          $(this).html('<i class="glyphicon glyphicon-ok"></i><span>&nbsp;&nbsp;Watching</span>');
          $(this).removeClass("btn-danger");
        }
      }
    )  

    $("#watchlist_handler_btn").click(function() {
      checkLogin(redirect_link);
      var watchlist_status = $(this).val();
      if (watchlist_status == NOT_WATCH) {
        Dajaxice.frittie.apps.activity.watch_activity(watchActivityCallback, {
          "unique_id": activity_unique_id,
        })
      } else {
        Dajaxice.frittie.apps.activity.unwatch_activity(unwatchActivityCallback, {
          "unique_id": activity_unique_id,
        })
      }
    })
})

    
/* HANDLE MESSAGING TO EVENT HOST */
$(function(){
  
    $("#message_host_btn").click(function() {
      $("#compose_new_message_modal").modal("show");
      var host_username = $("#host_username").val();
      var host_fullname = stripWhitespace($("#host_fullname").text());
      $("#find_people_input").val(host_fullname);
      $("#find_people_input").attr("disabled","disabled");
      $("#user_chat_username").val(host_username);
    })


    $("#compose_new_message_btn").click(function() {
      var new_message_content = $("#new_message_textarea").val();
      if (stripWhitespace(new_message_content).length != 0) {
          var user_chat_username = $("#user_chat_username").val();
          Dajaxice.frittie.apps.message.compose_new_message(composeNewMessageCallback,{
                'message_content': new_message_content,
                "user_chat_username": user_chat_username
          })
          $("#compose_new_message_modal").modal("hide");
          showAjaxLoadingIcon();
      } 
    })
})
/********************************/
    
 /* HANDLE FOLLOWING HOST */
$(function() {
 
    $("#follow_host_btn").hover(
        function() {
            if ($(this).val() == FOLLOWING) {
                $(this).html("Unfollow");
                $(this).removeClass("btn-primary");
                $(this).addClass("btn-danger");
            }
        },
        function() {
            if ($(this).val() == FOLLOWING) {
                $(this).html("Following");
                $(this).removeClass("btn-danger");
                $(this).addClass("btn-primary");
            }
        }
    )

    $("#follow_host_btn").click(function() {
        checkLogin(redirect_link);
        var user_view_username = $("#host_username").val();
        if ($(this).val() == NOT_FOLLOW) {
            Dajaxice.frittie.apps.member.follow_user(followUserCallback, {
                "user_view_username": user_view_username,
            })
        } else {
            Dajaxice.frittie.apps.member.unfollow_user(unfollowUserCallback,{
                "user_view_username": user_view_username,
            })
        }
    })
})
/****************************/    


$(function() {
    /* HANDLE ADD AND EDIT COMMENT */
    $("#publish_comment_btn").click(function() {
      checkLogin(redirect_link);
      var comment_content = $("#comment_textarea").val();
      if (stripWhitespace(comment_content).length != 0) {
          var action_type = $(this).val();
          if (action_type == "add") {
              Dajaxice.frittie.apps.activity.add_comment(addCommentCallback,{
                'activity_unique_id': activity_unique_id,
                'comment_content': comment_content
              })
          } else {
              var comment_unique_id = $("#selected_comment_unique_id").val();
              Dajaxice.frittie.apps.activity.edit_comment(editCommentCallback,{
                'comment_unique_id': comment_unique_id,
                'comment_content': comment_content
              })
          } 
      } 
    })

    $("#cancel_edit_comment_btn").click(function() {
      $("#publish_comment_btn").val("add");
      $("#publish_comment_btn").html("Publish");
      $("#cancel_edit_comment_btn").addClass("hide");
      $("#selected_comment_unique_id").val("");
      $("#comment_textarea").css("width",532);
      $("#comment_textarea").val("");
      $("#comment_textarea").focusout();
    })
    /*********************************/


    /* HANDLE DELETE COMMENT */
    $("#delete_comment_final_btn").click(function() {
        var comment_unique_id = $($($(this).parent()).find("#delete_comment_unique_id")[0]).val();
        Dajaxice.frittie.apps.activity.delete_comment(deleteCommentCallback,{
          "comment_unique_id": comment_unique_id, 
          "activity_unique_id": activity_unique_id
        })
    })
    /*********************************/
})
    


$(function(){
    /* CLEAR MODAL ERROR AFTER MODAL IS HIDDEN */
    $(".clear-modal-error").on('hidden.bs.modal',function() {
        clearModalError(this);  
    })

    /**** REPORT COMMENT ****/
    $(".report-comment-button").click(function() {
      checkLogin(redirect_link);
      var parent_el = $(this).closest("li.comment-item");
      var comment_pk = $(parent_el).attr("data-unique-id");
      $("#report_comment_unique_id").val(comment_pk);
    })

    $("#report_comment_final_btn").click(function() {
        var report_content = $("#report_comment_textarea").val();
        if (stripWhitespace(report_content).length != 0) {
            var comment_unique_id = $($($(this).parent()).find("#report_comment_unique_id")[0]).val();
            Dajaxice.frittie.apps.activity.report_comment(reportCommentCallback,{"comment_unique_id": comment_unique_id,"report_content": report_content})
            $("#report_comment_modal").modal("hide");
            showAjaxLoadingIcon();
        } else {
            $("#report_comment_textarea").addClass("error");
            $("#report_comment_modal .modal-body p").addClass("error");
        } 
    })
    /*************************/

    /**** REPORT ACTIVITY ****/
    $("#report_activity_final_btn").click(function() {
        var report_content = $("#report_activity_textarea").val();
        if (stripWhitespace(report_content).length != 0) {
            Dajaxice.frittie.apps.activity.report_activity(reportActivityCallback,{
              "activity_unique_id": activity_unique_id,
              "report_content": report_content
            })
            $("#report_activity_modal").modal("hide");
            showAjaxLoadingIcon();
        } else {
            $("#report_activity_textarea").addClass("error");
            $("#report_activity_modal .modal-body p").addClass("error");
        } 
    })
    /*************************/
})
    
/* HANDLE JOIN REQUEST */
$(function(){

    $('#disable_request_join_btn').tooltip();
    
    handleJoinRequestButtonHover();

    $("#accept_invite_btn").click(function() {
        Dajaxice.frittie.apps.activity.accept_invite_activity(decideInviteActivityCallback, {
          "unique_id": activity_unique_id
        })
        showAjaxLoadingIcon();
    })

    $("#decline_invite_btn").click(function() {
        var unique_id = $("#activity_unique_id").val();
        Dajaxice.frittie.apps.activity.decline_invite_activity(decideInviteActivityCallback, {
          "unique_id": activity_unique_id
        })
        showAjaxLoadingIcon();
    })

    $(".accept-request-btn").click(function() {
        var username = $(this).attr("data-username");
        Dajaxice.frittie.apps.activity.accept_joining_activity_request(decideJoinRequestCallback, {
          "unique_id": activity_unique_id,
          "username": username
        })
    })

    $(".decline-request-btn").click(function() {
        var username = $(this).attr("data-username");
        Dajaxice.frittie.apps.activity.decline_joining_activity_request(decideJoinRequestCallback, {
          "unique_id": activity_unique_id,
          "username": username
        })
    })

    $('#check_confirmation_btn').click(function(){
      var confirmation_id = $("#confirmation_id_input").val();
      if (stripWhitespace(confirmation_id).length != 0) {
        Dajaxice.frittie.apps.activity.check_confirmation_id(checkConfirmationIdCallback,{
            'activity_unique_id':activity_unique_id,
            'confirmation_id':confirmation_id
        });
      }
    });
})
/******************************/


    
/* HANDLE ACTIVITY PICTURE */
$(function(){

    $("ul#photo_thumbnail_list img.photo-thumbnail").mouseover(function() {
      $("ul#photo_thumbnail_list li.photo-thumbnail-item").removeClass("active");
      var img_src = $(this).attr("src");
      $("#main_activity_logo").attr("src",img_src);
      $(this).closest("li").addClass("active")
    })

    $("ul#photo_thumbnail_list img.photo-thumbnail").mouseover(function() {
        $("ul#photo_thumbnail_list li").removeClass("active");
        var img_src = $(this).attr("src");
        $("#main_activity_logo").attr("src",img_src);
        $(this).closest("li").addClass("active");
    })

    $("#update_activity_logo_file").change(function() {
      $("#activity_logo_form").submit();
      showAjaxLoadingIcon();
    })
})
/*********************/

/* DELETE ACTIVITY */
$(function() {
    $("#confirm_activity_name_input").on("input",function() {
      if ($(this).val().toLowerCase() == $("#activity_name").val().toLowerCase()) {
        $("#delete_activity_final_btn").removeAttr("disabled");
      } else {
        $("#delete_activity_final_btn").attr("disabled","disabled");
      }
    })
})
/*******************/

/* INVITE */
$(function() {
    var getEmailTagInputError = function() {
        var emails = $("#invite_emails_input").tagsinput("items");
        var error_type = null;
        if (emails.length == 0) {
          error_type = "empty";
        } else {
          for (var i = 0; i < emails.length; i++) {
            var email = emails[i];
            if (isValidEmail(email) == false) {
              error_type = "incorrect";
              break;
            } 
            if (email == $("#activity_host_email").val()) {
              error_type = "invalid";
              break;
            }
          }
        }
        return error_type
    }

    $("#invite_emails_final_btn").click(function() {
        var email_tagsinput_error = getEmailTagInputError();
        if (email_tagsinput_error == null) {
          $("#invite_email_modal .error-notice").addClass("hide");
          var emails = $("#invite_emails_input").tagsinput("items");
          var content = stripWhitespace($("#email_invitation_content").val());
          Dajaxice.frittie.apps.activity.invite_guests_by_email(inviteGuestsByEmailCallback,{
            "unique_id": activity_unique_id,
            "content": content,
            "emails": emails
          })
          $("#invite_email_modal").modal("hide");
          showAjaxLoadingIcon();
        } else {
          $("#invite_email_modal .error-notice").addClass("hide");
          $("#invite_email_modal .error-notice.email-" + email_tagsinput_error).removeClass("hide");
          $("#invite_email_modal .bootstrap-tagsinput").addClass("has-error");
        }
    })

    $("#invite_emails_input").on('itemAdded', function(event) {
        var email_tagsinput_error = getEmailTagInputError();
        if (email_tagsinput_error == null) {
          $("#invite_email_modal .error-notice").addClass("hide");
          $("#invite_email_modal .bootstrap-tagsinput").removeClass("has-error");
        } else {
          $("#invite_email_modal .error-notice").addClass("hide");
          $("#invite_email_modal .error-notice.email-" + email_tagsinput_error).removeClass("hide");
          $("#invite_email_modal .bootstrap-tagsinput").addClass("has-error");
        }
    });

    $("#invite_emails_input").on('itemRemoved', function(event) {
        var email_tagsinput_error = getEmailTagInputError();
        if (email_tagsinput_error == null || email_tagsinput_error == "empty") {
          $("#invite_email_modal .error-notice").addClass("hide");
          $("#invite_email_modal .bootstrap-tagsinput").removeClass("has-error");
        } else {
          $("#invite_email_modal .error-notice").addClass("hide");
          $("#invite_email_modal .error-notice.email-" + email_tagsinput_error).removeClass("hide");
          $("#invite_email_modal .bootstrap-tagsinput").addClass("has-error");
        }
    });

    $(".invite-modal").on('hidden.bs.modal',function() {
        var error_notices = $(this).find(".error-notice");
        for (var i = 0; i < error_notices.length; i++) {
          $(error_notices[i]).addClass("hide");
        }

        var error_fields = $(this).find(".has-error")
        for (var i = 0; i < error_fields.length; i++) {
          $(error_fields[i]).removeClass("has-error");
        }
    })
})
/*************************/

/* ADD TICKET FORM */
$(function() {
    var validate_input_change = function(input) {
        var is_form_clicked = $("#is_add_ticket_form_clicked").val(); 
        if (is_form_clicked == "true") {
              
          // Clear previous error field
          handleInputError(true,input,"required");
          handleInputError(true,input,"numeric");

          // Validate the input
          validateInput(this,false)

          // Make change to the group header 
          var group = $(this).closest(".form-group");
          var is_valid = validateGroupInput(group,true);
          if (is_valid) {
              $("#edit_activity_form .error-notice").addClass("hide");
          } else {
              $("#edit_activity_form .error-notice").removeClass("hide");
          }
        }
    }

    var add_ticket_form = $("#add_ticket_form");


    /****** VALIDATE FORM AND INPUT *******/
    $("#add_ticket_final_btn").click(function() {
        $("#is_add_ticket_form_clicked").val("true");
        var is_valid = true;
        var groups = $("#add_ticket_form .form-group");
        for (var i = 0; i < groups.length; i++) {
          var is_group_valid = validateGroupInput(groups[i],false);
          if (is_group_valid == false) {
            is_valid = false;
          } 
        }
        if (is_valid) {
          $("#add_ticket_form .error-notice").addClass("hide");
          $(add_ticket_form).submit();
        } else {
          $("#add_ticket_form .error-notice").removeClass("hide");
        }

    })  

    $("#add_ticket_form input").each(function() {
        $(this).change(function() {
            validate_input_change(this);
        })
    }); 
    /**************************************/

})
/**************************/



/* EDIT AND DELETE TICKET */
$(function() {
    
    $(".edit-ticket-btn").click(function() {
        // pass the ticket_id to the edit_ticket_modal
        var id = $(this).attr("data-ticket-id");
        var parent_el = $(this).closest(".ticket-data");
        var name =  stripWhitespace($($(parent_el).find(".name")[0]).text());
        var currency = stripWhitespace($($(parent_el).find(".currency")[0]).text());
        var price = stripWhitespace($($(parent_el).find(".price")[0]).text());
        var total_available_quantity = stripWhitespace($($(parent_el).find(".total-available-quantity")[0]).text());
        var maximum_quantity_transaction = stripWhitespace($($(parent_el).find(".maximum-quantity-transaction")[0]).text());
        $("#edit_ticket_form input[name='ticket_id']").val(id);
        $("#edit_ticket_form input[name='ticket_type']").val(name);
        $("#edit_ticket_form input[name='currency']").val(currency);
        $("#edit_ticket_form input[name='price']").val(price);
        $("#edit_ticket_form input[name='total_available']").val(total_available_quantity);
        $("#edit_ticket_form input[name='max_quantity']").val(maximum_quantity_transaction);
    })

    $(".delete-ticket-btn").click(function() {
        $("#delete_ticket_modal").modal("show");
        // pass the ticket id to the delete_ticket_modal
        var id = $(this).attr("data-ticket-id");
        $("#delete_ticket_form input[name='ticket_id']").val(id);
    })
})

/*************************/

/** BUY TICKET **/
$('.reserve-ticket-button').click(function(){
  var t_min = $(".count-ticket-min").attr("id");
  var t_max = $(".count-ticket-max").attr("id");

  var ticket_pks = [];
  var ticket_quantities = [];
  for (var i = t_min; i <= t_max; i++ ){ 
    var tmp = "#ticket"+i;
    if ($(tmp).val() > 0 ) {
      ticket_pks.push(i);
      ticket_quantities.push($(tmp).val());
    }
  }

  $(".reserve-ticket-error").addClass("hide");
  if (ticket_quantities.length != 0) {
    var is_exceed = false;
    var tickets = $("#ticket_info_tab").find("tr.ticket-row");
    console.log(tickets);
    for (var i = 0; i < tickets.length; i++) {
      var ticket = tickets[i];
      var available_ticket = $($(ticket).find(".num-ticket-remain")[0]).val();
      var select_ticket = $($(ticket).find("select")[0]).val();
      if (parseInt(available_ticket) < parseInt(select_ticket)) {
        is_exceed = true;
        break;
      }
    }
    if (is_exceed == false) {
        $('#reserve_ticket_modal').modal('show');
        showAjaxLoadingIcon();
    } else {
        $(".reserve-ticket-error.exceed-ticket-allow").removeClass("hide");
    }
  } else {
    $(".reserve-ticket-error.empty-ticket-reserve").removeClass("hide");
  }

  Dajaxice.frittie.apps.ticket.calculate_preview_ticket_price(previewTicketPriceCallback,{'activity_unique_id':activity_unique_id,'ticket_pks':ticket_pks,'ticket_quantities':ticket_quantities});
}); 
    
/*************************/

/* EDIT ACTIVITY */
$(function() {

  var remove_location_btn = '<span class="glyphicon glyphicon-remove remove-location-btn" onclick="' + 
                'enableInputField(' + "'" +  '#edit_activity_form #id_activity_place' + "'" + ');' + 
                '$(this).remove();' + "$('#edit_activity_form .location-unique-id').val('');" +
                "$('#edit_activity_form #id_activity_place').css('background-color','white');" + 
                '"></span>'

  $("#edit_activity_form .activity-location .input-field").append(remove_location_btn);
  $("#edit_activity_form #id_activity_place").attr("disabled","disabled");
  $("#edit_activity_form #id_activity_place").css("background-color","#eee");

  $("#edit_activity_form input[name='location_additional_info']").val($("#prefil_location_additional_info").val());
  $('#edit_activity_form #id_activity_place').typeahead(null, {
        displayKey: 'label',
        source: location_data.ttAdapter(),
        templates: {
          suggestion: Handlebars.compile([
              '<p class="picture pull-left"><img src="{{picture}}"/></p>',
              '<p class="name pull-left">{{label}}</p>'
          ].join(''))
      }
    })
    .on('typeahead:selected', function($e,datum) {
        $("#edit_activity_form .location-unique-id").val(datum['value']);
        $("#edit_activity_form .activity-location .input-field").append(remove_location_btn);
        $("#edit_activity_form #id_activity_place").attr("disabled","disabled");
        $("#edit_activity_form #id_activity_place").css("background-color","#eee");
        validateInput($("#edit_activity_form #id_activity_place"),false,"tt-hint");
        //validateInput($("#edit_activity_form .location-unique-id"),false,"tt-hint");
    });

    var start_date = $("#edit_activity_form #start_date").datepicker({
      //startDate: "1d",
    }).on('changeDate', function(ev) {
        var select_date = new Date(ev.date)
        end_date.setStartDate(select_date);
        start_date.hide();
    }).data('datepicker');

    var end_date = $("#edit_activity_form #end_date").datepicker({})
      .on('changeDate', function(ev) {
          end_date.hide();
      }).data('datepicker');

    $("#edit_activity_form #unlimited_people_checkbox").change(function() {
        if($(this).is(":checked")) {
            $("#edit_activity_form #id_limit").attr("disabled","disabled");
            $("#edit_activity_form #id_limit").removeAttr("required");
            $("#edit_activity_form #id_limit").removeAttr("numeric");
            $("#edit_activity_form #id_limit").val("");
            handleInputError(true,this,"required");
            handleInputError(true,this,"numeric");
            $($(this).closest(".form-group").find(".form-header")[0]).removeClass("has-error");
        } else {
            $("#edit_activity_form #id_limit").removeAttr("disabled")
            $("#edit_activity_form #id_limit").attr("required","required");
            $("#edit_activity_form #id_limit").attr("numeric","numeric");
            $("#edit_activity_form #id_limit").val($("#limit_people_value").val());
        }      
    });
})

/****** EDIT ACTIVITY PHOTO ******/
$(function(){
    if (isExist("#edit_activity_form #edit_activity_logo_upload_area") && isExist("#edit_activity_form #id_logo")) {
      dragAndShowThumbnail("edit_activity_logo_upload_area","id_logo","edit_activity_logo_thumbnail");
    }

    $('#edit_activity_form #id_logo').bind('change', function() {
      $("#edit_activity_form .activity-logo .error-field").addClass("hide");
      if (this.files[0].size > VALID_FILE_SIZE) {
          $("#edit_activity_form .activity-logo .error-field.image-size").removeClass("hide");
          $("#edit_activity_form .activity-logo .image-container").addClass("hide");
      } else {
          $("#edit_activity_form .activity-logo .image-container").removeClass("hide");
          //readImageURL(this,"#display_activity_logo");
          showThumbnail("display_activity_logo",this.files[0]);
      }
    })
})

/****** VALIDATE EDIT ACTIVITY FORM *******/
$(function() {
    
    var edit_activity_form = $("#edit_activity_form");

    var validate_input_change = function(input) {
        var is_form_clicked = $("#is_edit_activity_form_clicked").val(); 
        if (is_form_clicked == "true") {
              
          // Clear previous error field
          handleInputError(true,input,"required");
          handleInputError(true,input,"numeric");
          handleInputError(true,input,"unique-id-correct");

          // Validate the input
          validateInput(this,false,"tt-hint")

          // Make change to the group header 
          var group = $(this).closest(".form-group");
          var is_valid = validateGroupInput(group,true,"tt-hint");
          if (is_valid) {
              $("#edit_activity_form .error-notice").addClass("hide");
          } else {
              $("#edit_activity_form .error-notice").removeClass("hide");
              if ( ($("#edit_activity_form .activity-location .error-field.required").hasClass("hide") == false) &&
                ($("#edit_activity_form .activity-location .error-field.unique-id-correct").hasClass("hide") == false)) {
                $("#edit_activity_form .activity-location .error-field.unique-id-correct").addClass("hide");
              }
          }
        }
    }

    var validate_form_submit = function() {
        tinyMCE.triggerSave();
        var content = tinyMCE.get("edit_description").getContent();
        $("#edit_activity_form #edit_description").val(content);
        $("#edit_activity_form #edit_description").html(content);
        $("#is_edit_activity_form_clicked").val("true");
        var is_valid = true;
        var groups = $("#edit_activity_form .form-group");
        for (var i = 0; i < groups.length; i++) {
          var group_header = $(groups[i]).find(".form-header");
          var is_group_valid = validateGroupInput(groups[i],false,"tt-hint");
          if (is_group_valid) {
            $(group_header).removeClass("has-error"); 
          } else {
            $(group_header).addClass("has-error");
            is_valid = false;
          }
        }
        if (stripWhitespace(content).length == 0) {
          is_valid = false;
          $("#edit_activity_form .activity-description .error-field").removeClass("hide");
        }
        if (is_valid) {
          $("#edit_activity_form .error-notice").addClass("hide");
          $("#edit_activity_form .error-field").addClass("hide");
          runTopProgressBar();
          $(edit_activity_form).submit();
        } else {
          $("#edit_activity_form .error-notice").removeClass("hide");
          if ( ($("#edit_activity_form .activity-location .error-field.required").hasClass("hide") == false) &&
            ($("#edit_activity_form .activity-location .error-field.unique-id-correct").hasClass("hide") == false)) {
            $("#edit_activity_form .activity-location .error-field.unique-id-correct").addClass("hide");
          }
        }
    }

    $(".save-btn").click(function(e) {
        e.preventDefault();
        if (isExist("#edit_activity_form #is_make_publish")) {
           $("#edit_activity_form #is_make_publish").val(0);
        } 
        validate_form_submit();
    }) 

    $(".publish-btn").click(function(e) {
        e.preventDefault();
        if (isExist("#edit_activity_form #is_make_publish")) {
           $("#edit_activity_form #is_make_publish").val(1);
        } 
        validate_form_submit();
    }) 

    $("#edit_activity_form input").each(function() {
        $(this).change(function() {
            validate_input_change(this);
        })
    }); 

    $("#edit_activity_form textarea").each(function() {
        $(this).change(function() {
            validate_input_change(this);
        })
    }); 

    $("#id_ticket_type").change(function() {
        var value = $(this).val();
        if (value == "1") {
          $(".activity-ticket-policy").removeClass("hide");
          $("#ticket_policy").attr("required","required");
        } else {
          $(".activity-ticket-policy").addClass("hide");
          $("#ticket_policy").removeAttr("required");
        }
    })

})
/********************************/

