$(function() {

    // if ($("#is_activate_message").val() == "True") {
    //     $("#activity_section").css("margin-top","42px");
    //     $("#location_section").css("margin-top","42px");
    //     $("#show_feed_btn").css("margin-top","42px");
    //     $("#live_feed").css("margin-top","42px");
    //     $("#people_search").css("margin-top","62px");
    // }

	$(".search-scope-item").click(function() {
		var search_type_click = $($(this).find("input[type='hidden']")[0]).val();
		if (current_search_type != search_type_click) {
			$("#search_type").val(search_type_click);
			$("#search_query").val(search_query);
			$("#search_form").submit();
		}
	})

	var redirect_link = "/search/" + current_search_type + "/?q=" + search_query;

    if (isExist("#is_user_login_in_search")) {
        var num_results = $("#people_search .results_count_value").val();
        $("#people_search .results_count_text").html(parseInt(num_results)-1);
    }

    $("#people_search .follow-handler-btn").hover(
        function() {
            if ($(this).val() == FOLLOWING) {
                $(this).html("Unfollow");
                $(this).removeClass("btn-primary");
                $(this).addClass("btn-danger");
            }
        },
        function() {
            if ($(this).val() == FOLLOWING) {
                $(this).html("Following");
                $(this).removeClass("btn-danger");
                $(this).addClass("btn-primary");
            }
        }
    )

    $("#people_search .follow-handler-btn").click(function() {
        checkLogin(redirect_link);
        var user_view_username = $($(this).parent().find(".user-view-username")[0]).val();
        if ($(this).val() == NOT_FOLLOW) {
            Dajaxice.frittie.apps.member.follow_user(followUserCallback, {
                "user_view_username": user_view_username,
            })
        } else {
            Dajaxice.frittie.apps.member.unfollow_user(unfollowUserCallback,{
                "user_view_username": user_view_username,
            })
        }
    })
})