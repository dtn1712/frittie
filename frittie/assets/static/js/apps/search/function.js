var search_query = $("#search_scope input[name='search_query']").val();
var current_search_type = $("#search_scope input[name='search_type']").val();

var map;
var location_info_box;

if (current_search_type == "activity") {
	var activity_overlays_data = getListActivityLocationDataMapping();
	google.maps.event.addDomListener(window, 'load', function() {
	    map = initializeMap("map_section");
	    var zoom_control_div = document.createElement('div');
	    var zoom_control = new zoomControlHandler(zoom_control_div, map);

	    if (location.search.indexOf("lat") == -1 ){
	        navigator.geolocation.getCurrentPosition(function(position) {
	            var my_position =  new google.maps.LatLng(position.coords.latitude.toFixed(2), position.coords.longitude.toFixed(2));
	            map.setCenter(my_position);
	        });
	    }

	    Dajaxice.frittie.apps.activity.get_activity_overlays(getActivityOverlaysCallback,{
	    	'data': activity_overlays_data
	    });
	}); 
} else if (current_search_type == "location") {
	var location_overlays_data = getListLocationUniqueId();
	google.maps.event.addDomListener(window, 'load', function() {
	    map = initializeMap("map_section");
	    var zoom_control_div = document.createElement('div');
	    var zoom_control = new zoomControlHandler(zoom_control_div, map);

	    navigator.geolocation.getCurrentPosition(function(position) {
	        my_position =  new google.maps.LatLng(position.coords.latitude.toFixed(2), position.coords.longitude.toFixed(2));
	        map.setCenter(my_position);
	    });

	    Dajaxice.frittie.apps.location.get_location_overlays(getLocationOverlaysCallback,{
	        "data": location_overlays_data,
	    })
	});
}


function showLocationInfoBox(location_unique_id,position) {
    var activity = $("li.location-search-item[data-unique-id='" + location_unique_id + "']")[0];
    var box_text = document.createElement("div");
    box_text.className = "info-box";
    box_text.innerHTML = $("#location_info_box_template").html();
}

LocationOverlay.prototype = new google.maps.OverlayView();

function LocationOverlay(data){
    this.unique_id = data['location_unique_id'];
    this.number = data['location_number'];
    this.div_ = null;
    this.pos = new google.maps.LatLng(data['location_data']['lat'],data['location_data']['lng']);
}
                    
//init your html element here
LocationOverlay.prototype.onAdd= function(){
    var me = this;
    var div = document.createElement('div');
    div.id = "single_overlay_" + this.number;
    div.className = "overlay single-overlay";
    div.onclick = function() {
        $("li.location-search-item").css("background-color","white");
        var el = $("li.location-search-item[data-item-number=" + me.number + "]")[0]; 
        scrollPagePos($(el).offset().top - 60);
        $(el).css("background-color","#eaeaea");
    }
    div.onmouseover = function() {
        showLocationInfoBox(me,me.unique_id);
    }
    div.onmouseout = function() {
        //$(".infoBox").hide();
    }
    div.innerHTML = this.number;
    this.div_ = div;
    var panes = this.getPanes();
    panes.overlayMouseTarget.appendChild(div);
}
                    
LocationOverlay.prototype.draw = function(){
    var overlay_projection = this.getProjection();
    var position = overlay_projection.fromLatLngToDivPixel(this.pos);
    var div = this.div_;
    div.style.left = position.x + 'px';
    div.style.top = position.y + 'px';
}

LocationOverlay.prototype.onRemove= function(){
    this.div_.parentNode.removeChild(this.div_);
    this.div_ = null;
}




