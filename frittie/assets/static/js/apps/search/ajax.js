function followUserCallback(data) {
    if (data['results']['success']) {
        var username = data['results']['user_view_username'];
        var parent_el = $(".people-search-item[data-value='" + username + "']")
        var button_el = $(parent_el).find(".follow-handler-btn")[0];
        var follower_el = $(parent_el).find("span.num-follower");
        $(button_el).html("Following");
        $(button_el).val(FOLLOWING);
        $(button_el).removeClass("btn-default btn-danger");
        $(button_el).addClass("btn-success");
        $(follower_el).html(data['results']['user_view_num_followers']);
    } else {
        showAlertMessage(data['results']['error_message']);
    }
}

function unfollowUserCallback(data) {
    if (data['results']['success']) {
        var username = data['results']['user_view_username'];
        var parent_el = $(".people-search-item[data-value='" + username + "']")
        var button_el = $(parent_el).find(".follow-handler-btn")[0];
        var follower_el = $(parent_el).find("span.num-follower");
        $(button_el).html("Follow");
        $(button_el).val(NOT_FOLLOW);
        $(button_el).removeClass("btn-success btn-danger");
        $(button_el).addClass("btn-default");
        $(follower_el).html(data['results']['user_view_num_followers']);
    } else {
        showAlertMessage(data['results']['error_message']);
    }
}

function getLocationOverlaysCallback(data) {
    if (data['results']['success']) {
        var overlays = data['results']['overlays'];
        for (var i = 0; i < overlays.length; i++) {
            var item = overlays[i];
            var overlay = new LocationOverlay(item);
            overlay.setMap(map);
        }
    }
}