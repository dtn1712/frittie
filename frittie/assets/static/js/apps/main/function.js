function filterHomePageActivity(el) {
	  var active_el = $(el).parent().find(".active");
    for (var i = 0; i < active_el.length; i++) {
      $(active_el).removeClass("active");
    }
    $(el).addClass("active");

    var filter_how_value = setNullForUndefined($($("#activity_sort_by").find(".active")[0]).attr("data-value"));
    var filter_category_value = setNullForUndefined($($("#activity_filter_by").find(".active")[0]).attr("data-value"));

    Dajaxice.frittie.apps.activity.filter_activity(filterHomePageActivityCallback,{
      "filter_category_value": filter_category_value,
      "filter_how_value": filter_how_value,
      "filter_when_value": null,
      "html_template": "list_activity_homepage",
    })
    showAjaxLoadingIcon();
}

function filterHomePageLocation(el) {
    var active_el = $(el).parent().find(".active");
    for (var i = 0; i < active_el.length; i++) {
      $(active_el).removeClass("active");
    }
    $(el).addClass("active");

    var filter_category_value = setNullForUndefined($($("#location_filter_by").find(".active")[0]).attr("data-value"));

    Dajaxice.frittie.apps.location.filter_location(filterHomePageLocationCallback,{
      "filter_category_value": filter_category_value,
      "current_city": $("#current_city").val(),
      "html_template": "list_location_homepage",
    })
    showAjaxLoadingIcon();
}

function loadMoreActivity() {
  var num_activities_load = parseInt($("#num_activities_load").val());
  var total_activities_count = parseInt($("#total_activities_count").val());
  if ( num_activities_load < total_activities_count) {
      $("#is_loading_item").val("True");
      Dajaxice.frittie.apps.main.load_more_activity(loadMoreActivityCallback,{
        "num_activities_load": num_activities_load,
        "lat": $("#current_latitude").val(),
        "lng": $("#current_longitude").val(),
      })
      showAjaxLoadingIcon();
  }
}

function loadMoreLocation() {
  var num_locations_load = parseInt($("#num_locations_load").val());
  var total_locations_count = parseInt($("#total_locations_count").text());
  if (num_locations_load < total_locations_count) {
      $("#is_loading_item").val("True");
      Dajaxice.frittie.apps.main.load_more_location(loadMoreLocationCallback,{
        "num_locations_load": num_locations_load,
        "city": $("#current_city").val(),
      })
      showAjaxLoadingIcon();
  }
}


