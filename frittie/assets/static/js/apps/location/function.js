var location_unique_id = $("#location_unique_id").val();
var redirect_link = "/location/" + location_unique_id


function initDeleteComment(el) {
  var parent_el = $(el).closest("li.comment-item");
  var comment_pk = $(parent_el).attr("data-unique-id");
  $("#delete_comment_unique_id").val(comment_pk);
}


function initEditComment(el) {
  var parent_el = $(el).closest("li.comment-item");
  var comment_pk = $(parent_el).attr("data-unique-id");
  var text_el = $($(parent_el).find("p.comment-text")[0]).html();
  $("#publish_comment_btn").val("edit");
  $("#publish_comment_btn").html("Submit");
  $("#cancel_edit_comment_btn").removeClass("hide");
  $("#selected_comment_unique_id").val(comment_pk);
  $(".add-comment-area").css("width",365);
  $("#comment_textarea").val(text_el);
  $("#comment_textarea").focus();
  scrollPagePos($(".comment-section").offset().top);
}

google.maps.event.addDomListener(window, "load", function() {
    var map_options = {
      zoom: 16,
      scrollwheel: true,
    }
    var location_map = initializeMap("location_map_canvas",map_options);

    var location_lat = $("#location_lat").val();
    var location_lng = $("#location_lng").val();

    var location_position = new google.maps.LatLng(location_lat,location_lng);
    location_map.setCenter(location_position);

    var marker = new google.maps.Marker({
        position : location_position,
        map: location_map,
    });
});

function deleteBusinessHour(el) {
    var business_hour_id = $(el).attr("data-business-hour-id");
    Dajaxice.frittie.apps.location.delete_business_hour(addOrDeleteBusinessHourCallback, {
      "business_hour_id": business_hour_id
    })
    showAjaxLoadingIcon();
}

function showBusinesHourDeleteIcon() {
    $(".open-hour-item").hover(
      function() {
        var item = $(this).find(".delete-business-hour-btn")[0];
        if (isUndefined(item) == false) {
          $(item).removeClass("hide");
        }
      },
      function() {
        var item = $(this).find(".delete-business-hour-btn")[0];
        if (isUndefined(item) == false) {
          $(item).addClass("hide");
        }
      }
    )
}

function deleteDetailInfo(el) {
    var detail_info_id = $(el).attr("data-detail-info-id");
    Dajaxice.frittie.apps.location.delete_detail_info(addOrDeleteDetailInfoCallback, {
      "detail_info_id": detail_info_id 
    })
    showAjaxLoadingIcon();
}

function showDetailInfoDeleteIcon() {
    $(".detail-info-item").hover(
      function() {
        var item = $(this).find(".delete-detail-info-btn")[0];
        if (isUndefined(item) == false) {
          $(item).removeClass("hide");
        }
      },
      function() {
        var item = $(this).find(".delete-detail-info-btn")[0];
        if (isUndefined(item) == false) {
          $(item).addClass("hide");
        }
      }
    )
}