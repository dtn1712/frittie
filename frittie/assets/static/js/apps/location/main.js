$(function(){
  if ($("#is_activate_message").val() == "True") {
    $("#main_section").css("margin-top","-1px");
  }

  $("#share_button").hideshare({
      link: $("#short_url").val(),
      position: "top",
      linkedin: false,
      pinterest: false, 
  });

  $("#add_photo_form input[name='photo_type']").val("location")
  $("#add_photo_form input[name='callback_url']").val(redirect_link);
  $("#add_photo_form input[name='success_url']").val("/photo/location/" + location_unique_id);
  $("#add_photo_form input[name='unique_id']").val(location_unique_id);

  $("#update_main_picture_file").change(function() {
    $("#main_picture_form").submit();
    showAjaxLoadingIcon();
  })
});



/* HANDLE FOLLOW LOCATION */
$(function() {
  $("#follow_handler").hover(
    function(){
      if ($(this).val() == FOLLOWING) {
        $(this).html("Unfollow");
        $(this).addClass("btn-danger");
      }
    },
    function() {
      if ($(this).val() == FOLLOWING) {
        $(this).html('<i class="glyphicon glyphicon-ok"></i>&nbsp;Following');
        $(this).removeClass("btn-danger");
      }
    }
  )

  $("#follow_handler").click(function() {
    checkLogin(redirect_link);
    if ($(this).val() == NOT_FOLLOW){
        Dajaxice.frittie.apps.location.follow_location(followCallback,{'location_unique_id': location_unique_id})
    } else {
        Dajaxice.frittie.apps.location.unfollow_location(unfollowCallback,{'location_unique_id': location_unique_id})
    }
  });
});
/*********************************/




/* HANDLE ADD AND EDIT COMMENT */
$(function() {
  $("#publish_comment_btn").click(function() {
    checkLogin(redirect_link);
    var comment_content = $("#comment_textarea").val();
    if (stripWhitespace(comment_content).length != 0) {
        var action_type = $(this).val();
        if (action_type == "add") {
            Dajaxice.frittie.apps.location.add_comment(addCommentCallback,{
              'location_unique_id': location_unique_id,
              'comment_content': comment_content
            })
        } else {
            var comment_unique_id = $("#selected_comment_unique_id").val();
            Dajaxice.frittie.apps.location.edit_comment(editCommentCallback,{
              'comment_unique_id': comment_unique_id,
              'comment_content': comment_content
            })
        } 
    } 
  })

  $("#cancel_edit_comment_btn").click(function() {
      $("#publish_comment_btn").val("add");
      $("#publish_comment_btn").html("Publish");
      $("#cancel_edit_comment_btn").addClass("hide");
      $("#selected_comment_unique_id").val("");
      $(".add-comment-area").css("width",425);
      $("#comment_textarea").val("");
      $("#comment_textarea").focusout();
  })
})
/*********************************/




/* HANDLE DELETE COMMENT */
$(function() {
  $("#delete_comment_final_btn").click(function() {
      var comment_unique_id = $($($(this).parent()).find("#delete_comment_unique_id")[0]).val();
      Dajaxice.frittie.apps.location.delete_comment(deleteCommentCallback,{
        "comment_unique_id": comment_unique_id, 
        "location_unique_id": location_unique_id
      })
  })
})
/*********************************/ 

  
  
 
/* HANDLE REPORT COMMENT */
$(function() {

  /* CLEAR MODAL ERROR AFTER MODAL IS HIDDEN */
  $(".clear-modal-error").on('hidden.bs.modal',function() {
      clearModalError(this);  
  })

  
  $(".report-comment-button").click(function() {
      checkLogin(redirect_link);
      var parent_el = $(this).closest("li.comment-item");
      var comment_pk = $(parent_el).attr("data-unique-id");
      $("#report_comment_unique_id").val(comment_pk);
  })

  $("#report_comment_final_btn").click(function() {
      var report_content = $("#report_comment_textarea").val();
      if (stripWhitespace(report_content).length != 0) {
          var comment_unique_id = $($($(this).parent()).find("#report_comment_unique_id")[0]).val();
          Dajaxice.frittie.apps.location.report_comment(reportCommentCallback,{"comment_unique_id": comment_unique_id,"report_content": report_content})
          $("#report_comment_modal").modal("hide");
          showAjaxLoadingIcon();
      } else {
          $("#report_comment_textarea").addClass("error");
          $("#report_comment_modal .modal-body p").addClass("error");
      } 
  })
  
})
/*********************************/


/* HANDLE REPORT LOCATION */
$(function(){
  $("#report_location_final_btn").click(function() {
      var report_content = $("#report_location_textarea").val();
      if (stripWhitespace(report_content).length != 0) {
          var location_unique_id =$("#location_unique_id").val();
          Dajaxice.frittie.apps.location.report_location(reportCallback,{"location_unique_id": location_unique_id,"report_content": report_content})
          $("#report_location_modal").modal("hide");
          showAjaxLoadingIcon();
      } else {
          $("#report_location_textarea").addClass("error");
          $("#report_location_modal .modal-body p").addClass("error");
      } 
  })
})
/*********************************/


/* HANDLE DATEPICKER */
$(function() {

  
  var datepicker = $('#datepicker').datepicker({
      //startDate: "1d"
  }).on('changeDate', function(ev) {
      var select_date = new Date(ev.date)
      Dajaxice.frittie.apps.location.load_activity(loadActivityCallback,{
          "location_unique_id": location_unique_id,
          "select_date": select_date,
          "sort_type": "hot"
      })
      datepicker.hide();
      showAjaxLoadingIcon();
  }).data('datepicker');
  
})
/*********************************/


/* HANDLER SORT ACTIVITY */
$(function() {
  
  $(".sort-link").click(function() {
      var sort_type = $(this).attr("data-sort");
      var date = $("#datepicker_value").val();
      var year = parseInt(date.substring(6,10));
      var month = parseInt(date.substring(0,2))
      var day = parseInt(date.substring(3,5));
      var select_date = new Date(year,month-1,day,0,0,0,0);
      Dajaxice.frittie.apps.location.load_activity(loadActivityCallback,{
          "location_unique_id": location_unique_id,
          "select_date": select_date,
          "sort_type": sort_type
      })
      showAjaxLoadingIcon();
  })
  
})
/*********************************/


/* DELETE LOCATION */
$(function() {
  $("#confirm_location_name").on("input",function() {
      var value = $(this).val();
      if (value.toLowerCase() == $("#location_name").val().toLowerCase()) {
        $("#delete_location_final_btn").removeAttr("disabled");
      } else {
        $("#delete_location_final_btn").attr("disabled","disabled");
      }
  })
})
/**********************/



/* HANDLE EDIT LOCATION */
$(function() {
  var address_value = getLocationFormAddress("#edit_location_form");
  geocodeAddress("#edit_location_form","edit_location_map_canvas",address_value);

  var edit_location_form = $("#edit_location_form");

  handleAddressUSA("#edit_location_form",$("#edit_location_form select#id_country").val() == "US");

  $("#edit_location_form select#id_country").change(function(){
      handleAddressUSA("#edit_location_form",$(this).val() == "US");
  })

  $("#edit_location_btn").click(function() {
      $("#is_edit_location_form_clicked").val("true");
      var is_valid = validateForm(edit_location_form,false);
      if (is_valid) {
          $("#edit_location_form .error-notice").addClass("hide");
          $(edit_location_form).submit();
          showAjaxLoadingIcon();
      } else {
          $("#edit_location_form .error-notice").removeClass("hide");
      }
  })  

  $("#edit_location_form input").each(function() {
      $(this).change(function() {
          validateInputChange("#edit_location_form",this);
      })
  });

  $("#edit_location_form textarea").each(function() {
      $(this).change(function() {
          validateInputChange("#edit_location_form",this);
      })
  });

  $("#edit_location_modal").on("hidden.bs.modal",function(e) {
      clearForm(edit_location_form);
  })

  $(".address-field").change(function() {
      var address_value = getLocationFormAddress("#edit_location_form");
      geocodeAddress("#edit_location_form","edit_location_map_canvas",address_value);
  })

  $('#edit_location_form input[name="main_picture"]').bind('change', function() {
      $("#edit_location_form .location-picture .error-field").addClass("hide");
      if (this.files[0].size > VALID_FILE_SIZE) {
          $("#edit_location_form .location-picture .error-field.image-size").removeClass("hide");
          $("#edit_location_form .location-picture .image-container").addClass("hide");
      } else {
          $("#edit_location_form .location-picture .image-container").removeClass("hide");
          readImageURL(this,"#edit_location_form #display_edit_location_main_picture");
      }
    })

})
/**************************************************/


/* CREATE EVENT HERE */
$(function() {
  $("#create_event_here_button").click(function() {
      $("#create_activity_modal").modal("show");
      $("#create_activity_form .location-unique-id").val(location_unique_id);
      $("#create_activity_form .activity-location .create-location-link").addClass("hide");
      $("#create_activity_form #id_location").val($("#location_name").val());
      disableInputField($("#create_activity_form #id_location"));
      $("#is_create_event_here_modal").val("true");
  })

  $("#create_activity_modal").on("hidden.bs.modal",function() {
      if ($("#is_create_event_here_modal").val() == "true") {
        $("#create_activity_form .location-unique-id").val("");
        $("#create_activity_form .activity-location .create-location-link").removeClass("hide");
        $("#create_activity_form #id_location").val("");
        enableInputField($("#create_activity_form #id_location"));
        $("#is_create_event_here_modal").val("false");
      }
  }) 
})
/**********************/


/* HANDLE BUSINESS HOUR */
$(function() {

  $("#add_business_hour_btn").click(function(){
      var weekday = $("#weekday_select").val();
      var hour_start = $("#hours_start_select").val();
      var hour_end = $("#hours_end_select").val();
      Dajaxice.frittie.apps.location.add_business_hour(addOrDeleteBusinessHourCallback, {
        'location_unique_id': location_unique_id,
        "weekday": weekday,
        "hour_start": hour_start,
        "hour_end": hour_end
      })
      showAjaxLoadingIcon();
  })

  showBusinesHourDeleteIcon()
})


/* HANDLE DETAIL INFO */
$(function() {
  $("#add_detail_info_btn").click(function() {
    var info_name = $("#detail_info_name_input").val();
    var info_value = $("#detail_info_value_input").val();
    if (stripWhitespace(info_name).length != 0 && stripWhitespace(info_value).length != 0) {
      Dajaxice.frittie.apps.location.add_detail_info(addOrDeleteDetailInfoCallback, {
        "location_unique_id": location_unique_id,
        "info": info_name,
        "value": info_value,
      })
      showAjaxLoadingIcon();
    }
  })
  showDetailInfoDeleteIcon();
})



