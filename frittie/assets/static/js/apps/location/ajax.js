function followCallback(data) {
  hideAjaxLoadingIcon();
  if (data['results']['success']) {
    $("#follow_handler").val(FOLLOWING);
    $("#follow_handler").html('<i class="glyphicon glyphicon-ok"></i>&nbsp;Following');
    $("#follow_handler").removeClass("btn-primary btn-danger");
    $("#follow_handler").addClass("btn-success");
    $("#follower_count").html(data['results']['num_follow'])
  } else {
    showAlertMessage(data['results']['error_message'])
  }
}

function unfollowCallback(data) {
  hideAjaxLoadingIcon();
  if (data['results']['success']) {
    $("#follow_handler").val(NOT_FOLLOW);
    $("#follow_handler").html('Follow');
    $("#follow_handler").removeClass("btn-success btn-danger");
    $("#follow_handler").addClass("btn-primary");
    $("#follower_count").html(data['results']['num_follow'])
  } else {
    showAlertMessage(data['results']['error_message'])
  }
}

function addCommentCallback(data) {
  hideAjaxLoadingIcon();
  if (data['results']['success']) {
      prependNewItem("ul#comment_list",$.trim(data['results']['snippet_return']))
      $('#comment_textarea').val("")
      $(".comments-count").html(data['results']['num_comment'] + " Comments")
  } else {
      location.reload();
  }  
}

function editCommentCallback(data) {
  hideAjaxLoadingIcon();
  if (data['results']['success']) {
      var comment_el = $("li.comment-item[data-unique-id=" + data['results']['comment_unique_id'] + "]");
      var comment_pos = $(comment_el).offset().top;   
      $(comment_el).replaceWith($.trim(data['results']['snippet_return']));
      var new_el = $("li.comment-item[data-unique-id=" + data['results']['comment_unique_id'] + "]");
      $(new_el).effect("highlight", {}, 3000);
      scrollPagePos(comment_pos);
      $("#comment_textarea").val("");
  } else {
      showAlertMessage(data['results']['error_message'])
  }
}

function deleteCommentCallback(data) {
  hideAjaxLoadingIcon();
  if (data['results']['success']) {
      var comment_unique_id = data['results']['comment_unique_id'];
      var comment_el = $("li.comment-item[data-unique-id=" + comment_unique_id + "]")[0]
      scrollPagePos($(comment_el).offset().top);
      $(comment_el).slideUp("normal", function() { $(this).remove(); } );
      $(".comments-count").html(data['results']['num_comment'] + " Comments")
  } else {
      showAlertMessage(data['results']['error_message'])
  }
}

function reportCommentCallback(data) {
  hideAjaxLoadingIcon();
  if (data['results']['success']) {
      showAlertMessage(data['results']['success_message'])
  } 
  // else {
  //     showAlertMessage(data['results']['error_message'])
  // }
}

function loadActivityCallback(data) {
  hideAjaxLoadingIcon();
  if (data['results']['success']) {
      $("#activity_list").html(data['results']['snippet_return']);
      $(".activity-count").html(data['results']['num_activity'] + " Activities");
      $('.sort-link').removeClass("selected");
      $(".sort-link[data-sort=" + data['results']['sort_type'] + "]").addClass("selected") 
      cropImages(".image-container");   
  } else {
      showAlertMessage(data['results']['error_message'])
  }
}

function addOrDeleteBusinessHourCallback(data) {
  hideAjaxLoadingIcon();
  if (data['results']['success']) {
      $("#current_business_hours").html(data['results']['snippet_return']);
      showBusinesHourDeleteIcon()
  } else {
      showAlertMessage(data['results']['error_message'])
  } 
}

function addOrDeleteDetailInfoCallback(data) {
  hideAjaxLoadingIcon();
  if (data['results']['success']) {
      $("#current_detail_info").html(data['results']['snippet_return']);
      showDetailInfoDeleteIcon()
  } else {
      showAlertMessage(data['results']['error_message'])
  } 
}
