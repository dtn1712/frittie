$(function() {
	if ($("#is_activate_message").val() == "True") {
		$("#main_section").css("margin-top","42px");
	}

    $("#fb_share_button").click(function() {
        var width = 500, height = 300, left = (screen.width/2)-(width/2), top = (screen.height/3)-(height/2);
        window.open("https://www.facebook.com/sharer.php?u=http%3A//www.frittie.com/" , "Facemash", "menubar=0,resizable=0,width="+width+",height="+height+",left="+left+",top="+top);
    });


    var lastScrollTop = 0;
    $(window).scroll(function(event){
      if ($(window).scrollTop() > 200) {
        $("#scroll_top_btn").removeClass("hide");
      } else {
        $("#scroll_top_btn").addClass("hide");
      }
    });

    $("#scroll_top_btn").click(function() {
      scrollPageTop();
      $(this).addClass("hide");
    })

});

$(function(){

    if ($("#is_have_friend_list").val() == "True") {
    	showAjaxLoadingIcon();
    	$.getJSON("/friends/facebook",function(data) {
    		var facebook_friends = data['facebook_friends'];
	    	var items = "";
			for (var i = 0; i < facebook_friends.length; i++) {
				var id = facebook_friends[i]['id'];
				var name = facebook_friends[i]['name'];
				console.log(htmlUtf8Decode(name));
				items = items + '<li class="facebook-friend-item row" data-facebook-id="' + id + '">' + 
									"<img class='pull-left' src='http://graph.facebook.com/" + id + "/picture?type=large' width='50' height='50' >" + 
									'<div class="pull-left name">' + htmlUtf8Decode(name) + '</div>' +
									"<div class='pull-right'>" + 
										"<button class='btn btn-primary btn-sm' onclick='sendInviteRequest(this)'>Invite</button>" +
									"</div>" + 
								'</li>'
			}
			$("ul#facebook_friends_list").html(items);
			var existing_friends = data['existing_friends']
			if (existing_friends.length == 0) {
				var no_friend_on_frittie = "<div id='no_friend_on_frittie'>You have no friend on Frittie. Invite them now</div>"
				$("#existing_friends").html(no_friend_on_frittie);
			} else {
				var not_follow_user = "";
				var following_user = "";
				var count_not_follow = 0;
				var count_following = 0;
				for (var i = 0; i < existing_friends.length; i++) {
					var friend = existing_friends[i];
					var username = friend['user_info']['username'];
					var avatar = friend['user_info']['avatar'];
					var user_fullname = friend['user_info']['user_fullname'];
					
					if (friend['is_following'] == false) {
						not_follow_user = not_follow_user + '<li class="existing-friend-item row" data-username="' + username + '">' + 
									"<a href='/people/" + username + "' >" +
										"<img class='pull-left' src='" + avatar + "' width='50' height='50' >" + 
										'<div class="pull-left name">' + htmlUtf8Decode(user_fullname) + '</div>' +
									"</a>" + 
									"<div class='pull-right'>" + 
										"<button class='btn btn-default btn-sm follow-handler-btn' value='not_follow'>Follow</button>" +
									"</div>" + 
								'</li>'
						count_not_follow = count_not_follow + 1;
					} else {
						following_user = following_user + '<li class="existing-friend-item row" data-username="' + username + '">' + 
									"<a href='/people/" + username + "' >" +
										"<img class='pull-left' src='" + avatar + "' width='50' height='50' >" + 
										'<div class="pull-left name">' + htmlUtf8Decode(user_fullname) + '</div>' +
									"</a>" +
									"<div class='pull-right'>" + 
									"<button class='btn btn-primary btn-sm follow-handler-btn' value='following'>Following</button>" +
									"</div>" + 
								'</li>'
						count_following = count_following + 1;

					}
				}
				if (count_not_follow != 0) {
					$("#existing_friends_list").html(not_follow_user);
				}

				if (count_following != 0) {
					var content = "<div class='large-margin-top medium-margin-bottom medium-text'>You're already following " + count_following + " friends (<a class='link-color normal-text' id='toggle_following_friends_list'>hide</a>)</div>" ;
					if (count_following == 1) {
						content = "<div class='large-margin-top medium-margin-bottom medium-text'>You're already following " + count_following + " friend (<a class='link-color normal-text' id='toggle_following_friends_list'>hide</a>)</div>" ;
					}
					$("#existing_friends").append(content);
					$("#existing_friends").append("<ul id='exist_following_friends_list'>" + following_user + "</ul>")
				}
			}
			hideAjaxLoadingIcon();

			$(".follow-handler-btn").click(function() {
				if ($(this).val() == NOT_FOLLOW) {
					Dajaxice.frittie.apps.member.follow_user(followUserCallback, {
						"user_view_username": $(this).closest("li").attr("data-username"),
					})
				} else {
					Dajaxice.frittie.apps.member.unfollow_user(unfollowUserCallback,{
						"user_view_username": $(this).closest("li").attr("data-username"),
					})
				}
			})

			$(".follow-handler-btn").hover(
				function() {
					if ($(this).val() == FOLLOWING) {
						$(this).html("Unfollow");
						$(this).removeClass("btn-primary");
						$(this).addClass("btn-danger");
					}
				},
				function() {
					if ($(this).val() == FOLLOWING) {
						$(this).html("Following");
						$(this).removeClass("btn-danger");
						$(this).addClass("btn-primary");
					}
				}
			)

			$("#toggle_following_friends_list").click(function() {
				var value = $(this).text();
				if (value.toLowerCase() == "hide") {
					$(this).html("show");
					$("#exist_following_friends_list").addClass("hide");
				} else {
					$(this).html("hide");
					$("#exist_following_friends_list").removeClass("hide");
				}
			})
	    })
    }

    $("#facebook_friends input.search").keyup(function() {
    	var query = $("#facebook_friends input.search").val();
    	listFilter('#facebook_friends_list li.facebook-friend-item div.name',query);
    })
})


$(function() {
	$('#import_facebook_btn').click(function(e){
		console.log("yes");
		FB.login(function(response) {
			if (response.authResponse) {
				FB.api('/me', function(info) {
					fflogin(response, info);
				});    
			} else {
			}
		}, 
		{scope:'read_friendlists'});
	});

	$("#no_facebook_friend input.search").keyup(function() {
    	var query = $("#no_facebook_friend input.search").val();
    	listFilter('#facebook_friends_list1 li.facebook-friend-item div.name',query);
    	listFilter('#facebook_friends_list2 li.facebook-friend-item div.name',query);
    })
})