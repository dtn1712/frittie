function requestCallback(response) {}

function sendMultipleInviteRequest() {
	FB.ui({
			method: 'apprequests',
		  	message: 'Join me on Frittie',
	},requestCallback);
} 

function sendInviteRequest(el) {
	var friend_id = $(el).closest("li").attr("data-facebook-id");
	console.log(friend_id)
	var invite_link = WEBSITE_HOMEPAGE + "/accounts/signup";
	console.log(invite_link);
	FB.ui({
			method: 'send',
			name: 'Join me on Frittie.',
			link: invite_link,
			to: friend_id,
	},requestCallback);
}

function fflogin(response, info){
	if (response.authResponse) {
		var accessToken = response.authResponse.accessToken;
		FB.api('/me/friends', function(response) {
			if(response.data) {					
				populateFBFriends(response);
			} else {
				console.log("Error reading friend list");
			}
		});
	}
}

function populateFBFriends(response){
	$(".following-wrap .panel .invite-all").show();					
	var total_friend = response.data.length;                        
	var s1 = "";
	$.each(response.data,function(index,friend) {
		if (index <= total_friend / 2) {							
			var location_name = "";
			s1 = s1 + '<li class="facebook-friend-item row" data-facebook-id="' + friend.id + '">' + 
							"<img class='pull-left' src='http://graph.facebook.com/" + friend.id + "/picture?type=large' width='50' height='50' >" + 
							'<div class="pull-left name">' + friend.name + '</div>' +
							"<div class='pull-right'>" + 
								"<button class='btn btn-primary btn-sm' onclick='sendInviteRequest(this)'>Invite</button>" +
							"</div>" + 
						'</li>'
		}
	});
	$("#facebook_friends_list1").html(s1);
	var s2 = "";
	$.each(response.data,function(index,friend) {
		if (index > total_friend / 2) {								
			var location_name = "";
			s2 = s2 + '<li class="facebook-friend-item row" data-facebook-id="' + friend.id + '">' + 
							"<img class='pull-left' src='http://graph.facebook.com/" + friend.id + "/picture?type=large' width='50' height='50' >" + 
							'<div class="pull-left name">' + friend.name + '</div>' +
							"<div class='pull-right'>" + 
								"<button class='btn btn-primary btn-sm' onclick='sendInviteRequest(this)'>Invite</button>" +
							"</div>" + 
						'</li>'
		}
	});
	$("#facebook_friends_list2").html(s2);
}



