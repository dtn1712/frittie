function followUserCallback(data) {
	hideAjaxLoadingIcon();
	if (data['results']['success']) {
		var el = $("li.existing-friend-item[data-username='" + data['results']['user_view_username'] + "']")[0];
		$($(el).find('button')[0]).html("Following");
		$($(el).find('button')[0]).val(FOLLOWING);
		$($(el).find('button')[0]).removeClass("btn-default btn-danger");
		$($(el).find('button')[0]).addClass("btn-primary");
	} else {
		showAlertMessage(data['results']['error_message']);
	}
}

function unfollowUserCallback(data) {
	hideAjaxLoadingIcon();
	if (data['results']['success']) {
		var el = $("li.existing-friend-item[data-username='" + data['results']['user_view_username'] + "']")[0];
		$($(el).find('button')[0]).html("Follow");
		$($(el).find('button')[0]).val(NOT_FOLLOW);
		$($(el).find('button')[0]).removeClass("btn-primary btn-danger");
		$($(el).find('button')[0]).addClass("btn-default");
	} else {
		showAlertMessage(data['results']['error_message']);
	}
}