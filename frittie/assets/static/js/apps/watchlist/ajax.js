function getWatchListActivitiesCallback(data) {
  hideAjaxLoadingIcon();
  if (data['results']['success']) {
      $('.watchlist-section').html(data['results']['snippet_return']);
      $("#num_activities_load").val(data['results']['num_activities_load'])
      addDotToLongTextWithElementAttr(".add-dot","data-html-maxlength");
      cropImages(".image-container");
  } else {
      showAlertMessage(data['results']['error_message'])
      scrollPageTop();
  }  
}