var geocoder = new google.maps.Geocoder();

$('#create_location_modal').on('shown.bs.modal', function (e) {
    var map_options = {
      zoom: 16,
      scrollwheel: true,
      zoomControl: true,
    }
    var create_activity_map = initializeMap("create_location_map_canvas",map_options);
    if (location.search.indexOf("lat") == -1 ) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var my_position =  new google.maps.LatLng(position.coords.latitude.toFixed(2), position.coords.longitude.toFixed(2));
            create_activity_map.setCenter(my_position);
        });
    } 
}); 

function handleAddressUSA(form,is_USA) {
  if (is_USA) {
      $(form + " .location-state").removeClass("hide");
      $(form + " .location-city").addClass("pull-left");
      $(form + " .location-zipcode label").html("Zip Code*");
      $(form + " .location-city").css("width","200px");
      $(form + " #id_zip_code").attr("required","required");
  } else {
      $(form + " .location-state").addClass("hide");
      $(form + " .location-city").removeClass("pull-left");
      $(form + " .location-zipcode label").html("Zip Code");
      $(form + " .location-city").css("width","100%");
      $(form + " #id_zip_code").removeAttr("required");
      $(form + " .location-zipcode .error-field").addClass("hide");
      $(form + " .location-zipcode .input-field").removeClass("has-error");
  }
}

function validateInputChange(form,el) {
    var is_form_clicked = $($(form).find(".check-form-clicked")[0]).val(); 
    if (is_form_clicked == "true") {
        
        // Clear previous error field
        handleInputError(true,el,"required");
        handleInputError(true,el,"numeric");
        handleInputError(true,el,"unique-id-correct");
        handleInputError(true,el,"valid-url");

        // Validate the input
        validateInput(el,false,"tt-hint")
        var is_valid = validateForm(form,true,"tt-hint");
        if (is_valid) {
          $(form + " .error-notice").addClass("hide");
        } else {
          $(form + " .error-notice").removeClass("hide");
        }
    }
}

function setLatLng(form,lat,lng) {
    $(form + " input[name='lat']").val(lat);
    $(form + " input[name='lng']").val(lng);
}

function reverseGeocode(form,latLng) {
    geocoder.geocode({'latLng': latLng}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[0]) {
              console.log(results[0]);
              var new_address = results[0].formatted_address;
              var address_components = results[0].address_components;

              var address_info = getAddressInfo(address_components);

              var country = address_components[address_components.length - 1]['short_name'];
              // var last_comma_pos = new_address.lastIndexOf(",");
              // var first_comma_pos = new_address.indexOf(",");
              // var second_last_comma_pos = new_address.substring(0,last_comma_pos-1).lastIndexOf(",");
              if (country == "US") {
                  handleAddressUSA(form,true);
                  // var state_and_zip_code = stripWhitespace(new_address.substring(second_last_comma_pos+1,last_comma_pos)).split(" ");
                  // var state = state_and_zip_code[0];
                  // var zip_code = state_and_zip_code[1];
                  if (first_comma_pos == second_last_comma_pos) {
                      //$($(form).find("input[name='city']")[0]).val(new_address.substring(0,first_comma_pos));
                      $($(form).find("input[name='city']")[0]).val(address_info.city);
                      $($(form).find("input[name='address1']")[0]).val("");
                  } else {
                      // $($(form).find("input[name='city']")[0]).val(new_address.substring(first_comma_pos+1,second_last_comma_pos));
                      // $($(form).find("input[name='address1']")[0]).val(new_address.substring(0,first_comma_pos));
                      $($(form).find("input[name='city']")[0]).val(address_info.city);
                      $($(form).find("input[name='address1']")[0]).val(address_info.address);
                  }
                  $($(form).find("input[name='state']")[0]).val(address_info.state);
                  $($(form).find("input[name='zip_code']")[0]).val(address_info.zip_code)
              } else {
                  handleAddressUSA(form,false);
                  $($(form).find("select[name='country']")[0]).val(country);
                  $($(form).find("input[name='address1']")[0]).val(address_info.address);
                  $($(form).find("input[name='zip_code']")[0]).val("");
                  $($(form).find("input[name='city']")[0]).val(address_info.city);
                  // if (second_last_comma_pos != -1) {
                  //     $($(form).find("input[name='city']")[0]).val(new_address.substring(second_last_comma_pos+1,last_comma_pos));
                  // }
              }
          } 
        } 
    });
}

function geocodeAddress(form,map_el,address) {
    geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var map_options = {
                zoom: 16,
                scrollwheel: true,
                zoomControl: true,
            }
            var position = results[0].geometry.location;
            var address_components = results[0].address_components;
            var address_info = getAddressInfo(address_components)
           
            $($(form).find("select[name='country']")[0]).val(address_info.country);
            $($(form).find("input[name='address1']")[0]).val(address_info.address);

            if (address_info.country == "US") {
              handleAddressUSA(form,true);
              $($(form).find("input[name='zip_code']")[0]).val(address_info.zip_code);
            } else {
              handleAddressUSA(form,false);
              $($(form).find("input[name='zip_code']")[0]).val("");
            }
            
            var location_map = initializeMap(map_el,map_options);
            location_map.setCenter(position);
            var marker = new google.maps.Marker({
                map: location_map,
                position: position,
                draggable:true,
                animation: google.maps.Animation.DROP,
            });

            setLatLng(form,position.lat(),position.lng());

            google.maps.event.addListener(marker, "dragend", function(event) {
                reverseGeocode(form,event.latLng);
                setLatLng(form,event.latLng.lat(),event.latLng.lng());
            })
        } 
    });
}

function getLocationFormAddress(form) {
    var address1 = $(form + " input[name='address1']").val();
    var address2 = $(form + " input[name='address2']").val();
    var city = $(form + " input[name='city']").val();
    var country = $(form + " select[name='country']").val();
    var address_value = address1 + " " + address2 + " " + city + " ";
    if (country == "US") {
      var state = $(form + " select[name='state']").val();
      var zip_code = $(form + " input[name='zip_code']").val();
      address_value = address_value + state + " " + zip_code;
    } else {
      address_value = address_value + country; 
    }
    return address_value;
}

function checkGooglePlaceExistCallback(data) {
  var geocoder = new google.maps.Geocoder();
  geocoder.geocode( { 'address': data['results']['address']}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      var address_components = results[0].address_components;
      var address_info = getAddressInfo(address_components);
      var position = results[0].geometry.location;
      $("#create_location_form .google-place-address").val(address_info.address);
      $("#create_location_form #id_city").val(address_info.city);
      $("#create_location_form #id_country").val(address_info.country);
      handleAddressUSA("#create_location_form",address_info.country == "US")
      if (isUndefined(address_info.state) == false) $("#create_location_form #id_state").val(address_info.state);
      if (isUndefined(address_info.state) == false) $("#create_location_form #id_zip_code").val(address_info.zip_code);
      validateInput("#create_location_form #id_address1",false);
      validateInput("#create_location_form #id_city",false);
      validateInput("#create_location_form #id_country",false);
      validateInput("#create_location_form #id_state",false);

      var map_options = {
          zoom: 16,
          scrollwheel: true,
          zoomControl: true,
      }

      var location_map = initializeMap("create_location_map_canvas",map_options);
      location_map.setCenter(position);
      var marker = new google.maps.Marker({
          map: location_map,
          position: position,
          draggable:true,
          animation: google.maps.Animation.DROP,
      });

      setLatLng("#create_location_form",position.lat(),position.lng());

      google.maps.event.addListener(marker, "dragend", function(event) {
          reverseGeocode("#create_location_form",event.latLng);
          setLatLng("#create_location_form",event.latLng.lat(),event.latLng.lng());
      })
    }
  });
  if (data['results']['is_exist']) {
    $("#exist_location_link").attr("href","/location/" + data['results']['location_unique_id']);
    $("#create_location_form .location-exist-error").removeClass("hide");
  } else {
    $("#create_location_form .location-exist-error").addClass("hide");
    $("#create_location_form .is-google-place").val(true);
    $("#create_location_form .google-place-id").val(data['results']['google_place_id']);
  }
}

$(function() {

    var address_picker = new AddressPicker();
    
    $('#create_location_form #id_address1').typeahead(null, 
      {
        name: 'google-place',
        displayKey: 'description',
        source: address_picker.ttAdapter(),
        templates: {
            suggestion: Handlebars.compile([
              '<p class="picture pull-left"><i class="fa fa-map-marker"></i></p>',
              '<p class="name pull-left">{{description}}</p>'
            ].join(''))
        }
      }
    )
    .on('typeahead:selected', function($e,datum) {
        Dajaxice.frittie.apps.location.check_google_place_exist(checkGooglePlaceExistCallback,{
            "google_place_id":datum['id'],
            "address": datum['description']
        });
    });

    // $('#create_location_form #id_address1').bind("typeahead:selected", address_picker.updateMap);
    // $('#create_location_form #id_address1').bind("typeahead:cursorchanged", address_picker.updateMap);

    $("#create_location_form #id_address1").keyup(function() {
        $("#create_location_form .google-place-id").val("");
        $("#create_location_form .is-google-place").val(false);
        $("#create_location_form .google-place-address").val("");
    })

})


$(function() {

    var create_location_form = $("#create_location_form");

    handleAddressUSA("#create_location_form",$("#create_location_form select#id_country").val() == "US");

    $("#create_location_form select#id_country").change(function(){
      handleAddressUSA("#create_location_form",$(this).val() == "US");
    })

    /****** VALIDATE FORM AND INPUT *******/
    $("#create_location_btn").click(function() {
        $("#is_create_location_form_clicked").val("true");
        if ($("#create_location_form .location-exist-error").hasClass("hide")) {
          var is_valid = validateForm(create_location_form,false,"tt-hint");
          if (is_valid) {
            $("#create_location_form .error-notice").addClass("hide");
            $(create_location_form).submit();
            showAjaxLoadingIcon();
          } else {
            $("#create_location_form .error-notice").removeClass("hide");
          }
        }

    })  

    $("#create_location_form input").each(function() {
        $(this).change(function() {
          if ($("#create_location_form .location-exist-error").hasClass("hide")) {
            validateInputChange("#create_location_form",this);
          }
        })
    });

    $("#create_location_form textarea").each(function() {
        $(this).change(function() {
          if ($("#create_location_form .location-exist-error").hasClass("hide")) {
            validateInputChange("#create_location_form",this);
          }
        })
    });

    $("#create_location_modal").on("hidden.bs.modal",function(e) {
      if ($("#create_location_form .error-notice").hasClass("hide") == false) {
        //clearForm(create_location_form);
        $("#create_location_form .error-notice").addClass("hide");
        $("#create_location_form .error-field").addClass("hide");
        $("#create_location_form .has-error").removeClass("has-error");
        $("#create_location_form .location-picture .image-container").addClass("hide");
      }
    })

    $(".address-field").change(function() {
        var address_value = getLocationFormAddress("#create_location_form");
        geocodeAddress("#create_location_form","create_location_map_canvas",address_value);
    })

    $('#create_location_form input[name="main_picture"]').bind('change', function() {
      $("#create_location_form .location-picture .error-field").addClass("hide");
      if (this.files[0].size > VALID_FILE_SIZE) {
          $("#create_location_form .location-picture .error-field.image-size").removeClass("hide");
          $("#create_location_form .location-picture .image-container").addClass("hide");
      } else {
          $("#create_location_form .location-picture .image-container").removeClass("hide");
          readImageURL(this,"#create_location_form #display_location_main_picture");
      }
    })

    // $("#create_location_form #id_country").change(function() {
    //     $("#create_location_form #id_city").val("");
    //     $("#create_location_form #id_address1").val("");
    //     $("#create_location_form #id_address2").val("");
    // })

})