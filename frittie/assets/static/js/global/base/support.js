var is_login = $("#check_login").val();
var user_login_username = (is_login) ? $("#user_login_username").val() : null;

function showMessage(data,el) {
    var new_item = $(data).hide();
    $(el).append(new_item);
    new_item.slideDown();
    setTimeout(function(){
        new_item.slideUp(function(){ 
            jQuery(this).remove(); 
        });
    },3000);
}

// Another function of getting cursor
function getCaret(el) {
  if (el.selectionStart) {
    return el.selectionStart;
  } else if (document.selection) {
    el.focus();

    var r = document.selection.createRange();
    if (r == null) {
      return 0;
    }

    var re = el.createTextRange(),
        rc = re.duplicate();
    re.moveToBookmark(r.getBookmark());
    rc.setEndPoint('EndToStart', re);

    return rc.text.length;
  } 
  return 0;
}

function stripLargeInnerWhitespace(str) {
    return str.replace( /  +/g, ' ' )
}

function stripLargeOuterWhitespace(str) {
   return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '')
}

// Strip whitespace function
function stripWhitespace(str) {
    return stripLargeInnerWhitespace(stripLargeOuterWhitespace(str));
}

// Convert 24hr system to am/pm system 
function convert24hrToAmPm(year,month,day,hour,minute){
    var minute_str = minute.toString();
    if (minute < 10){
        minute_str = "0" + minute.toString();
    }
    if (hour == 0) 
        return "12" + ":" + minute_str+ "am";
    if ((hour > 0) && (hour < 12))
        return hour.toString() + ":" + minute_str + "am";
    if (hour == 12) 
        return "12" + ":" + minute_str + "pm";
    if (hour > 12)
        return (hour-12).toString() + ":" + minute_str + "pm";
}

// Simple function return this type of format: Wednesday, January 13, 2012
function convertDateName(year,month,day,weekday) {
  return week_day_num[weekday] + ", " + month_name_full[month] + " " + day + ", " + year;
}

function isNumber (o) {
  return !isNaN(parseFloat(o));
}

function isEmpty(el) {
    if(($(el).val().length==0) || ($(el).val()==null)) {
      return true;
    }
    return false
}

function isExist(el) {
    if ($(el).length > 0){
      return true
    }
    return false 
}

function isUndefined(el) {
  if (typeof el === "undefined") return true;
  return false;
}

function isVisible(el) {
  if ($(el).hasClass("hide")) return false;
  return true;
}

function isValidEmail(value) {
  var at_pos = value.indexOf("@");
  var dot_pos = value.lastIndexOf(".");
  if (at_pos < 1 || dot_pos < at_pos + 2 || dot_pos + 2 >= value.length){
    return false;
  }
  return true;
}

// Return a datetime format for comment: June 19, 2012, 12:42pm. All the function here can be found in common.js
function standardDatetimeFormat(year,month,day,hour,minute) {
    return month_name_full[month] + " " + day + ", " + year + ", " + convert_24hr_to_AM_PM(year,month,day,hour,minute)['string']
}

function scrollPageTop() {
  $("html, body").animate({scrollTop:0}, '300', 'swing');
}

function scrollPagePos(pos) {
   $("html, body").animate({scrollTop:pos}, '300', 'swing');
}

function stretchWindowMinHeight(el,offset) {
  if(typeof(offset)==='undefined') offset = 0;
  $(el).css("min-height",$(window).height()-offset);
  $(window).resize(function() {
    $(el).css("min-height",$(window).height()-offset);
  })
}

function stretchToElementHeight(el1,el2) {
  $(el1).css("height",$(el2).height());
  $(window).resize(function() {
    $(el1).css("height",$(el2).height());
  })
}

function getMultipleCheckbox(el){
    var result = [];
    $(el).each(function(index, value){  
        var name =  $(this).attr("name");
        result.push(parseInt(name.substring(7,name.length),10));     
    });
    return result
}

function capitalizeFirstLetter(string){
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function setNullForUndefined(variable) {
  if (typeof variable === 'undefined') variable = null; 
  return variable;
}

function setEmptyForUndefined(variable) {
  if (typeof variable === 'undefined') variable = ""; 
  return variable;
}

function setDefaultValueForUndefined(variable,value) {
  if (typeof variable === 'undefined') variable = value; 
  return variable;
}

function appendNewItem(el,data) {
    var new_item = $(data).hide();
    $(el).append(new_item);
    new_item.slideDown();
}

function prependNewItem(el,data) {
    var new_item = $(data).hide();
    $(el).prepend(new_item);
    new_item.slideDown();
}

function showAjaxLoadingIcon(pos) {
    var default_pos = { 
        top:DEFAULT_AJAX_LOADING_ICON_TOP, 
        left: DEFAULT_AJAX_LOADING_ICON_LEFT
    };
    pos = setDefaultValueForUndefined(pos,default_pos)
    $("#ajax_loading_icon").css({
      "top": pos.top + "%",
      "left": pos.left + "%",
    })
    $("#ajax_loading_icon").show();
}

function hideAjaxLoadingIcon() {
   $("#ajax_loading_icon").css({
      "top": DEFAULT_AJAX_LOADING_ICON_TOP + "%",
      "left": DEFAULT_AJAX_LOADING_ICON_LEFT + "%",
   })
   $("#ajax_loading_icon").hide();
}

function addDotToLongTextStringOutput(el,total_char_num) {
    var content = stripWhitespace(el);
    if (content.length > total_char_num) {
      var new_value = content.substring(0,total_char_num) + "...";   
      return new_value;
    }
    return content;
}

function addDotToLongTextWithNumChar(el,total_char_num) {
    $(el).each(function() {
        var content = stripWhitespace($(this).text())
        if (content.length > total_char_num) {
            var last_pos = content.substring(0,total_char_num).lastIndexOf(" ");
            var new_value = content.substring(0,last_pos) + "...";   
            $(this).html(new_value);
        }
    })  
}

function addDotToLongTextWithElementAttr(el,attr_value) {
    $(el).each(function() {
        var content = stripWhitespace($(this).text())
        var total_char_num = $(this).attr(attr_value);
        if (content.length > total_char_num) {
            var last_pos = content.substring(0,total_char_num).lastIndexOf(" ");
            var new_value = content.substring(0,last_pos) + "...";   
            $(this).html(new_value);
        }
    })     
}

function showThumbnail(thumbnail_section_id,file,width,height) {
    if (isUndefined(width)) width = 100;
    if (isUndefined(height)) height = 100;
    var imageType = /image.*/
    if(!file.type.match(imageType)){
        console.log("Not an Image");
    }
    var image = document.createElement("img");
    var thumbnail = document.getElementById(thumbnail_section_id);
    thumbnail.innerHTML = "";
    image.file = file;
    thumbnail.appendChild(image)

    var reader = new FileReader()
    reader.onload = (function(aImg){
        return function(e){
          aImg.src = e.target.result;
        };
    }(image))
    var ret = reader.readAsDataURL(file);
    var canvas = document.createElement("canvas");
    ctx = canvas.getContext("2d");
    image.onload= function(){
        ctx.drawImage(image,width,height)
    }
}

function showThumbnails(thumbnail_section_id,files){
  for (var i = 0; i < files.length; i++) {
      showThumbnail(thumbnail_section_id,files[i]);
  }
}

function readImageURL(input,image_el) {
  if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
          $(image_el).attr('src', e.target.result)
      };
      reader.readAsDataURL(input.files[0]);
  }
}

function dragAndShowThumbnail(upload_area,input_el,thumbnail_el) {
    var fileDiv = document.getElementById(upload_area);
    var fileInput = document.getElementById(input_el);

    console.log(input_el);
    console.log(fileInput);

    fileInput.addEventListener("change",function(e){
        var files = this.files
        showThumbnail(thumbnail_el,files[0])
    },false)

    fileDiv.addEventListener("click",function(e){
        $(fileInput).show().focus().click().hide();
        e.preventDefault();
    },false)

    fileDiv.addEventListener("dragenter",function(e){
        e.stopPropagation();
        e.preventDefault();
    },false);

    fileDiv.addEventListener("dragover",function(e){
        e.stopPropagation();
        e.preventDefault();
    },false);

    fileDiv.addEventListener("drop",function(e){
        e.stopPropagation();
        e.preventDefault();
        var dt = e.dataTransfer;
        var files = dt.files;
        showThumbnail(thumbnail_el,files[0])
    },false);
}

jQuery.fn.outerHTML = function() {
    return jQuery('<div />').append(this.eq(0).clone()).html();
}