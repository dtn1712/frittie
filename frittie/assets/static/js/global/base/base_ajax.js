function getActivityOverlaysCallback(data) {
    hideAjaxLoadingIcon();
   	if (data['results']['success']) {
       	var single_overlays = data['results']['single_overlays'];
      	for (var i = 0; i < single_overlays.length; i++) {
            var item = single_overlays[i];
            var overlay = new ActivitySingleOverlay(item);
            overlay.setMap(map);
            all_overlays.push(overlay);
        }
        var group_overlays = data['results']['group_overlays'];
        for (var i = 0; i < group_overlays.length; i++) {
            var item = group_overlays[i];
            var group_overlay = new ActivityGroupOverlay(item,i);
            group_overlay.setMap(map);
            all_overlays.push(group_overlay);
        }
    }
}

function getPreviewActivityCallback(data) {
    hideAjaxLoadingIcon();
 	  if (data['results']['success']) {
        $("#preview_activity_modal").html(data['results']['snippet_return']).triggerHandler('loadPreviewContent');;
        $("#preview_activity_modal").modal("show");
        $("#preview_activity_modal .watch-action").hover(
          function() {
            if ( $($(this).find(".watch-value")[0]).val() == WATCHING) {
              $("#preview_activity_modal .watch-text-display").html("Remove from watchlist");
            }
          },
          function() {
            if ( $($(this).find(".watch-value")[0]).val() == WATCHING) {
              $("#preview_activity_modal .watch-text-display").html("Watching");
            }
          }
        )

        var show_location_map = function() {
            var position = new google.maps.LatLng($('#preview_activity_modal .activity-location-lat').val(),$("#preview_activity_modal .activity-location-lng").val());
            var map_options = {
              zoom: 16,
              scrollwheel: true,
              zoomControl: true,
            }
            var preview_activity_map = initializeMap("preview_activity_map_canvas", map_options);
            preview_activity_map.setCenter(position);
            var marker = new google.maps.Marker({
                position: position,
                map: preview_activity_map,
                animation: google.maps.Animation.DROP
            });
        }

        $('#preview_activity_modal').bind('loadPreviewContent', function(event, data) {
           if ($("#preview_activity_modal .activity-preview-details .is-location-model").val() == "True") {
              show_location_map();
            }
        }); 

        $("#preview_activity_modal").on('shown.bs.modal',function(e) {
            if ($("#preview_activity_modal .activity-preview-details .is-location-model").val() == "True") {
              show_location_map();
            }   
        });  

  	} else {
    	showAlertMessage(data['results']['error_message']);
  	} 
}

function previewActivityWatchCallback(data) {
    hideAjaxLoadingIcon();
    if (data['results']['success']) {
      $("#preview_activity_modal .watch-value").val(data['results']['watch_value'])
      $("#preview_activity_modal .watch-text-display").html("Watching");
      if (parseInt(data['results']['num_watcher'] > 1)) {
          $("#preview_activity_modal .num-watcher").html(data['results']['num_watcher'] + " watchers");
      } else {
          $("#preview_activity_modal .num-watcher").html(data['results']['num_watcher'] + " watcher");
      }
      $("#preview_activity_modal .watch-action").hover(
        function() {
          if ( $($(this).find(".watch-value")[0]).val() == WATCHING) {
            $("#preview_activity_modal .watch-text-display").html("Remove from watchlist");
          }
        },
        function() {
          if ( $($(this).find(".watch-value")[0]).val() == WATCHING) {
            $("#preview_activity_modal .watch-text-display").html("Watching");
          }
        }
      )
    } else {
      $("#preview_activity_modal").modal("hide");
      showAlertMessage(data['results']['error_message'])
    } 
}

function previewActivityUnwatchCallback(data) {
    hideAjaxLoadingIcon();
    if (data['results']['success']) {
      $("#preview_activity_modal .watch-value").val(data['results']['watch_value'])
      $("#preview_activity_modal .watch-text-display").html("Add to watchlist");
      if (parseInt(data['results']['num_watcher'] > 1)) {
          $("#preview_activity_modal .num-watcher").html(data['results']['num_watcher'] + " watchers");
      } else {
          $("#preview_activity_modal .num-watcher").html(data['results']['num_watcher'] + " watcher");
      }
    } else {
      $("#preview_activity_modal").modal("hide");
      showAlertMessage(data['results']['error_message'])
    } 
}

function joinActivityHandlerCallback(data) {
    hideAjaxLoadingIcon();
    if (data['results']['success']) {
      var action = data['results']['action']
      var send_email = false
      var email_template = "";
      var extra_context = {}

      if (action == "request_join") {
        send_email = true;
        email_template = "request_join_activity";
      } else if (action == "cancel_attending") {
        send_email = true;
        email_template = "cancel_attending_activity";
        extra_context['reason'] = data['results']['reason']
      } 
      
      if (send_email) {
        var unique_id = $("#activity_unique_id").val();
        Dajaxice.frittie.apps.activity.send_email_notify_activity_host(Dajax.process,{
          "unique_id": unique_id,
          "email_template": email_template,
          "extra_context": extra_context
        })
      }
      showAlertMessage(data['results']['success_message'])
    } else {
      showAlertMessage(data['results']['error_message'])
    }
}

function filterMapActivityCallback(data) {
    hideAjaxLoadingIcon();
    if (data['results']['success']) {
      $("#content_area").html(data['results']['snippet_return'])
      addDotToLongTextWithElementAttr(".add-dot","data-html-maxlength");
      for (var i = 0; i < all_overlays.length; i++) {
        all_overlays[i].setMap(null);
      }
      var activity_overlays_data = getListActivityLocationDataMapping();
      Dajaxice.frittie.apps.activity.get_activity_overlays(getActivityOverlaysCallback,{
        'data': activity_overlays_data
      });
    } else {
      showAlertMessage(data['results']['error_message'])
    }
}

