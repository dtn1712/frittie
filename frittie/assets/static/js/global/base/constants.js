var WEBSITE_HOMEPAGE = location.protocol + '//' + location.host;

var STATIC_URL = $("#static_url").val();

var DEFAULT_AJAX_LOADING_ICON_TOP = 35;
var DEFAULT_AJAX_LOADING_ICON_LEFT = 48;

var MONTH_NAME_SHORT = {
      1: "Jan", 
      2: "Feb", 
      3: "Mar", 
      4: "Apr", 
      5: "May", 
      6: "Jun", 
      7: "Jul", 
      8: "Aug", 
      9: "Sep", 
      10: "Oct", 
      11: "Nov", 
      12: "Dec"
};

var MONTH_NAME_FULL = {
      1: "January", 
      2: "February", 
      3: "March", 
      4: "April", 
      5: "May", 
      6: "June", 
      7: "July", 
      8: "August", 
      9: "September", 
      10: "October", 
      11: "November", 
      12: "December"
};

var WEEK_DAY_TEXT = {
    "Mon": "Monday", 
    "Tue": "Tuesday", 
    "Wed": "Wednesday", 
    "Thu": "Thursday", 
    "Fri": "Friday", 
    "Sat": "Saturday", 
    "Sun": "Sunday"
};

var WEEK_DAY_NUM = {
    0: "Monday", 
    1: "Tuesday", 
    2: "Wednesday", 
    3: "Thursday", 
    4: "Friday", 
    5: "Saturday", 
    6: "Sunday"
};

var STATE_NAME = {
   "AL":"Alabama",
   "AK":"Alaska",
   "AZ":"Arizona",
   "AR":"Arkansas",
   "CA":"California",
   "CO":"Colorado",
   "CT":"Connecticut",
   "DE":"Delaware",
   "FL":"Florida",
   "GA":"Georgia",
   "HI":"Hawaii",
   "ID":"Idaho",
   "IL":"Illinois",
   "IN":"Indiana",
   "IA":"Iowa",
   "KS":"Kansas",
   "KY":"Kentucky",
   "LA":"Louisana",
   "ME":"Maine",
   "MD":"Maryland",
   "MA":"Massachusetts",
   "MI":"Michigan",
   "MN":"Minnesota",
   "MS":"Mississippi",
   "MO":"Missouri",
   "MT":"Montana",
   "NE":"Nebraska",
   "NV":"Nevada",
   "NH":"New Hampshire",
   "NJ":"New Jersey",
   "NM":"New Mexico",
   "NY":"New York",
   "NC":"North Carolina",
   "ND":"North Dakota",
   "OH":"Ohio",
   "OK":"Oklahoma",
   "OR":"Oregon",
   "PA":"Pennsylvania",
   "RI":"Rhode Island",
   "SC":"South Carolina",
   "SD":"South Dakota",
   "TN":"Tennessee",
   "TX":"Texas",
   "UT":"Utah",
   "VT":"Vermont",
   "VA":"Virginia",
   "WA":"Washington",
   "WV":"West Virginia",
   "WI":"Wisconsin",
   "WY":"Wyoming"
};

var DEFAULT_MAP_ZOOM = 14;
var DEFAULT_SCROLLWHEEL_SETTING = false;
var DEFAULT_ROTATECONTROL_SETTING = false;
var DEFAULT_STREETVIEW_SETTING = false;
var DEFAULT_PANCONTROL_SETTING = false;
var DEFAULT_ZOOMCONTROL_SETTING = false;

var CURRENT_LATITUDE = parseFloat($("#current_latitude").val());
var CURRENT_LONGITUDE = parseFloat($("#current_longitude").val());

var CURRENT_POSITION = new google.maps.LatLng(CURRENT_LATITUDE, CURRENT_LONGITUDE);

var HOUR_OFFSET = {
    "in": 4, 
    "out": 5
};

/******************************************************/
/*                                                    */
/*  Constant value for daylight saving time in USA    */
/*  DST_start and DST_end define the interval time    */
/*  of daylight saving time. hour_offset define how   */
/*  many hour deduct to get the exact local time      */
/*                                                    */
/******************************************************/
var DST_START = {
    "month": 3, 
    "day": 11
};

var DST_END = {
    "month": 11, 
    "day": 4
};

var WATCHING = "watching";
var NOT_WATCH = "not_watch";

var FOLLOWING = "following";
var NOT_FOLLOW = "not_follow";

var VALID_FILE_SIZE = 5242880

var PREFIX_UNIQUE_ID = "LO,AC,IV,PT,VD,CM,CN,RP,NF,MG,FD,DI,UN"

var IMAGE_FILE_FORMAT = "jpg,png,gif,jpeg"


