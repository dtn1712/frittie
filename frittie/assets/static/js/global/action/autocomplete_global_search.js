var autocomplete_data = new Bloodhound({
  	datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.label); },
  	queryTokenizer: Bloodhound.tokenizers.whitespace,
  	limit: 100,
  	prefetch: {
  		url:"/search/autocomplete/",
  		ttl: 10 * 60 * 1000,
  	}
});
var city_data;
if (location.pathname.indexOf("explore") != -1) {
	city_data = new Bloodhound({
	  	datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.label); },
	  	queryTokenizer: Bloodhound.tokenizers.whitespace,
	  	limit: 100,
	  	prefetch: {
	  		url: "/search/autocomplete/city_explore",
	  		ttl: 365 * 24 * 3600 * 1000,
	  	}
	});
} else {
	city_data = new Bloodhound({
	  	datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.label); },
	  	queryTokenizer: Bloodhound.tokenizers.whitespace,
	  	limit: 100,
	  	prefetch: {
	  		url: "/search/autocomplete/city",
	  		ttl: 365 * 24 * 3600 * 1000,
	  	}
	});
}

autocomplete_data.initialize();
city_data.initialize();

$(function() {


	$("#search_query").keydown(function(e) {
    	if (e.keyCode == 13) {
          	$(this).closest('form').submit();
       	}
   	});

	$('#search_query').typeahead(null, 
	{
	    displayKey: 'label',
	    source: autocomplete_data.ttAdapter(),
	    templates: {
	        suggestion: Handlebars.compile([
	            '<div class="picture pull-left"><img src="{{picture}}"/></div>',
	            '<div class="details pull-left">',
	            	'<div class="name">',
	            		'{{label}}',
	            	'</div>',
	            	'<div class="type">',
	            		'{{type}}',
	            	'</div>',
	            '</div>'
	        ].join(''))
	    }
	},
	{
		displayKey: 'label',
	    source: city_data.ttAdapter(),
	    templates: {
	        suggestion: Handlebars.compile([
	            '<div class="picture pull-left"><img src="{{picture}}"/></div>',
	            '<div class="details pull-left">',
	            	'<div class="name">',
	            		'{{label}}',
	            	'</div>',
	            	'<div class="type">',
	            		'{{type}}',
	            	'</div>',
	            '</div>'
	        ].join(''))
	    }	
	}
	).on('typeahead:selected', function($e,datum) {
		runTopProgressBar();
		window.location.href = datum['url'];
  	});
})