$(function() {

  
  $("#request_join_activity_btn").click(function() {
      var unique_id = $("#activity_unique_id").val();
      var redirect_link = "/activity/" + unique_id;
      checkLogin(redirect_link);
      var is_valid = validateInput("#request_introduction_textarea",false);
      if (is_valid) {
        var introduction = setEmptyForUndefined($("#request_introduction_textarea").val());
        if (stripWhitespace(introduction).length != 0) {
            Dajaxice.frittie.apps.activity.request_join_activity(joinActivityHandlerCallback,{
              "unique_id": unique_id,
              "introduction": introduction
            })
            $("#request_join_activity_modal").modal("hide");
            showAjaxLoadingIcon();
        }
      }
  })

  $("#cancel_request_join_activity_btn").click(function() {
      var unique_id = $("#activity_unique_id").val();
      Dajaxice.frittie.apps.activity.cancel_request_join_activity(joinActivityHandlerCallback, {
        "unique_id": unique_id
      })
      showAjaxLoadingIcon();
  })

  $("#cancel_attending_activity_btn").click(function() {
      var unique_id = $("#activity_unique_id").val();
      var reason = setEmptyForUndefined($("#cancel_reason_textarea").val());
      if (stripWhitespace(reason).length != 0) {
        Dajaxice.frittie.apps.activity.cancel_attending_activity(joinActivityHandlerCallback,{
          "unique_id": unique_id,
          "reason": reason,
        })
        showAjaxLoadingIcon();
        $("#cancel_attending_activity_modal").modal("hide");
      }
  })

})