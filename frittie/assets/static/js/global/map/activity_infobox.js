var activity_info_box;


function hoverActivityItem(exclude_item_number){
    exclude_item_number = setNullForUndefined(exclude_item_number);
    $("li.activity-item").hover(
        function() {
            $(this).css("background-color","#eaeaea");
        },
        function() {
            if ($(this).attr("data-item-number") != exclude_item_number) {
                $(this).css("background-color","white");
            } else {
                $(this).css("background-color","#eaeaea");
            }
        }
    )
}

function hideInfoBox(infobox) {
    $("li.activity-item").css("background-color","white");
    $(infobox).parent().parent().remove();
    activity_info_box = null;
    hoverActivityItem();
}

function highlightActivity(number) {
    $("li.activity-item").css("background-color","white");
    var activity_el = $("li.activity-item[data-item-number=" + number + "]")[0];
    scrollPagePos($(activity_el).offset().top - 60);
    $(activity_el).css("background-color","#eaeaea");
    hoverActivityItem(number);
}


function renderActivityInfoBoxTemplate(data) {
    $("#activity_info_box_template .activity-time").html(data['time']);
    $("#activity_info_box_template .activity-name").html(addDotToLongTextStringOutput(data['name'],35));
    $("#activity_info_box_template .activity-name").attr("onclick","showPreviewActivity('" + data['unique_id'] + "')");
    $("#activity_info_box_template .place-name").html(addDotToLongTextStringOutput(data['place_name'],30));
    $("#activity_info_box_template .place-position").html(addDotToLongTextStringOutput(data['place_position'],50));
    $("#activity_info_box_template .single-overlay").attr("onclick","highlightActivity('" + data['number'] + "')");
    $("#activity_info_box_template .single-overlay").html(data['number']);
}

function showActivityInfoBox(overlay,activity_unique_id) {
    var activity = $("li.activity-item[data-unique-id='" + activity_unique_id + "']")[0];
    var data = {
        "unique_id": activity_unique_id,
        "time" : $($(activity).find(".activity-time span")[0]).html(),
        "name" : $($(activity).find(".activity-title span")[0]).html(),
        "place_name" : $($(activity).find(".place-name")[0]).html(),
        "place_position":  $($(activity).find(".place-position")[0]).html(),
        "number" : $(activity).attr("data-item-number"), 
    }

    renderActivityInfoBoxTemplate(data);
    $("#activity_info_box_template .single-overlay").addClass("hide");
    var box_text = document.createElement("div");
    box_text.className = "info-box-wrapper";
    box_text.id = "box_" + overlay.number;
    box_text.innerHTML = $("#activity_info_box_template").html();

    var lat = overlay.pos.lat();
    var lng = overlay.pos.lng();

    activity_info_box = new InfoBubble({
        minWidth: 232,
        minHeight:113,
        map: map,
        content: box_text,
        position: new google.maps.LatLng(lat,lng + 0.00125),
        shadowStyle: 0,
        padding: 0,
        backgroundColor: "#ffffff",
        borderRadius: 4,
        arrowSize: 15,
        borderWidth: 1,
        borderColor: "#d5d3d0",
        disableAutoPan: true,
        hideCloseButton: true,
        arrowPosition: 50,
        backgroundClassName: 'info-box single-info-box',
        arrowStyle: 0
    });

    activity_info_box.open();

    /*google.maps.event.addListener(map, 'zoom_changed', function() {
        var zoom_value = map.getZoom() - 13;
        console.log(zoom_value);
        console.log("here");
        var new_pos = new google.maps.LatLng(lat,lng - zoom_value * 0.0001);
        console.log(new_pos);
        activity_info_box.setPosition(new_pos);
    });*/
}

        
function showGroupActivitiesInfoBox(group_overlay) {
    var box_html = "";
    var activities = group_overlay.activities;
    for (var i = 0; i < activities.length;i++) {
        var activity_unique_id = activities[i]['activity_unique_id']; 
        var activity = $("li.activity-item[data-unique-id='" + activity_unique_id + "']")[0];
        var data = {
            "unique_id": activity_unique_id,
            "time" : $($(activity).find(".activity-time span")[0]).html(),
            "name" : $($(activity).find(".activity-title span")[0]).html(),
            "place_name" : $($(activity).find(".place-name")[0]).html(),
            "place_position":  $($(activity).find(".place-position")[0]).html(),
            "number" : $(activity).attr("data-item-number"), 
        }
        renderActivityInfoBoxTemplate(data);
        $("#activity_info_box_template .single-overlay").removeClass("hide");
        box_html = box_html + $("#activity_info_box_template").html();
        if (i != activities.length - 1) {
            box_html = box_html + "<hr>";
        }
    }

    var box_text = document.createElement("div");
    
    box_text.className = "info-box-wrapper";
    box_text.id = "box_" + group_overlay.group_id;
    box_text.innerHTML = box_html;

    var lat = group_overlay.pos.lat();
    var lng = group_overlay.pos.lng();

    activity_info_box = new InfoBubble({
        minWidth: 250,
        maxHeight: 250,
        map: map,
        content: box_text,
        position: new google.maps.LatLng(lat,lng + 0.00175),
        shadowStyle: 0,
        padding: 0,
        backgroundColor: "#ffffff",
        borderRadius: 4,
        arrowSize: 15,
        borderWidth: 1,
        borderColor: "#d5d3d0",
        disableAutoPan: true,
        hideCloseButton: true,
        arrowPosition: 50,
        backgroundClassName: 'info-box group-info-box',
        arrowStyle: 0
    });
    
    activity_info_box.open();

}