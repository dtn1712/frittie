google.maps.visualRefresh = true;

var base_map_options = {
		zoom: DEFAULT_MAP_ZOOM,
		scrollwheel: DEFAULT_SCROLLWHEEL_SETTING,
		rotateControl: DEFAULT_SCROLLWHEEL_SETTING,
		streetViewControl:DEFAULT_SCROLLWHEEL_SETTING,
		panControl: DEFAULT_SCROLLWHEEL_SETTING,
		zoomControl: DEFAULT_SCROLLWHEEL_SETTING,
		center: CURRENT_POSITION,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
};

var googleMapKeywords = ['Terms of Use',
						 'Report a map error',
						 'Map data',
						 'Click to see this area on Google Maps'];

var all_overlays = []

function removeGoogleMapTrademark(map_id) {
	var listDiv = $("#" + map_id + " div.gm-style").find("div");
	for (var i = 0; i < listDiv.length; i++) {
			var content = $(listDiv[i]).html();
			for (var j = 0; j < googleMapKeywords.length; j++) {
					if (content.indexOf(googleMapKeywords[j]) !== -1) $(listDiv[i]).remove();
			}    
	}
}

function zoomControlHandler(controlDiv, map) {
	google.maps.event.addDomListener(zoom_out, 'click', function() {
			var currentZoomLevel = map.getZoom();
			if(currentZoomLevel != 0){
					map.setZoom(currentZoomLevel - 1);}     
	});
	google.maps.event.addDomListener(zoom_in, 'click', function() {
			var currentZoomLevel = map.getZoom();
			if(currentZoomLevel != 21){
					map.setZoom(currentZoomLevel + 1);}
	});
}

// Add a marker to the map and push to the array.
function addMarker(map,location,markers) {
	var marker = new google.maps.Marker({
		position: location,
		map: map
	});
	if (typeof markers !== 'undefined') markers.push(marker);
}

// Sets the map on all markers in the array.
function setAllMarker(map,markers) {
	for (var i = 0; i < markers.length; i++) {
		markers[i].setMap(map);
	}
}

// Removes the markers from the map, but keeps them in the array.
function clearAllMarkers(map,markers) {
	setAllMarker(null,markers);
}

// Shows any markers currently in the array.
function showAllMarkers(map,markers) {
	setAllMarker(map,markers);
}

// Remove only one specified marker
function removeMarker(marker) {
	marker.setMap(null);
}

function initializeMap(el,map_options) {
	if (typeof map_options !== 'undefined') {
		 $.extend(base_map_options,map_options);
	}

	var map = new google.maps.Map(document.getElementById(el),base_map_options);

	// Remove Google Trademark
	google.maps.event.addListenerOnce(map, 'idle', function(){
			removeGoogleMapTrademark(el);
	});

	return map;
}

function showActivityOverlayInfo(activity_unique_id,lat,lng) {
	hideInfoBox(".group-info-box");
	hideInfoBox(".single-info-box");
	var overlay_el = $(".overlay[data-position='" + lat + "," + lng + "']")[0];
	var overlay = {
		number: $(overlay_el).text(),
		pos: new google.maps.LatLng(lat,lng)
	}
	showActivityInfoBox(overlay,activity_unique_id);

}

function filterMapActivity(el) {
	var active_el = $(el).parent().find(".active");
	for (var i = 0; i < active_el.length; i++) {
		$(active_el).removeClass("active");
	}
	$(el).addClass("active");

	var filter_when_value = setNullForUndefined($($("#list_filter_when").find(".active")[0]).attr("data-value"));
	var filter_how_value = setNullForUndefined($($("#list_filter_how").find(".active")[0]).attr("data-value"));
	var filter_category_value = setNullForUndefined($($("#activity_category_dropdown").find(".active")[0]).attr("data-value"));

	Dajaxice.frittie.apps.activity.filter_activity(filterMapActivityCallback,{
		"filter_category_value": filter_category_value,
		"filter_how_value": filter_how_value,
		"filter_when_value": filter_when_value,
		"html_template": "list_activity_map",
	})
	showAjaxLoadingIcon();
}

function moveToActivityOverlay(el,class_item) {
	var activity_item = $(el).closest(class_item);
	var lat = parseFloat($($(activity_item).find(".activity-location-lat")[0]).val());
	var lng = parseFloat($($(activity_item).find(".activity-location-lng")[0]).val());
	map.setCenter(new google.maps.LatLng(lat,lng + 0.03));
}

function moveToLocationOverlay(el,class_item) {
	var location_item = $(el).closest(class_item);
	var position = $(location_item).attr("data-position");
	console.log(position);
	var lat = parseFloat(position.substring(0,position.indexOf(",")));
	var lng = parseFloat(position.substring(position.indexOf(",")+1));
	map.setCenter(new google.maps.LatLng(lat,lng + 0.03));
}

