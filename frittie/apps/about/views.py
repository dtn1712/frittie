from django.shortcuts import render_to_response
from django.template import RequestContext, Context, loader

def about_page(request):
    return render_to_response("apps/about/page/about.html",
            		context_instance = RequestContext(request)
    			)
    
def career(request):
    return render_to_response("apps/about/page/career.html",
            		context_instance = RequestContext(request)
    			)

def contact_us(request):
    return render_to_response("apps/about/page/contact_us.html",
            		context_instance = RequestContext(request)
    			)

def help(request):
    return render_to_response("apps/about/page/help.html",
            		context_instance = RequestContext(request)
    			)

def term_service(request):
    return render_to_response("apps/about/page/term_service.html",
            		context_instance = RequestContext(request)
    			)
    
