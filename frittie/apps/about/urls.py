from django.conf.urls import *

urlpatterns = patterns('frittie.apps.about.views',
   url(r"^$", "about_page", name="about_page"),
   url(r'^career/$','career',name="career"),
   url(r'^contact_us/$','contact_us',name="contact_us"),
   url(r'^help/$','help',name="help"),
   url(r'^term_service/$','term_service',name="term_service"),
)
