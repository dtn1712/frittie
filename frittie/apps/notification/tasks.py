from __future__ import absolute_import

from celery import shared_task

from frittie.apps.notification.helper import generate_notification_helper

@shared_task
def send_notification(subj,action,obj,force=False,extra_context=None,template=None):
	generate_notification_helper(subj,action,obj,force,extra_context,template)
