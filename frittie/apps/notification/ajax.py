from dajax.core import Dajax
from dajaxice.decorators import dajaxice_register

from django.http import HttpResponseRedirect
from django.utils import simplejson
from django.contrib.auth.models import User

from frittie.apps.app_helper import get_user_login_object, generate_html_snippet, generate_message
from frittie.apps.main.models import Location, Notification, Message 
from frittie.apps.app_settings import NOTIFICATION_PAGE
from frittie.apps.main.constants import LIST_NOTIFICATION

import datetime, json

# @dajaxice_register
# def load_more_notification(request):
# 	results = {}
# 	try:
# 		user_login = get_user_login_object(request)
# 		new_notifications = Notification.objects.filter(notify_to=user_login,notification_type=notification_type,status="1")
# 		old_notifications = Notification.objects.filter(notify_to=user_login,notification_type=notification_type,status="0").exclude(pk__in=new_notifications.all().values_list('pk',flat=True)).order_by("-date")[:20]
# 		results['success'] = True
# 		results['notification_type'] = notification_type
# 		results['new_notifications_count'] = len(new_notifications)
# 		results['snippet_return'] = generate_html_snippet(request,NOTIFICATION_PAGE[LIST_NOTIFICATION],{
# 				"new_notifications": new_notifications,
# 				"old_notifications": old_notifications, 
# 		})
# 		for notification in new_notifications:
# 			notification.status = "0"
# 			notification.save()
# 	except Exception as e:
# 		results['success'] = False
# 		results['error_message'] = generate_message("ajax_request","fail",{})	
# 	return json.dumps({"results": results})


