from django.conf.urls import *
from frittie import settings

urlpatterns = patterns('frittie.apps.feed.views',
    url(r"^$", "main_page",name="feed_main"),
)
