from __future__ import absolute_import

from celery import shared_task

from frittie.apps.feed.helper import generate_feed_helper

@shared_task
def create_feed(subj,action,obj):
	generate_feed_helper(subj,action,obj)
