from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.template import RequestContext

from frittie.apps.app_helper import get_user_login_object

from helper import get_feeds

@login_required()
def main_page(request):
	feeds = get_feeds(request)
	is_no_feed = True
	if len(feeds) > 0:
		is_no_feed = False
	return render_to_response("apps/feed/page/main_page.html",{
			"is_no_feed": is_no_feed,
			"feeds": feeds,
		},context_instance=RequestContext(request))




	