from django.template.loader import render_to_string

from frittie.apps.app_helper import generate_unique_id, get_user_login_object
from frittie.apps.main.models import Feed
from frittie.apps.app_settings import FEED_SNIPPET_TEMPLATE

import uuid

'''
	@param: subj - The subject of the feed. 
				   A dictionary which have required 
				   main subject and related subject
	
	@param:	action - The action of the feed.
	
	@param: obj - The object of the feed.
				  A dictionary which have required
				  main object, object type, display 
				  name 
'''
def generate_feed_helper(subj,action,obj):
	data = {
		"subject": subj['main'],
		"action": action,
		"object": obj['main'],
		"object_type": obj['object_type']
	}

	# If user login is private user, dont generate feed
	if subj['main'].get_profile().privacy_status == "0": return 

	if len(subj['related']) > 0:
		template = action + "_" + obj['display'] 
		feed_content = render_to_string(FEED_SNIPPET_TEMPLATE[template],data)
		feed = Feed.objects.create(unique_id=generate_unique_id("feed"),content=feed_content,
					object_type=obj['object_type'],create_by=subj['main'],
					object_unique_id=obj['main'].unique_id)
		feed.user_receive.add(*subj['related'])
		feed.save()

def get_feeds(request):
	user_login = get_user_login_object(request)
	return Feed.objects.filter(user_receive=user_login).order_by('-date')


def convert_list(data):
	d=[]
	for i in range(0,len(data)):
		d.append(data[i]['fields'])
		i = json.dumps(data[i]['fields'])

	return json.dumps(d)

def convert_list_pictures(user_profile):
	data = user_profile.feed.all()
	pictures_url = []
	for i in data :
		photo_url = get_photo_url(i.photo_id) 
		pictures_url.append(photo_url)
	pictures_url = json.dumps(pictures_url)
	return pictures_url

def add_number_of_feed(data):
	d=[]
	for i in range(0,len(data)):
		a=  data[i]['fields']
		a= json.dumps(a)
		a= json.loads(a)
		print a['photo_id']
		photo_url =  get_photo_url(str(a['photo_id']))
		a['id']= i
		a['photo_url'] = photo_url
		d.append(a)
		#a.append({'id':i })
	return d