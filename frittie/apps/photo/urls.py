from django.conf.urls import *
from django.conf import settings

urlpatterns = patterns('frittie.apps.photo.views',
	url(r'^activity/(?P<activity_unique_id>\w+)',"activity_photo_page"),
	url(r'^location/(?P<location_unique_id>\w+)','location_photo_page'),
	url(r'^add/$','add_photo'),
	url(r'^(?P<photo_unique_id>\w+)/delete','delete_photo'),
)