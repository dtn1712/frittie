from django import forms
from django.contrib.auth.models import User

from frittie.apps.main.models import Photo

import datetime

class AddPhotoForm(forms.Form):
    #filename = forms.CharField(max_length=30, label=u'Photo Name')
    photo_name = forms.CharField(max_length=30,label=u'Photo Name')
    time = forms.DateTimeField(initial=datetime.date.today)
    image = forms.ImageField(label='Avatar',required=False)
    unique_id = forms.CharField(max_length=60)