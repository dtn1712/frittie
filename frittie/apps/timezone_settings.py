TIMEZONE_TO_UTC = {
	"America/Chicago" : -5,
	"America/New_York" : -4,
	"America/Los_Angeles" : -7,
	"Asia/Seoul" : 9 ,
	"Asia/Singapore" : 8,
	"Asia/Phnom_Penh" : 7,
	"Asia/Tokyo" : 9,
} 
