# Scrapy settings for crawler project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/topics/settings.html
#

BOT_NAME = 'crawler'

SPIDER_MODULES = ['crawler.spiders']
NEWSPIDER_MODULE = 'crawler.spiders'

ITEM_PIPELINES = [ 'crawler.pipelines.CrawlerPipeline']

import os
#os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
import sys
#PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

#sys.path.append(PROJECT_ROOT)
#SETTING_PATH = os.path.abspath(__file__ + "/../../")
#sys.path.append(SETTING_PATH)
#os.environ['DJANGO_SETTINGS_MODULE'] = 'frittie.settings'

def setup_django_env(path):
    import imp, sys
    from django.core.management import setup_environ

    #f, filename, desc = imp.find_module('settings', [path])
    print 'File information'
    #print f
    #print filename
    #print desc
    print '~~~~~~~~~~'
    #project = imp.load_module('settings', f, filename, desc)       

    #setup_environ(project)

    # Add path's parent directory to sys.path
    sys.path.append(os.path.abspath(os.path.join(path, os.path.pardir)))

current_dir = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
setup_django_env(os.path.abspath("../../"))

ROOT_PATH = os.path.dirname(__file__)


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME':  os.path.join(ROOT_PATH, "db/frittie.db"),
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}

# import dj_database_url
# DATABASES['default'] = dj_database_url.config(default='sqlite://db/sqlite3.db')