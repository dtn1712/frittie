from django.contrib.gis.geoip import GeoIP

from frittie.apps.main.constants import SITE_DATA, ACTIVITY_CATEGORY_MAP
from frittie.apps.main.constants import FILTER_HOW_MAP, FILTER_WHEN_MAP
from frittie.apps.main.constants import DEFAULT_LATITUDE, DEFAULT_LONGITUDE
from frittie.apps.main.constants import DEFAULT_CITY, DEFAULT_COUNTRY
from frittie.apps.main.constants import LOCATION_CATEGORY_MAP

from frittie.apps.app_helper import handle_request_get_message, get_user_login_object
from frittie.apps.app_helper import capitalize_first_letter, get_client_ip, get_short_url
from frittie.apps.message.helper import get_new_messages_count
from frittie.apps.notification.helper import get_new_notifications_count
from frittie.apps.feed.helper import get_feeds
from frittie.apps.activity.forms import ActivityForm
from frittie.apps.location.forms import LocationForm
from frittie.apps.main.models import Notification

from frittie.settings import SITE_NAME, STAGE

from helper import activate_email_reminder_message

import datetime

def site_data(request):
	return SITE_DATA 

def global_data(request):
	results = {}
	client_ip = get_client_ip(request)
	results['client_ip'] = client_ip
	g = GeoIP()
	results['current_lat'] = DEFAULT_LATITUDE
	results['current_lng'] = DEFAULT_LONGITUDE
	results['current_city'] = DEFAULT_CITY
	results['current_country'] = DEFAULT_COUNTRY
	results['geo_data'] = None
	if g.city(client_ip) != None:
		geo_data = g.city(client_ip)
		results['current_lat'] = geo_data['latitude']
		results['current_lng'] = geo_data['longitude']
		results['current_city'] = geo_data['city']
		results['current_country'] = geo_data['country_name']
		results['geo_data'] = geo_data
	results['user_login'] = get_user_login_object(request)
	activate_message = activate_email_reminder_message(request,results['user_login']) 
	if activate_message == None:
		data = { 'user_login':results['user_login'], 'site_name': capitalize_first_letter(SITE_NAME) }
		results["request_message"] = handle_request_get_message(request,data)
		results['is_activate_message'] = False
	else:
		results["request_message"] = activate_message
		results['is_activate_message'] = True
	if results['user_login']:
		results['new_notifications_count'] = get_new_notifications_count(request)
		results['new_messages_count'] = get_new_messages_count(request)
		results['total_new_notifications_and_messages'] = results['new_notifications_count'] + results['new_messages_count']
		#results['feeds'] = get_feeds(request)
	results['create_activity_form'] = ActivityForm()
	results['create_location_form'] = LocationForm()
	results['now'] = datetime.datetime.now()
	results['get_parameter'] = request.GET
	results['activity_categories'] = ACTIVITY_CATEGORY_MAP
	results['activity_filter_how'] = FILTER_HOW_MAP
	results['activity_filter_when'] = FILTER_WHEN_MAP
	results['location_categories'] = LOCATION_CATEGORY_MAP
	results['stage'] = STAGE
	results['short_url'] = get_short_url(request)
	return results


