from django import template
from django.utils import timezone

from decimal import Decimal

from frittie.apps.app_helper import convert_24hr_to_ampm
from frittie.apps.main.constants import MONTH_SHORT_MAP, DAY_SUFFIX, DAY_IN_MONTH_MAP
from frittie.apps.main.constants import WEEK_DAY_NUM_MAP, MONTH_FULL_MAP
from frittie.apps.main.utils import get_elapse_time_text

import datetime

register = template.Library()

def valid_numeric(arg):
	if isinstance(arg, (int, float, Decimal)):
		return arg
	try:
		return int(arg)
	except ValueError:
		return float(arg)


@register.filter
def sub(value, arg):
	"""Subtract the arg from the value."""
	try:
		return valid_numeric(value) - valid_numeric(arg)
	except (ValueError, TypeError):
		try:
			return value - arg
		except Exception:
			return ''
sub.is_safe = False


@register.filter
def multiply(value, arg):
	"""Multiply the arg with the value."""
	try:
		return valid_numeric(value) * valid_numeric(arg)
	except (ValueError, TypeError):
		try:
			return value * arg
		except Exception:
			return ''
multiply.is_safe = False


@register.filter
def div(value, arg):
	"""Divide the arg by the value."""
	try:
		return valid_numeric(value) / valid_numeric(arg)
	except (ValueError, TypeError):
		try:
			return value / arg
		except Exception:
			return ''
div.is_safe = False


@register.filter(name="abs")
def absolute(value):
	"""Return the absolute value."""
	try:
		return abs(valid_numeric(value))
	except (ValueError, TypeError):
		try:
			return abs(value)
		except Exception:
			return ''
absolute.is_safe = False


@register.filter
def mod(value, arg):
	"""Return the modulo value."""
	try:
		return valid_numeric(value) % valid_numeric(arg)
	except (ValueError, TypeError):
		try:
			return value % arg
		except Exception:
			return ''
mod.is_safe = False

@register.filter
def istoday(value):
	today = timezone.datetime.today()
	if today.day == value.day and today.month == value.month and today.year == value.year:
		return True
	return False


@register.filter
def istomorrow(value):
	today = timezone.datetime.today()
	if value.day == today.day + 1 and today.month == value.month and today.year == value.year:
		return True
	if today.day == DAY_IN_MONTH_MAP[today.month] and value.month == today.month + 1 and value.day == 1:
		return True
	return False


@register.filter
def ampm(value):
	return convert_24hr_to_ampm(value).upper()


@register.filter
def date(value):
	month = MONTH_SHORT_MAP[value.month]
	suffix = "th"
	if int(value.day) in DAY_SUFFIX:
		suffix = DAY_SUFFIX[int(value.day)]
	return month + " " + str(value.day) + suffix + ", " + str(value.year) 


@register.filter
def first_char_upper(value):
	return value[0].upper() + value[1:]


@register.filter
def words_first_char_upper(value):
	words = value.split()
	result = ""
	for word in words:
		new_word = word[0].upper() + word[1:]
		result += new_word + " "
	return result[:len(result)-1]


@register.filter
def us_date_format(value):
	return value.strftime("%m-%d-%Y")
	

@register.filter
def display_datetime(value):
	return value


@register.filter
def isfollowing(value,arg):
	return value.get_profile().is_following(arg)


@register.filter
def display_month(value):
	return MONTH_SHORT_MAP[value.month]


@register.filter
def display_full_month(value):
	return MONTH_FULL_MAP[value.month]


@register.filter
def display_weekday(value):
	return WEEK_DAY_NUM_MAP[value.weekday()]

@register.filter
def display_weekday_from_int(value):
	return WEEK_DAY_NUM_MAP[int(value)]

@register.filter
def display_elapse_time(value):
	return get_elapse_time_text(value)


@register.filter
def display_price(value,arg):
	if arg.lower() == "usd":
		return format(value, ',.2f')
	else:
		return format(value,",")

@register.filter
def get_dictionary_item_value(value, arg):
    return value[arg]

@register.filter
def get_total_cost_per_buyer(value,arg):
	buyer = value
	ticket_transactions = arg
	total = 0
	currency = "USD"
	for ticket_transaction in ticket_transactions:
		currency = ticket_transaction.ticket.currency
		if ticket_transaction.user.username == buyer.username:
			total += ticket_transaction.total_cost
	return str(format(total,",.2f")) + " " + currency




