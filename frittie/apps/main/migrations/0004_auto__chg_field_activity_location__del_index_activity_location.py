# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Renaming column for 'Activity.location' to match new field type.
        db.rename_column(u'main_activity', 'location_id', 'location')
        # Changing field 'Activity.location'
        db.alter_column(u'main_activity', 'location', self.gf('django.db.models.fields.CharField')(max_length=300))
        # Removing index on 'Activity', fields ['location']
        #db.delete_index(u'main_activity', ['location_id'])


    def backwards(self, orm):
        # Adding index on 'Activity', fields ['location']
        #db.create_index(u'main_activity', ['location_id'])


        # Renaming column for 'Activity.location' to match new field type.
        db.rename_column(u'main_activity', 'location', 'location_id')
        # Changing field 'Activity.location'
        db.alter_column(u'main_activity', 'location_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['main.Location']))

    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'main.activity': {
            'Meta': {'ordering': "['name']", 'object_name': 'Activity'},
            'activity_requests': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'activity_requests'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['main.ActivityRequest']"}),
            'activity_type': ('django.db.models.fields.CharField', [], {'default': "'1'", 'max_length': '20'}),
            'category': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'comments': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'activity_comment'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['main.Comment']"}),
            'confirmations': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'activity_confirmation'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['main.ConfirmationActivity']"}),
            'create_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_publish': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_repeated': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_update': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'limit': ('django.db.models.fields.IntegerField', [], {'default': '-1'}),
            'list_photos': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'activity_photos'", 'blank': 'True', 'to': u"orm['main.Photo']"}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'logo': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'logo'", 'null': 'True', 'to': u"orm['main.Photo']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'posts': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'activity_posts'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['main.Post']"}),
            'reports': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'activity_reports'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['main.Report']"}),
            'sell_ticket': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'tags'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['main.Tag']"}),
            'tickets': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'ticket'", 'blank': 'True', 'to': u"orm['main.Ticket']"}),
            'timezone': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'tokens': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'activity_tokens'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['main.GeneralToken']"}),
            'unique_id': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'user_create': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'user_create'", 'to': u"orm['auth.User']"}),
            'users_watch': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'user_watch'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['auth.User']"})
        },
        u'main.activitycancelattending': {
            'Meta': {'object_name': 'ActivityCancelAttending'},
            'activity': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'activity_cancel'", 'to': u"orm['main.Activity']"}),
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reason': ('django.db.models.fields.TextField', [], {}),
            'user_cancel': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'activity_user_camcel'", 'to': u"orm['auth.User']"})
        },
        u'main.activityrequest': {
            'Meta': {'object_name': 'ActivityRequest'},
            'activity': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Activity']"}),
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'introduction': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'is_invite_request': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'request_status': ('django.db.models.fields.CharField', [], {'default': "'0'", 'max_length': '10'}),
            'user_request': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'activity_request_user'", 'to': u"orm['auth.User']"})
        },
        u'main.activityuserrelation': {
            'Meta': {'object_name': 'ActivityUserRelation'},
            'activity': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['main.Activity']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'main.comment': {
            'Meta': {'object_name': 'Comment'},
            'comment_type': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'content': ('django.db.models.fields.TextField', [], {}),
            'create_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'edit_date': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reports': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'comment_reports'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['main.Report']"}),
            'unique_id': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'main.confirmationactivity': {
            'Meta': {'object_name': 'ConfirmationActivity'},
            'activity': ('django.db.models.fields.related.ForeignKey', [], {'max_length': '100', 'to': u"orm['main.Activity']", 'null': 'True', 'blank': 'True'}),
            'confirmation_id': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_confirmed': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'user_confirm': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'confirmation_user'", 'to': u"orm['auth.User']"})
        },
        u'main.conversation': {
            'Meta': {'object_name': 'Conversation'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latest_message': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'latest_message'", 'null': 'True', 'to': u"orm['main.Message']"}),
            'messages': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'messages'", 'null': 'True', 'to': u"orm['main.Message']"}),
            'unique_id': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'user1': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'user1'", 'to': u"orm['auth.User']"}),
            'user2': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'user2'", 'to': u"orm['auth.User']"})
        },
        u'main.emailtracking': {
            'Meta': {'object_name': 'EmailTracking'},
            'context_data': ('django.db.models.fields.TextField', [], {}),
            'email_template': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'send_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'pending'", 'max_length': '50'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '350', 'blank': 'True'}),
            'text_content': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'to_emails': ('django.db.models.fields.TextField', [], {})
        },
        u'main.feed': {
            'Meta': {'object_name': 'Feed'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'create_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'feed_create_by'", 'to': u"orm['auth.User']"}),
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_type': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'object_unique_id': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'unique_id': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'user_receive': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'feed_user_receive'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['auth.User']"})
        },
        u'main.generaltoken': {
            'Meta': {'object_name': 'GeneralToken'},
            'create_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'token': ('django.db.models.fields.TextField', [], {}),
            'token_type': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'main.invitation': {
            'Meta': {'object_name': 'Invitation'},
            'activity': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'activity_invitation'", 'to': u"orm['main.Activity']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'list_photos': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['main.Photo']", 'symmetrical': 'False', 'blank': 'True'}),
            'list_videos': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['main.Video']", 'symmetrical': 'False', 'blank': 'True'}),
            'message': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'send_from': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'send_from'", 'to': u"orm['auth.User']"}),
            'send_to_email': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'unique_id': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'main.location': {
            'Meta': {'ordering': "['name']", 'object_name': 'Location'},
            'address1': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'address2': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'category': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'comments': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'location_comment'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['main.Comment']"}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'create_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'create_by'", 'to': u"orm['auth.User']"}),
            'description': ('django.db.models.fields.TextField', [], {}),
            'follow_by': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'follow_by'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['auth.User']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lat': ('django.db.models.fields.DecimalField', [], {'max_digits': '19', 'decimal_places': '10'}),
            'list_photos': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'location_photos'", 'blank': 'True', 'to': u"orm['main.Photo']"}),
            'lng': ('django.db.models.fields.DecimalField', [], {'max_digits': '19', 'decimal_places': '10'}),
            'main_picture': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'main_picture'", 'null': 'True', 'to': u"orm['main.Photo']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'reports': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'location_reports'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['main.Report']"}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '2', 'blank': 'True'}),
            'unique_id': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'website_link': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'zip_code': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'})
        },
        u'main.locationbusinesshour': {
            'Meta': {'object_name': 'LocationBusinessHour'},
            'hour_end': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'hour_start': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'location_open_hour'", 'to': u"orm['main.Location']"}),
            'weekday': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        u'main.locationdetailinfo': {
            'Meta': {'object_name': 'LocationDetailInfo'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'info': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'location_detail_info'", 'to': u"orm['main.Location']"}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'main.message': {
            'Meta': {'object_name': 'Message'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'1'", 'max_length': '1'}),
            'unique_id': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'user_receive': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'user_receive'", 'to': u"orm['auth.User']"}),
            'user_send': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'user_send'", 'to': u"orm['auth.User']"})
        },
        u'main.notification': {
            'Meta': {'object_name': 'Notification'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'notification_type': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True'}),
            'notify_from': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'notify_from'", 'to': u"orm['auth.User']"}),
            'notify_to': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'notify_to'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['auth.User']"}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'1'", 'max_length': '1'}),
            'unique_id': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'main.photo': {
            'Meta': {'object_name': 'Photo'},
            'caption': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'object_unique_id': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'photo_type': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'unique_id': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'upload_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'user_post': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'main.post': {
            'Meta': {'object_name': 'Post'},
            'content': ('django.db.models.fields.TextField', [], {}),
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'object_unique_id': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'post_type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'unique_id': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'main.report': {
            'Meta': {'object_name': 'Report'},
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'report_content': ('django.db.models.fields.TextField', [], {}),
            'report_type': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'unique_id': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'user_report': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'user_report'", 'blank': 'True', 'to': u"orm['auth.User']"})
        },
        u'main.socialfriendlist': {
            'Meta': {'object_name': 'SocialFriendList'},
            'friends': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user_social_auth': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'social_account'", 'unique': 'True', 'to': u"orm['socialaccount.SocialAccount']"})
        },
        u'main.tag': {
            'Meta': {'object_name': 'Tag'},
            'content': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user_tag': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'user_tag'", 'null': 'True', 'to': u"orm['auth.User']"})
        },
        u'main.ticket': {
            'Meta': {'object_name': 'Ticket'},
            'currency': ('django.db.models.fields.CharField', [], {'default': "'USD'", 'max_length': '3'}),
            'fee': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'maximum_quantity_transaction': ('django.db.models.fields.IntegerField', [], {}),
            'price': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'ticket_name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'ticket_status': ('django.db.models.fields.CharField', [], {'default': "'1'", 'max_length': '1'}),
            'total_available_quantity': ('django.db.models.fields.IntegerField', [], {})
        },
        u'main.tickettransaction': {
            'Meta': {'object_name': 'TicketTransaction'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'quantity': ('django.db.models.fields.IntegerField', [], {}),
            'ticket': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'transaction_ticket'", 'to': u"orm['main.Ticket']"}),
            'total_cost': ('django.db.models.fields.DecimalField', [], {'max_digits': '10', 'decimal_places': '2'}),
            'transaction_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'transaction_users'", 'to': u"orm['auth.User']"})
        },
        u'main.userprofile': {
            'Meta': {'object_name': 'UserProfile'},
            'authorized_account': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'avatar': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'avatar'", 'null': 'True', 'to': u"orm['main.Photo']"}),
            'basic_info': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'cover_picture': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'cover_picture'", 'null': 'True', 'to': u"orm['main.Photo']"}),
            'date_of_birth': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'facebook_id': ('django.db.models.fields.CharField', [], {'max_length': '40', 'null': 'True', 'blank': 'True'}),
            'followers': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'followers'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['auth.User']"}),
            'following': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'following'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['auth.User']"}),
            'gender': ('django.db.models.fields.CharField', [], {'default': "'m'", 'max_length': '1'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'invitations': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'invitation'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['main.Invitation']"}),
            'privacy_status': ('django.db.models.fields.CharField', [], {'default': "'1'", 'max_length': '1'}),
            'reports': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'user_reports'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['main.Report']"}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '2', 'blank': 'True'}),
            'timezone': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'profile'", 'unique': 'True', 'to': u"orm['auth.User']"}),
            'watchlist': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'watchlist'", 'blank': 'True', 'to': u"orm['main.Activity']"})
        },
        u'main.video': {
            'Meta': {'object_name': 'Video'},
            'filename': ('django.db.models.fields.CharField', [], {'max_length': '60', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key_data': ('django.db.models.fields.CharField', [], {'max_length': '90', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'unique_id': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'upload_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'user_post': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'video': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'video_type': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'socialaccount.socialaccount': {
            'Meta': {'unique_together': "(('provider', 'uid'),)", 'object_name': 'SocialAccount'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'extra_data': ('allauth.socialaccount.fields.JSONField', [], {'default': "'{}'"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'provider': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'uid': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        }
    }

    complete_apps = ['main']