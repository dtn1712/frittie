########################################
#                                      #
#           CONSTANT DATA              #
#                                      #
########################################
from frittie.settings import SITE_NAME, FACEBOOK_APP_ID
from frittie.apps.app_helper import setup_constant_countries_alpha2, setup_constant_countries_alpha3

import datetime

LOGO_ICON_URL = "https://frittie.s3.amazonaws.com/img/ico/favicon.ico"

ACTIVITY = "activity"
LOCATION = "location"
COMMENT = "comment"
USER_PROFILE = "user_profile"

LIST_ACTIVITY = "list_activity"
LIST_NOTIFICATION = "list_notification"
LIST_USER = "list_user"
LIST_LOCATION = "list_location"
LIST_MEDIA = "list_media"
LIST_MEDIA_THUMBNAIL = "list_media_thumbnail"
LIST_TICKET = "list_ticket"

LOCATION_PRIVATE_PLACE = "7"

COMMENT_ITEM = "comment_item"
PREVIEW_CALCULATE_TICKET_ITEM = "preview_calculate_ticket_item"
WATCH_LIST_ITEM = "watch_list_item"
MESSAGE_ITEM = "message_item"
CONVERSATION_ITEM = "conversation_item"

OBJECT_VERB = "have"

SITE_NAME_INITIAL_CAPITAL = SITE_NAME[0].upper() + SITE_NAME[1:]

WATCHING = "watching"
NOT_WATCH = "not_watch"

CURRENCY = (
    ("USD","UNITED_STATE_DOLLAR"),
    ("VND","VIETNAM_DONG"),
    # ("SGD","SINGAPORE_DOLLAR"),
    # ("JPY","JAPANESE_YEN"),
    # ("KRW","SOUTH_KOREAN_WON"),
)

SHORTCUT_CURRENCY = (
    ("USD"),
    ("VND"),
    # ("SGD"),
    # ("JPY"),
    # ("KRW"),
)

DEFAULT_END_DATE = datetime.datetime(2100,1,1)
DEFAULT_FILTER_WHEN = 0       # Defaul time which is starting from now
DEFAULT_FILTER_HOW = 1        # Latest
DEFAULT_FILTER_CATEGORY = "-1"    # All category 
DEFAULT_LIST_ACTIVITY_HTML_TEMPLATE = "list_activity_map"
DEFAULT_LIST_LOCATION_HTML_TEMPLATE = "list_location_map"
DEFAULT_LATITUDE = 47.616000
DEFAULT_LONGITUDE = -122.322464
DEFAULT_CITY = "Philadelphia"
DEFAULT_COUNTRY = "United States"
DEFAULT_TIMEZONE = "America/New_York"
DEFAULT_SERVER_EMAIL = "team@frittie.com"
DEFAULT_SITE_NAME = 'frittie'

SITE_DATA = {
  "SITE_NAME" : SITE_NAME,
  "SITE_NAME_INITIAL_CAPITAL": SITE_NAME_INITIAL_CAPITAL, 
  "SITE_DESCRIPTION": SITE_NAME_INITIAL_CAPITAL + " is a web application provide users opportunity to connect with other people by joining in activity in their favourite location",
  "SHORTCUT_CURRENCY" : SHORTCUT_CURRENCY ,
  "FACEBOOK_APP_ID": FACEBOOK_APP_ID,
  "LOCATION_PRIVATE_PLACE": LOCATION_PRIVATE_PLACE,
  "DEFAULT_TIMEZONE": DEFAULT_TIMEZONE
}

REQUEST_RESERVED = "2"
REQUEST_ACCEPT = "1"
REQUEST_WAITING = "0"
REQUEST_DECLINE = "-1"
REQUEST_INVITED = "2"

VALID_FILE_SIZE = 5242880

PUBLIC = "1"
PRIVATE = "0"

PUBLIC_LOCATION = "8"
PRIVATE_LOCATION = "7" 

UNLIMITED = -1

TICKET_OPEN = "1"
TICKET_CLOSE = "0"

STATUS_NEW = "1"
STATUS_OLD = "0"

HIDE = "0"
SHOW = "1"

YES_VALUE = "1"
NO_VALUE = "0"

SECONDS_PER_DAY = 86400
SECONDS_PER_HOUR = 3600
SECONDS_PER_MINUTE = 60

FILTER_HOW_MAP = {
    1: "Latest", 
    2: "Hot"
}

FILTER_WHEN_MAP = {
    1: "Today", 
    2: "Tomorrow", 
    3: "This Week", 
    4: "Next Week", 
    5: "This Month",
}

MONTH_SHORT_MAP = {
    1: "Jan",
    2: "Feb", 
    3: "Mar", 
    4: "Apr", 
    5: "May", 
    6: "Jun", 
    7: "Jul", 
    8: "Aug", 
    9: "Sep", 
    10: "Oct", 
    11: "Nov", 
    12: "Dec"
}

MONTH_FULL_MAP = {
    1: "January", 
    2: "February", 
    3: "March", 
    4: "April", 
    5: "May", 
    6: "June", 
    7: "July", 
    8: "August", 
    9: "September", 
    10: "October", 
    11: "November", 
    12: "December"
}

DAY_IN_MONTH_MAP = {
    1:31, 
    2:28, 
    3:31, 
    4:30, 
    5:31, 
    6:30, 
    7:31, 
    8:31, 
    9:30, 
    10:31, 
    11:30, 
    12:31
}

WEEK_DAY_TEXT_MAP = {
    "Mon": "Monday", 
    "Tue": "Tuesday", 
    "Wed": "Wednesday", 
    "Thu": "Thursday", 
    "Fri": "Friday", 
    "Sat": "Saturday", 
    "Sun": "Sunday"
}

WEEK_DAY_NUM_MAP = {
    0: "Monday", 
    1: "Tuesday", 
    2: "Wednesday", 
    3: "Thursday", 
    4: "Friday", 
    5: "Saturday", 
    6: "Sunday"
}

WEEK_DAY = (
  ('0',"Monday"), 
  ('1',"Tuesday"), 
  ('2',"Wednesday"), 
  ('3',"Thursday"), 
  ('4',"Friday"), 
  ('5',"Saturday"), 
  ('6',"Sunday")
)

DAY_SUFFIX = {
    1: "st", 
    2: "nd", 
    3: "rd"
}

USA_COUNTRY_CODE = "United States of America"

USA_STATES = (
   ("AL","Alabama"),("AK","Alaska"),("AZ","Arizona"),("AR","Arkansas"),("CA","California"),
   ("CO","Colorado"),("CT","Connecticut"),("DE","Delaware"),("FL","Florida"),("GA","Georgia"),
   ("HI","Hawaii"),("ID","Idaho"),("IL","Illinois"),("IN","Indiana"),("IA","Iowa"),
   ("KS","Kansas"),("KY","Kentucky"),("LA","Louisana"),("ME","Maine"),("MD","Maryland"),
   ("MA","Massachusetts"),("MI","Michigan"),("MN","Minnesota"),("MS","Mississippi"),("MO","Missouri"),
   ("MT","Montana"),("NE","Nebraska"),("NV","Nevada"),("NH","New Hampshire"),("NJ","New Jersey"),
   ("NM","New Mexico"),("NY","New York"),("NC","North Carolina"),("ND","North Dakota"),("OH","Ohio"),
   ("OK","Oklahoma"),("OR","Oregon"),("PA","Pennsylvania"),("RI","Rhode Island"),("SC","South Carolina"),
   ("SD","South Dakota"),("TN","Tennessee"),("TX","Texas"),("UT","Utah"),("VT","Vermont"),
   ("VA","Virginia"),("WA","Washington"),("WV","West Virginia"),("WI","Wisconsin"),("WY","Wyoming"),
)

COUNTRIES_ALPHA2 = setup_constant_countries_alpha2()
COUNTRIES_ALPHA3 = setup_constant_countries_alpha3()

DEFAULT_COUNTRIES_FORMAT = COUNTRIES_ALPHA2

YES_NO = (
  ("1","Yes"),
  ("0","No"),
)

LOCATION_CATEGORY = (
    ("1","Art & Entertaining"),
    ("2","Business"),
    ("3","Education"),
    ("4","Food & Drink"),
    ("5","Nightlife"),
    ("6","Outdoor"),
    ("7","Private place"),
    ("8","Public place"),
    ("9","Shopping"),
    ("10","Sight")
)

LOCATION_CATEGORY_MAP = {
    "1":"Art & Entertaining",
    "2":"Business",
    "3":"Education",
    "4":"Food & Drink",
    "5":"Nightlife",
    "6":"Outdoor",
    "7":"Private place",
    "8":"Public place",
    "9":"Shopping",
    "10":"Sight"
}

PRIVACY_STATUS = (
    (PUBLIC,"Public"),
    (PRIVATE,"Private"),
)

ACTIVITY_REQUEST_STATUS = (
  (REQUEST_RESERVED, "Reserved"),
  (REQUEST_ACCEPT, "Acccepted"),
  (REQUEST_WAITING, "Waiting"),
  (REQUEST_DECLINE, "Decline"),
  (REQUEST_INVITED, "Invited"),
)

STATUS = (
  (STATUS_OLD, "old"),
  (STATUS_NEW, "new"),
) 

APPEARANCE_STATUS = (
  (HIDE, "hide"),
  (SHOW, "show"),
) 

NOTIFICATION_TYPE = (
  ("activity","activity"),
  ("location","location"),
) 

PRIVACY_OPTIONS = (
    (PUBLIC,'Public'),
    (PRIVATE,"Private")
)

PRIVACY_OPTIONS_MAP = {
  "1": "Public",
  "0": "Private"
}

ENTER_ACTIVITY = {
  ("1", "Used"),
  ("0", "New"),
}

ACTIVITY_CATEGORY = (
  ("1", "Art/Entertaining"),
  ("2", "Business/Networking"),
  ("3", "Education"),
  ("4", "Dating/Dinner"),
  ("5", "Meetup/Hangout"),
  ("6", "Party"),
  ("7", "Sport/Outdoor"),
  ("8", "Volunteer/Community"),
  ("9", "Others"),
)

ACTIVITY_CATEGORY_MAP = {
  "1": "Art/Entertaining",
  "2": "Business/Networking",
  "3": "Education",
  "4": "Dating/Dinner",
  "5": "Meetup/Hangout",
  "6": "Party",
  "7": "Sport/Outdoor",
  "8": "Volunteer/Community",
  "9": "Other",
}

GENDER = (
    ("m", "Male"),
    ("f", "Female"),
)

GENDER_MAP_BY_NAME = {
  "male":"1",
  "female":"0"
}

GENDER_MAP_BY_CODE = {
  "1":"male",
  "0":"female"
}

COMMENT_TYPE = (
  (ACTIVITY,'activity'),
  (LOCATION,'location'),
)


REPORT_TYPE = (
    ('comment','comment'),
    ("activity","activity"),
    ("location","location"),
    ("user","user")
)

PHOTO_TYPE = (
    ('activity','activity'),
    ('location','location'),
    ('user_profile','user_profile'),
    ("default_image","default_image")
)

POST_TYPE = (
    ('activity','activity'),
    ('location','location'),
)

VIDEO_TYPE = (
    ("1", "activity"),
    ("2","location"),
)

TICKET_STATUS = (
    ( TICKET_OPEN, "Open" ),
    ( TICKET_CLOSE, "Close")
)


ACTIVITY_TYPE = (
    ("0", "Normal Event"),
    ("1", "Sell Ticket Event"),
    ("2", "Sale Event")
)

RATING_TYPE = (
    ( ACTIVITY,"activity"),
    ( LOCATION,"location"),
)

FEED_OBJECT_TYPE = (
    ( ACTIVITY,"activity"),
    ( LOCATION,"location"),
)



