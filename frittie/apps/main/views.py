###############################
#  Author: Dang Nguyen,Duc Vu #
#  Date: 5/24/2012            #
#  Last Modified: 10/04/2013  #
###############################

from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404, get_list_or_404
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User, AnonymousUser
from django.utils import simplejson, timezone
from django.core import serializers

from frittie.apps.app_helper import get_user_login_object, convert_list_to_json, get_template_path
from frittie.apps.activity.helper import get_nearby_activity
from frittie.apps.location.helper import get_nearby_location, get_current_city
from frittie.apps.main.models import Activity, Location, Photo

from frittie.apps.main.constants import PUBLIC, LOCATION_PRIVATE_PLACE
from frittie.apps.app_settings import NUM_ITEM_PER_PAGE

import json, logging, datetime

APP_NAME = "main"

def main_page(request):
	context_instance = RequestContext(request)
	data = {'app_name':APP_NAME}
	now = datetime.datetime.now()
	today = now + datetime.timedelta(days=-1)
	user_login = context_instance['user_login']
	template = "apps/main/page/landing_page.html"
	if user_login != None:
		template = "apps/main/page/list_activity_homepage.html"
		if "location" in request.GET:
			template = "apps/main/page/list_location_homepage.html"

		data['is_first_page'] = True
		data['is_last_page'] = False
		current_page = 0
		start, end = 0, NUM_ITEM_PER_PAGE
		previous_page, next_page = 0, 0
		is_have_page_parameter = False
		if "page" in request.GET:
			is_have_page_parameter = True
			current_page = int(request.GET['page'])
			data['is_first_page'] = True if int(current_page) == 0 else False

		start = int(current_page) * NUM_ITEM_PER_PAGE
		end = int(start) + NUM_ITEM_PER_PAGE
		previous_page = int(current_page) - 1 if int(current_page) != 0 else 0
		if "lat" in request.GET and "lng" in request.GET and "city" in request.GET:
			context_instance['current_lat'] = request.GET['lat']
			context_instance['current_lng'] = request.GET['lng']
			context_instance['current_city'] = request.GET['city']
			if "location" in request.GET:
				city = get_current_city(request,request.GET['city'])
				data['total_locations'] = Location.objects.filter(city=city).exclude(category=LOCATION_PRIVATE_PLACE,main_picture=None).count()
				next_page = int(current_page) + 1 if end < data['total_locations'] else current_page
				data['is_last_page'] = True if end >= data['total_locations'] else False 
				data['locations'] = get_nearby_location(request,city,start,end)
			else:
				activities = get_nearby_activity(request,request.GET['lat'],request.GET['lng'])
				data['total_activities'] = len(activities)
				data['activities'] = activities[start:end]
				next_page = int(current_page) + 1 if end < data['total_activities'] else current_page
				data['is_last_page'] = True if end >= data['total_activities'] else False
		else:
			if "location" in request.GET:
				city = get_current_city(request,None)
				data['total_locations'] = Location.objects.filter(city=city).exclude(category=LOCATION_PRIVATE_PLACE,main_picture=None).count()
				next_page = int(current_page) + 1 if end < data['total_locations'] else current_page
				data['is_last_page'] = True if end >= data['total_locations'] else False
				data['locations'] = get_nearby_location(request,None,start,end)
			else:
				activities = get_nearby_activity(request)
				data['total_activities'] = len(activities)
				data['activities'] = activities[start:end]
				next_page = int(current_page) + 1 if end < data['total_activities'] else current_page
				data['is_last_page'] = True if end >= data['total_activities'] else False
		full_path = request.get_full_path()
		if "?" in full_path:				
			data['previous_page_link'] = full_path[:full_path.find("page")] + "page=" + str(previous_page) if is_have_page_parameter else full_path + "&page=" + str(previous_page)
			data['next_page_link'] = full_path[:full_path.find("page")] + "page=" + str(next_page) if is_have_page_parameter else full_path + "&page=" + str(next_page)
		else:
			data['previous_page_link'] = full_path + "?page=" + str(previous_page)
			data['next_page_link'] = full_path + "?page=" + str(next_page)
	return render_to_response(template,data,context_instance=context_instance)

# This will show a general page for all kind of error
def error_page(request):
	data = {'app_name':APP_NAME}
	return render_to_response("error.html",data,context_instance=RequestContext(request))




