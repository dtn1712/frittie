from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin

from tastypie.admin import ApiKeyInline
from tastypie.models import ApiAccess, ApiKey

from frittie.apps.main.models import *

########################################
#                                      #
#      DEFINE CLASS ADMIN AREA         #
#                                      #
########################################
class SocialFriendListAdmin(admin.ModelAdmin):
    list_display = ['user_social_auth',"friends"]


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ['user','city','state','privacy_status',"avatar"]


class MessageAdmin(admin.ModelAdmin):
    list_display = ['user_send','user_receive','content','date','status']

class EmailTrackingAdmin(admin.ModelAdmin):
    list_display = ['pk','to_emails',"email_template","text_content","context_data","status","send_time"]


class LocationBusinessHourAdmin(admin.ModelAdmin):
    list_display = ['pk','weekday','hour_start','hour_end','location']

class ConversationAdmin(admin.ModelAdmin):
    list_display = ['user1','user2','latest_message']


class CommentAdmin(admin.ModelAdmin):
    list_display = ['comment_type','unique_id','user','content','create_date','edit_date']


class ReportAdmin(admin.ModelAdmin):
    list_display = ['report_type','unique_id','date']


class ActivityRequestAdmin(admin.ModelAdmin):
    list_display = ['activity','user_request','date','request_status']


class ActivityAdmin(admin.ModelAdmin):
    list_display = ['name','description','start_time','end_time','logo',
                    'user_create','activity_type','location','limit']

    
class ActivityUserRelationAdmin(admin.ModelAdmin):
    list_display = ['activity','user','status']


class LocationAdmin(admin.ModelAdmin):
    list_display = ["pk",'name','description','create_by','category','address1','address2','city','state','zip_code','country','main_picture']
    search_fields = ['name','category','zip_code','state','country']


class NotificationAdmin(admin.ModelAdmin):
    list_display = ['notification_type','content','notify_from','date','status']


class InvitationAdmin(admin.ModelAdmin):
    list_display = ['unique_id','activity','message']


class TicketAdmin(admin.ModelAdmin):
    list_display = ['price','fee','ticket_name','ticket_status',"total_available_quantity","maximum_quantity_transaction"]


class PhotoAdmin(admin.ModelAdmin):
    search_fields = ["caption",'photo_type','user_post']
    list_display = ['unique_id',"caption",'photo_type','object_unique_id','user_post','upload_date']
    list_filter = ["caption",'photo_type','unique_id','user_post','upload_date']

class ActivityCancelAttendingAdmin(admin.ModelAdmin):
    list_display = ["activity","user_cancel","date","reason"]

class UserModelAdmin(UserAdmin):
    inlines = UserAdmin.inlines + [ApiKeyInline]

class FeedAdmin(admin.ModelAdmin):
    list_display = ['content','unique_id','create_by','date']


class TagAdmin(admin.ModelAdmin):
    list_display = ['content','user_tag']

class ConfirmationAdmin(admin.ModelAdmin):
    list_display = ['user_confirm','confirmation_id','is_confirmed']

class VideoAdmin(admin.ModelAdmin):
    search_fields = ["filename",'video_type','user_post']
    list_display = ["filename",'video_type','user_post','upload_date']
    list_filter = ["filename",'video_type','user_post','upload_date']



########################################
#                                      #
#            REGISTER AREA             #
#                                      #
########################################
admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Activity, ActivityAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(Report, ReportAdmin)
admin.site.register(Location, LocationAdmin)
admin.site.register(Notification, NotificationAdmin)
admin.site.register(ActivityRequest,ActivityRequestAdmin)
admin.site.register(ActivityCancelAttending,ActivityCancelAttendingAdmin)
admin.site.register(Photo, PhotoAdmin)
admin.site.register(Video, VideoAdmin)
admin.site.register(Invitation, InvitationAdmin)
admin.site.register(Ticket, TicketAdmin)
admin.site.register(ConfirmationActivity, ConfirmationAdmin)
admin.site.register(Message,MessageAdmin)
admin.site.register(Conversation,ConversationAdmin)
admin.site.register(ApiAccess)
admin.site.register(Feed,FeedAdmin)
admin.site.register(Tag,TagAdmin)
admin.site.register(SocialFriendList,SocialFriendListAdmin)
admin.site.register(EmailTracking,EmailTrackingAdmin)
admin.site.register(LocationBusinessHour,LocationBusinessHourAdmin)




