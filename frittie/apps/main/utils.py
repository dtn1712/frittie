import datetime
import urllib

from constants import SECONDS_PER_DAY, SECONDS_PER_HOUR, SECONDS_PER_MINUTE

def get_elapse_time_text(value):  
   # Get total elapse seconds
   elapse_time = datetime.datetime.utcnow() - datetime.datetime(value.year,value.month,value.day,value.hour,value.minute,value.second)
   total_seconds = elapse_time.total_seconds()

   days = int(total_seconds / SECONDS_PER_DAY)
   hours = int((total_seconds % SECONDS_PER_DAY) / SECONDS_PER_HOUR)
   minutes = int(((total_seconds % SECONDS_PER_DAY) % SECONDS_PER_HOUR) / SECONDS_PER_MINUTE)
   
   if days == 0 and hours == 0 and minutes == 0:
      return "Just now"

   min_value, hour_value, day_value = ' minute', ' hour', ' day'
   if minutes != 1: min_value += "s"
   if hours != 1: hour_value += "s"
   if days != 1: day_value += "s" 
   
   if days == 0 and hours == 0:
   	return str(minutes) + min_value + " ago"
   if days == 0:
   	return str(hours) + hour_value + " ago"
   return str(days) + day_value + " ago"

def is_empty(s):
   if s == None: return True
   if len(s.strip()) == 0: return True
   return False

