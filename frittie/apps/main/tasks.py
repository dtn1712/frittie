from celery.task import PeriodicTask
from celery.task.schedules import crontab
from celery.decorators import periodic_task 

from django.contrib.auth.models import User
from django.core.mail import send_mail ## import to send mail

from frittie.apps.watchlist.helper import check_upcoming
from frittie.apps.watchlist.helper import check_happen_in
from frittie.apps.main.models import Activity, EmailTracking
from frittie.apps.mailer.helper import send_mass_email, generate_and_send_email
from frittie.apps.activity.helper import get_activity_common_info

from datetime import timedelta, datetime

# use next line if you want to send email at a specific time of week (eg: 17:30 every friday)
@periodic_task(run_every=crontab(hour=17, minute=30, day_of_week="fri"))
#@periodic_task(run_every=crontab(hour="*", minute="*", day_of_week="*"))  
def remindUpcomingActivities():  
	email_items = []
	users = User.objects.all()    
	for user in users:
		activities = []
		if user.get_profile():
			for activity in user.get_profile().watchlist.all():
				if activity:
					if ((check_happen_in(activity.start_time, datetime.now(), 2)) or
						(check_happen_in(activity.start_time, datetime.now(), 1))):
						activities.append(get_activity_common_info(activity))
						print activity.unique_id
			email = user.email
			if email and activities:
				email_template = "remind_upcoming_activities"
				context = { "activities" : activities, "unique_id":activity.unique_id}
				email_item = EmailTracking.objects.create(to_emails = email, email_template = email_template, context_data = context)
				email_items.append(email_item.id)
				#generate_and_send_email(email_template, context, [recipient])
				#send_mail('suggestions', str(activities).strip('[]'),  'team@frittie.com', [recipient], fail_silently = False)
	print "firing test task"
	send_mass_email(email_items)


# Remind users who follow the upcoming events
# @periodic_task(run_every=crontab(hour="*", minute="*", day_of_week="*"))  
# def remindUpcomingActivities():  
	# activities = Activity.objects.all()
	# for activity in activities:
	# 	if ((check_happen_in(activity.start_time, datetime.now(), 2)) or
	# 			(check_happen_in(activity.start_time, datetime.now(), 1))):
	# 		users = User.objects.all()
	# 		followers = []
	# 		for user in users:
	# 			if activity in user.get_profile().watchlist.all():
	# 				followers.append(user.email)
	# 		print followers
	# 		if followers:
	# 			email_template = "remind_followers"
	# 			context = { "activity" : activity }
	# 			if (check_happen_in(activity.start_time, datetime.now(), 2)):
	# 				context["datesleft"] = "two days"
	# 			else:
	# 				context["datesleft"] = "one day"
	# 			generate_and_send_email(email_template, context, followers)

