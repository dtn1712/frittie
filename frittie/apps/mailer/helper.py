from django.core.mail import get_connection, EmailMultiAlternatives
from django.template.loader import render_to_string
from django.conf import settings as django_settings

from frittie.apps.main.models import EmailTracking
from frittie.apps.app_settings import EMAIL_SUBJECT_SNIPPET_TEMPLATE
from frittie.apps.app_settings import EMAIL_CONTENT_HTML_SNIPPET_TEMPLATE, EMAIL_CONTENT_TEXT_SNIPPET_TEMPLATE
from frittie.settings import SERVER_EMAIL

import logging, datetime

logger = logging.getLogger(__name__)


def generate_and_send_email(email_template,context,send_email_list):
	subject = render_to_string(EMAIL_SUBJECT_SNIPPET_TEMPLATE[email_template],context)
	html_content = render_to_string(EMAIL_CONTENT_HTML_SNIPPET_TEMPLATE[email_template],context)
	text_content = render_to_string(EMAIL_CONTENT_TEXT_SNIPPET_TEMPLATE[email_template],context)
	email = EmailMultiAlternatives(subject, text_content, SERVER_EMAIL,send_email_list)
	email.attach_alternative(html_content, "text/html")
	context_data = ""
	try:
		context_data = json.dumps(context)
	except:
		pass
	email_tracking = EmailTracking.objects.create(to_emails=",".join(send_email_list),email_template=email_template,subject=subject,text_content=text_content,context_data=context_data)
	try:
		email.send()
		email_tracking.status = "success"
		email_tracking.send_time = datetime.datetime.now()
		email_tracking.save()
		return email_tracking.id
	except Exception as e:
		logger.exception(e)
		email_tracking.status = "fail"
		email_tracking.save()
		return None
	

def send_mass_email(list_email_tracking_id):
	emails = EmailTracking.objects.filter(id__in=list_email_tracking_id)
	list_send_emails = []
	for email in emails:
		list_send_emails.append(email.generate_email())
	try:
		connection = get_connection()
		connection.open()
		connection.send_messages(list_send_emails)
		connection.close()
		EmailTracking.objects.filter(id__in=list_email_tracking_id).update(status='success',send_time=datetime.datetime.now())
	except Exception as e:
		logger.exception(e)
		EmailTracking.objects.filter(id__in=list_email_tracking_id).update(status='fail')


