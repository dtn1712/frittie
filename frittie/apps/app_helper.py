####################################
#	Author: Dang Nguyen	,Duc Vu    #
#	File: app_helpers.py	       #
####################################

# Some common function used for any kind of app
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from django.db.models import Q
from django.core import serializers
from django.shortcuts import render_to_response, get_object_or_404
from django.template.loader import render_to_string
from django.template import RequestContext

from django.conf import settings as django_settings
from django.utils.html import strip_tags
from django.contrib.gis.geoip import GeoIP

from frittie.apps.app_settings import MODEL_KEY_MAP, UNSPECIFIED_MODEL_KEY, MODEL_KEY_LIST
from frittie.apps.app_settings import MESSAGE_SNIPPET_TEMPLATE, HTML_SNIPPET_TEMPLATE
from frittie.settings import SITE_NAME, ADMIN_USERNAMES, SECRET_KEY, OW_LY_API_KEY
from frittie.apps.timezone_utils import GeoTimeZoneReader
from frittie.apps.timezone_settings import TIMEZONE_TO_UTC

from firebase_token_generator import create_token

import collections, datetime, decimal, json, os, logging
import shortuuid, random, math, pycountry, pytz, requests

logger = logging.getLogger(__name__)

CATALOGUE = "CATALOGUE"
COMMENT_CHARACTER = "<!--"

def read_catalogue(list_file,path):
	catalogue_path = path
	if CATALOGUE not in path:
		catalogue_path = path + CATALOGUE if path[len(path)-1] == "/" else path + "/" + CATALOGUE
	f = open(catalogue_path,"r")
	for filename in f:
		if len(filename.replace("\n","")) != 0 and filename.startswith(COMMENT_CHARACTER) == False:
			list_file.append(filename.replace("\n",""))

# This function to get current time-zone of client
def get_current_time_zone(request):
	g = GeoIP()
	client_ip = get_client_ip(request)	
	data = g.city(client_ip)
	db = GeoTimeZoneReader()
	timezone = "America/New_York"
	try:
		timezone = db.get_timezone(data['country_code'], data['region'])
	except Exception as e:
		logger.exception(e)
	return timezone

# This function to get current time based on the time_zone
def get_current_time_at_client_time_zone(request):
	time_zone = get_current_time_zone(request)
	return datetime.datetime.now(pytz.timezone(time_zone))

def convert_list_to_json(data):
	d = []
	for i in range(0,len(data)):
		d.append(data[i]['fields'])
	return json.dumps(d)

def capitalize_first_letter(s):
	if s == None: return
	if len(s) == 0: return
	return s[0].upper() + s[1:].lower()

def json_encode_decimal(obj):
	if isinstance(obj, decimal.Decimal):
		return str(obj)
	raise TypeError(repr(obj) + " is not JSON serializable")

def convert_24hr_to_ampm(time):
	hour = time.hour
	minute = time.minute
	if int(hour) >= 12:
		hour = int(hour) % 12
		return str("%02d" % hour) + ":" + str("%02d" % minute) + "pm"
	else:
		return str("%02d" % hour) + ":" + str("%02d" % minute) + "am"

def convert_ampm_to_24hr(hour,ampm):
	result = 0
	if ampm.lower() == 'am':
		return int(hour) % 12
	elif ampm.lower() == 'pm':
		return 12 + int(hour) % 12
	else:
		raise Exception("Invalid time format. Expect 'am' or 'pm' but got %s" %(ampm))
  
def convert_time(date,time,ampm=None,date_separator='/',time_separator=':',format='mm/dd/yyyy'):
	date_list = date.split(date_separator)
	format_list = format.split('/')
	year = int(date_list[2])
	month = int(date_list[0])
	day = int(date_list[1])
	time_list = time.split(time_separator)
	minute = int(time_list[1])
	hour = 0
	if ampm:
		hour = convert_ampm_to_24hr(int(time_list[0]),ampm)
	else:
		hour = int(time_list[0])
	new_time = datetime.datetime(year,month,day,hour,minute)
	return new_time

def get_display_time(datetime,is_ampm_format=True):
	if datetime == None: return None
	time = convert_24hr_to_ampm(datetime)
	date = str(datetime.date())
	return date + " " + time

def get_duplicate_object(l):
	l2 = collections.Counter(l)
	return [i for i in l2 if l2[i]>1]


def remove_duplicate_object(l):
	return list(set(l))


def set_fixed_string(s,s_len):
	if s_len >= len(s): 
		return s
	return s[:s_len] + '...'

# Convert queryset type to list. Note: cannot convert object
def convert_queryset_to_list(obj_queryset):
	result = []
	if len(obj_queryset) != 0:
		for obj in obj_queryset:
			result.append(obj)
	return result

# Get current login user object
def get_user_login_object(request):
	user_login = request.user
	if user_login.is_anonymous():
		return None
	return user_login

# Return JSON of data with all fields in the parameter excluded
def get_JSON_exclude_fields(data,fields,extras_param={},relations_param={}):
	obj_list = convert_queryset_to_list(data)
	if len(obj_list) != 0:
		return serializers.serialize("json",data,indent=4,excludes=fields,extras=extras_param,relations=relations_param)
	else:
		return []

# Return JSON of data with all fields in the parameter included
def get_JSON_include_fields(data,fields,extras_param={},relations_param={}):
	obj_list = convert_queryset_to_list(data)
	if len(obj_list) != 0:
		return serializers.serialize("json",data,indent=4,fields=fields,extras=extras_param,relations=relations_param)
	else:
		return []

# Get the current ip from client to see their zip code and
# return appropriate location. Currently, this is not work with localhost
def get_client_ip(request):
	x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
	if x_forwarded_for:
		ip = x_forwarded_for.split(',')[-1].strip()
	else:
		ip = request.META.get('REMOTE_ADDR')
	return ip

# 	Author: Hieu Le
# 	check whether the position2 is within a distance from position 1 
# 	return true if distance between pos 2 and pos 1 < desireDistance 
def check_position_within_distance(lat1, lng1, lat2, lng2, desireDistance):
	rEarth = 6371000 #radius of the earth in meters
	distance = math.acos(math.sin(lat1)*math.sin(lat2) + math.cos(lat1)*math.cos(lat2)*math.cos(lng1-lng2))*rEarth
	if distance <= desireDistance:
		return True
	return False

# Setup the constant month tuple using for the form
def setup_constant_month():
	month_value = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
	l = []
	for i in range(1,13):
		l.append([i,month_value[i-1]])
	return tuple(tuple(x) for x in l)

# Setup the constant day tuple 
def setup_constant_day():
	l = []
	for i in range(1,32):
		l.append([i,i])
	return tuple(tuple(x) for x in l)

# Setup the constant year tuple 
def setup_constant_year():
	l = []
	current_year = datetime.datetime.now().year
	for i in range(1920,int(current_year) - 13):
		l.append([i,i])
	return tuple(tuple(x) for x in l)

# Setup the constant countries in alpha2 code
def setup_constant_countries_alpha2():
	output = []
	for country in list(pycountry.countries):
		data = (str(country.alpha2),country.name.encode("utf-8"))
		output.append(data)
	return tuple(output)	

# Setup the constant countries in alpha3 code
def setup_constant_countries_alpha3():
	output = []
	for country in list(pycountry.countries):
		data = (str(country.alpha3),country.name.encode("utf-8"))
		output.append(data)
	return tuple(output)	


def generate_html_snippet(request,snippet,data):
	return render_to_response(HTML_SNIPPET_TEMPLATE[snippet],data,context_instance=RequestContext(request)).content
	

def generate_message(action,result,data):
	template_name = action + "_" + result
	message = render_to_string(MESSAGE_SNIPPET_TEMPLATE[template_name],data)
	return message


def handle_request_get_message(request,data):
	if request.session.get('is_show_request_message'):
		del request.session['is_show_request_message']
		if "action" in request.GET and "result" in request.GET:	
			return generate_message(request.GET['action'],request.GET['result'],data)
	return None


def generate_unique_id(model_type=None):
	prefix = UNSPECIFIED_MODEL_KEY
	if model_type != None:
		if model_type.lower() in MODEL_KEY_MAP:
			prefix = MODEL_KEY_MAP[model_type.lower()]
	return prefix + shortuuid.uuid()[:11] + shortuuid.uuid()[:11] + str("%03d" % random.randint(1,999))

# get this week range start from now to sunday
def get_this_week_range():
	now = datetime.datetime.now()
	current_weekday = now.weekday()
	result = {}
	#if current_weekday == 4:
	result['start'] = now ## start from now
	# elif current_weekday > 4:
	# 	result['start'] = now - datetime.timedelta(days=current_weekday-4)
	# else:
	# 	result['start'] = now - datetime.timedelta(days=7+current_weekday-4)
	result['end'] = result['start'] + datetime.timedelta(days=6-current_weekday) #end at sunday
	return result

def get_next_week_range():
	now = datetime.datetime.now()
	result = {}
	current_weekday = now.weekday()
	start_next_week = now - datetime.timedelta(days=current_weekday) + datetime.timedelta(days=7)
	#if current_weekday  >= 4 and current_weekday <=6:
	result['start'] = start_next_week
	# else:
	# 	result['start'] = now + datetime.timedelta(days=4-current_weekday)
	result['end'] = result['start'] + datetime.timedelta(days=7)
	return result

def get_this_month_range():
	now = datetime.datetime.now()
	result = {}
	result['start'] = datetime.datetime(now.year,now.month,1)
	if now.month == 12:
		result['end'] = datetime.datetime(now.year+1,1,1)
	else:
		result['end'] = datetime.datetime(now.year,now.month+1,1) 
	return result

def get_any_admin_object():
	for username in ADMIN_USERNAMES:
		try:
			admin = User.objects.get(username=username)
			return admin
		except User.DoesNotExist:
			pass
	return None

def generate_token(custom_data,options):
	return create_token(SECRET_KEY,custom_data,options)

def make_two_numbers(original):
	if len(original)==1:
		return "0"+original
	else:
		return original

def get_short_url(request):
	long_url = str(request.build_absolute_uri())
	# try:
	# 	r = requests.get("http://ow.ly/api/1.1/url/shorten?apiKey=" + OW_LY_API_KEY + "&longUrl=" + long_url)
	# 	data = r.json()
	# 	return data['results']['shortUrl']
	# except:
	# 	pass	
	return long_url
	
def get_different_hours_from_timezones(first,second):
	try:
		if (TIMEZONE_TO_UTC[str(second)] is not None ) and (TIMEZONE_TO_UTC[str(first)] is not None):
			return TIMEZONE_TO_UTC[str(second)]-TIMEZONE_TO_UTC[str(first)]
	except:
		pass
	return 0

def is_str_unique_id(s):
	if len(s) != 27 or s[24:27].isdigit() == False or s[0:2].upper() not in MODEL_KEY_LIST: 
		return False
	return True


def get_template_path(app_name,template_name,flavour):
	if flavour == None:
		return "apps/" + app_name + "/page/" + template_name + ".html"
	else:
		prefix = "mobile" if flavour == "mobile" else "web"
		return "apps/" + app_name + "/page/" + prefix + "/" + template_name + ".html"

