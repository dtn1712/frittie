from django.utils import simplejson
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response, get_object_or_404, get_list_or_404
from django.contrib.auth.models import User
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.db.models import Q

from haystack.views import SearchView
from haystack.query import SearchQuerySet

from frittie.apps.app_settings import PEOPLE_SEARCH_TEMPLATE, LOCATION_SEARCH_TEMPLATE, ACTIVITY_SEARCH_TEMPLATE
from frittie.apps.app_helper import get_user_login_object
from frittie.apps.app_settings import SEARCH_TYPE
from frittie.apps.main.models import Location, Activity, UserProfile
from frittie.apps.search.helper import build_activity_autocomplete_data
from frittie.apps.search.helper import build_location_autocomplete_data
from frittie.apps.search.helper import build_user_autocomplete_data
from frittie.apps.search.receivers import city_autocomplete_data_normal, city_autocomplete_data_explore

from frittie.apps.main.constants import PUBLIC

from frittie.settings import ADMIN_USERNAMES

import json, logging, datetime

logger = logging.getLogger(__name__)

class MainSearchView(SearchView):

	def __call__(self,request):
		query = request.GET['q']
		if "search_type" in request.GET:
			search_type = request.GET['search_type']
			if search_type.lower() in SEARCH_TYPE:
				return HttpResponseRedirect("/search/" + search_type.lower() + "/?q=" + query)

		self.request = request

		self.form = self.build_form()
		self.query = self.get_query()
		self.results = self.get_results()

		is_have_activity = False
		is_have_location = False
		is_explore = False
		if "explore" in request.GET:
			if bool(int(request.GET['explore'])) == True:
				is_explore = True
		activities = []

		for result in self.results:
			if "city" in result.model_name:
				if self.query.lower().replace(" ","") in result.object.name.lower():
					if is_explore:
						return HttpResponseRedirect("/explore?city=" + result.object.name + "&lat=" + 
							str(result.object.latitude) + "&lng=" + str(result.object.longitude))
					else:
						return HttpResponseRedirect("/?city=" + result.object.name + "&lat=" + 
							str(result.object.latitude) + "&lng=" + str(result.object.longitude))
			if "activity" in result.model_name:
				is_have_activity = True
				if result.object.activity_type == PUBLIC:
					activities.append(result.object)
			if "location" in result.model_name:
				is_have_location = True
		context = {
			"query": self.query,
			"form": self.form,
			"results": self.results,
			"suggestion": None,
		}
		template = ACTIVITY_SEARCH_TEMPLATE
		if is_have_activity:
			context['search_type'] = "activity"
			context['activities'] = activities
		elif is_have_location:
			context['search_type'] = "location"
			template = LOCATION_SEARCH_TEMPLATE
		else:
			context['search_type'] = "people"
			template = PEOPLE_SEARCH_TEMPLATE
		return render_to_response(template, context, context_instance=self.context_class(self.request))


class CustomSearchView(SearchView):
	def create_response(self):
		context = {
				'query': self.query,
				'form': self.form,
				'results': self.results,
				'suggestion': None,
		}

		if self.results and hasattr(self.results, 'query') and self.results.query.backend.include_spelling:
			context['suggestion'] = self.form.get_suggestion()

		context.update(self.extra_context())
		return render_to_response(self.template, context, context_instance=self.context_class(self.request))

class PeopleSearchView(CustomSearchView):
	def extra_context(self):
		extra = super(PeopleSearchView, self).extra_context()
		extra['search_type'] = 'people'
		return extra

class LocationSearchView(CustomSearchView):
	def extra_context(self):
		extra = super(LocationSearchView, self).extra_context()
		extra['search_type'] = 'location'
		return extra

class ActivitySearchView(CustomSearchView):
	def extra_context(self):
		extra = super(ActivitySearchView, self).extra_context()
		extra['search_type'] = 'activity'
		activities = []
		for result in self.results:
			if "activity" in result.model_name:
				activities.append(result.object)
		extra['activities'] = activities
		return extra

def get_all_autocomplete_data(request):
	results = []
	try:
		user_login = get_user_login_object(request)
		activities = Activity.objects.filter(start_time__gte=datetime.datetime.now(),activity_type=PUBLIC)
		locations = Location.objects.all()
		users = []
		if user_login != None:
			users = User.objects.all().exclude(username__in=ADMIN_USERNAMES).exclude(username=user_login.username)
		else:
			users = User.objects.all().exclude(username__in=ADMIN_USERNAMES)
		build_activity_autocomplete_data(activities,results)
		build_location_autocomplete_data(locations,results)
		build_user_autocomplete_data(users,results)
	except Exception as e:
		logger.exception(e)
		results = {
			"http_status": 500,
			"error_message": str(e),
		}
	return HttpResponse(json.dumps(results,indent=2), content_type='application/json')

def get_specific_autocomplete_data(request,search_type):
	results = []
	try:
		user_login = get_user_login_object(request)
		if search_type.lower() == "city":
			results = city_autocomplete_data_normal
		elif search_type.lower() == "city_explore":
			results = city_autocomplete_data_explore
		elif search_type.lower() == "activity":
			activities = Activity.objects.filter(start_time__gte=datetime.datetime.now(),activity_type=PUBLIC)
			build_activity_autocomplete_data(activities,results)
		elif search_type.lower() == "location":
			locations = Location.objects.all()
			build_location_autocomplete_data(locations,results)
		elif search_type.lower() == "people":
			users = []
			if user_login != None:
				users = User.objects.all().exclude(username__in=ADMIN_USERNAMES).exclude(username=user_login.username)
			else:
				users = User.objects.all().exclude(username__in=ADMIN_USERNAMES)
			build_user_autocomplete_data(users,results)
	except Exception as e:
		logger.exception(e)
		results = {
			"http_status": 500,
			"error_message": str(e),
		}
	return HttpResponse(json.dumps(results,indent=2), content_type='application/json')
