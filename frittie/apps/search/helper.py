from frittie.settings import STATIC_URL
from frittie.apps.main.constants import LOGO_ICON_URL

import logging

logger = logging.getLogger(__name__)

def build_activity_autocomplete_data(activities,container):
	for activity in activities:
		try:
			data = {
				"value": activity.unique_id,
				"label": activity.name,
				"type": "Activity",
				"url": "/activity/" + activity.unique_id
			}
			if activity.logo == None:
				data['picture'] = LOGO_ICON_URL
			else:
				data['picture'] = activity.logo.image.url
			container.append(data)
		except Exception as e:
			logger.exception(e)
	return container

def build_location_autocomplete_data(locations,container):
	for location in locations:
		try:
			data = {
				"value": location.unique_id,
				"label": location.name + " - " + location.city + ", " + location.country,
				"type": "Location",
				"url": "/location/" + location.unique_id,
				"lat": str(location.lat),
				"lng": str(location.lng),
			}
			if location.main_picture != None:
				data['picture'] = location.main_picture.image.url
			else:
				data['picture'] = LOGO_ICON_URL
			container.append(data)
		except Exception as e:
			logger.exception(e)	
	return container

def build_user_autocomplete_data(users,container):
	for user in users:
		try:
			data = {
				"value": user.username,
				"label": user.get_profile().get_user_fullname(),
				"type": "People",
				"url": "/people/" + user.username
			}
			if user.get_profile().avatar != None:
				data['picture'] = user.get_profile().avatar.image.url
			else:
				data['picture'] = LOGO_ICON_URL
			container.append(data)
		except Exception as e:
			logger.exception(e)
	return container

def build_city_autocomplete_data(cities,container,is_explore):
	for city in cities:
		try:
			data = {
				"value": city.name,
				"label": city.display_name,
				"picture": STATIC_URL + "img/apps/body/city_icon.jpg",
				"type": "City",
			}
			if is_explore:
				data['url'] = "/explore?city=" + city.name + "&lat=" + str(city.latitude) + "&lng=" + str(city.longitude)
			else:
				data['url'] = "/?city=" + city.name + "&lat=" + str(city.latitude) + "&lng=" + str(city.longitude)
			container.append(data)
		except Exception as e:
			logger.exception(e)
	return container