from django.contrib.auth.models import User
from django.conf.urls import *

from haystack.views import SearchView, search_view_factory
from haystack.forms import ModelSearchForm
from haystack.query import SearchQuerySet

from views import PeopleSearchView, LocationSearchView, ActivitySearchView, MainSearchView
from views import get_all_autocomplete_data, get_specific_autocomplete_data

from frittie.apps.app_settings import PEOPLE_SEARCH_TEMPLATE, LOCATION_SEARCH_TEMPLATE, ACTIVITY_SEARCH_TEMPLATE
from frittie.apps.main.models import UserProfile, Location, Activity
from frittie.apps.search.helper import build_city_autocomplete_data

from cities_light.models import City

peopleSQS = SearchQuerySet().models(User,UserProfile)
locationSQS = SearchQuerySet().models(Location)
activitySQS = SearchQuerySet().models(Activity)
mainSQS = SearchQuerySet()

urlpatterns = patterns('haystack.views',

    url(r'^people/', search_view_factory(
                            view_class=PeopleSearchView,
                            template=PEOPLE_SEARCH_TEMPLATE,
                            searchqueryset=peopleSQS,
                            form_class=ModelSearchForm
                        ), name='people_search'),

    url(r'^location/', search_view_factory(
                            view_class=LocationSearchView,
                            template=LOCATION_SEARCH_TEMPLATE,
                            searchqueryset=locationSQS,
                            form_class=ModelSearchForm
                        ), name='location_search'),

    url(r'^activity/', search_view_factory(
                            view_class=ActivitySearchView,
                            template=ACTIVITY_SEARCH_TEMPLATE,
                            searchqueryset=activitySQS,
                            form_class=ModelSearchForm
                        ), name='activity_search'),

    url(r'^autocomplete/$', get_all_autocomplete_data),
    url(r'^autocomplete/(?P<search_type>\w+)/$', get_specific_autocomplete_data),
    url(r'^$',search_view_factory(
                    view_class=MainSearchView,
                    searchqueryset=mainSQS,
                    form_class=ModelSearchForm
                ), name="main_search")
)