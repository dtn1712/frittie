from django.contrib.auth.models import User
from django.core.paginator import Paginator, InvalidPage
from django.db import models
from django.contrib.auth import authenticate, login  
from django.conf.urls import patterns, url, include

from tastypie.authorization import Authorization, DjangoAuthorization
from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.serializers import Serializer  
from tastypie.authentication import Authentication , ApiKeyAuthentication, BasicAuthentication
from tastypie.models import create_api_key
from tastypie.models import ApiKey
from tastypie.utils import trailing_slash

from frittie.apps.main.models import Activity

from haystack.query import SearchQuerySet


import json
import logging
import math
from frittie.settings import STATIC_URL
APPEND_SLASH = False
TASTYPIE_ALLOW_MISSING_SLASH = True
class ActivityResource(ModelResource):
    #location = fields.OneToOneField('frittie.apps.api.locationapi.LocationResource', 'location' , null=True, blank=True)
    user_create = fields.OneToOneField('frittie.apps.api.userapi.UserResource', 'user_create' , null=True, blank=True)
    class Meta:
        queryset = Activity.objects.all()
        resource_name = 'Activity'
        allowed_methods = ['get','post','put','delete']
        serializer = Serializer(formats=['json', 'plist'])
        authorization= Authorization()
        #authentication = ApiKeyAuthentication()
        models.signals.post_save.connect(create_api_key, sender=User)
        filtering = {
            'user_create': ALL_WITH_RELATIONS,
            'location':ALL_WITH_RELATIONS,
            'pub_date': ['exact', 'lt', 'lte', 'gte', 'gt'],
        }

    def prepend_urls(self):
        return [
        url(r"^(?P<resource_name>%s)/nearby%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('nearby'), name="api_get_nearby"),
        #url(r"^(?P<resource_name>%s)%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('emptyurl'), name="api_get_emptyurl"),
        url(r"^(?P<resource_name>%s)/invite%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('invite'), name="api_invite"),
        url(r"^(?P<resource_name>%s)/invite%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('invite'), name="api_invite"),
        url(r"^(?P<resource_name>%s)/d/search%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('search'), name="api_search"),
        url(r"^(?P<resource_name>%s)/nearbysecond%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('nearby'), name="api_get_nearby"),
        url(r"^(?P<resource_name>%s)/addphoto%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('add_photo'), name="api_add_photo"),
        ]

    def nearby(self, request, **kwargs):   
        self.method_check(request, allowed=['post','get'])
        self.is_authenticated(request)
        self.throttle_check(request)
        activities = Activity.objects.filter(**self.remove_api_resource_names(kwargs))
        data=json.loads(request.body)
        lat= data['lat']
        lng= data['lng']
        rad= int(data['rad'])
        username = data['username']
        objects = []
        photos_url={}
        for act in activities:
            print act.location.lat
            print act.location.lng
            print float(lat)
            print float(lng)
            rate = 0.0
            const = 0.00
            if rad <=501:
                const = 1.14
            elif rad <=2000:
                const = 3.0
            elif rad <=5000:
                const = 5.0
            elif rad <=10000:
                const = 8.0
            print 'test2'
            print 'const'
            print const
            print str(math.sqrt(pow(float(act.location.lat) - float(lat),2) + pow(float(act.location.lng) - float(lng),2)) )
            if (math.sqrt(pow(float(act.location.lat) - float(lat),2) + pow(float(act.location.lng) - float(lng),2))  < const ):
                listfollow = act.location.follow_by.all()
                for user1 in listfollow:
                    if user1.username==username:
                        rate = len(listfollow)*0.5
                        break
                user2 = User.objects.get(username__exact=username)
                friends = Friendship.objects.friends_of(user2).values_list('username', flat=True)
                #print friends
                for friend in friends:
                    print friend
                    if friend == act.user_create.username:
                        rate = rate + 0.3
              
                print "rate"
                print rate

                photos = act.list_photos.all()

                images=[]
                for i in photos:
                    images.append(i.image.url)
                activity_id = act.activity_id
                photos_url[activity_id]=images
                bundle = self.build_bundle(obj=act, request=request)
                bundle = self.full_dehydrate(bundle)
                photo = Photo.objects.get(photo_id = bundle.data['logo'])

                #print bundle.data
                bundle.data['photos_url']= images
                bundle.data['logo'] = photo.image.url
                #print bundle.data['photos_url']
                #print bundle.data['activity_id']
                #bundle['photos_url'] = photos_url
                objects.append(bundle.data)
        #object_list = {
        #    'objects': objects,
        #    'photos_url':photos_url,
        #}
        return self.create_response(request, objects)


    def emptyurl(self, request, **kwargs):   
        self.method_check(request, allowed=['post','get'])
        self.is_authenticated(request)
        self.throttle_check(request)
        data=json.loads(request.body)
        user_create= data['user_create']
        location = data['location']
        print user_create
        print location
        
        objects = []
        object_list = {
            'objects': objects,
        }

        self.log_throttled_access(request)
        return self.create_response(request, object_list)

    def search(self, request, **kwargs):   
        self.method_check(request, allowed=['post','get'])
        self.is_authenticated(request)
        self.throttle_check(request)
        data=json.loads(request.body)
        querry= data['querry']
        qpage= data['page']
        print querry
        print qpage
        # Do the query.
        sqs = SearchQuerySet().models(Activity).load_all().auto_query(querry)
        paginator = Paginator(sqs, 1)

        try:
            rpage = paginator.page(qpage)
        except InvalidPage:
            raise Http404("Sorry, no results on that page.")

        objects = []

        for result in rpage.object_list:
            bundle = self.build_bundle(obj=result.object, request=request)
            bundle = self.full_dehydrate(bundle)
            objects.append(bundle)

        object_list = {
            'objects': objects,
        }

        self.log_throttled_access(request)
        return self.create_response(request, object_list)


    def invite(self,request, **kwargs):
        self.method_check(request, allowed=['post','get'])
        self.is_authenticated(request)
        self.throttle_check(request)
        data=json.loads(request.body)
        user_invite = data['user_invite']
        list_users = data['list_user']
        
        list_users = list_users.split(',')

        activity = data['activity_id']
        activity_id= str(data['activity_id'])
        message = data['message']

        list_photos = data['list_photos']
        list_photos = list_photos.split(',')
        list_videos = data['list_videos']
        list_videos = list_videos.split(',')
        #print user_invite
        
        #print activity_id 
        #print message 
        #print list_photos
        #print list_videos

        #print photo_id        
        # Do the query.
        #activity = Activity.objects.get(activity_id= activity_id)


        try:
            activity = Activity.objects.get(activity_id= activity_id)
        except Activity.DoesNotExist:
            return self.create_response(request, {'success': '0','feed_back':"the activity with your id doesn't exist"})

        
        #User.objects.get(username="itamsvtd" ) 
 
        user_invite = str(user_invite)

        print 'testtt'
        print user_invite
        user_invite =  User.objects.get(username= user_invite) 
        invitation = Invitation.objects.create(user_invite=user_invite,message=message,activity=activity)

        for p in list_photos:
            try:
                #print len(i)
                #print i 
                photo = Photo.objects.get(photo_id= p) 
               
                invitation.list_photos.add(photo)
            except Photo.DoesNotExist:
                return self.create_response(request, {'success': '0','feed_back':"the photo with your username doesn't exist"})
    
        for v in list_videos:
            try:
             
                video = Video.objects.get(video_id = v) 
               
                invitation.list_videos.add(video)
            except Video.DoesNotExist:
                return self.create_response(request, {'success': '0','feed_back':"video with your username doesn't exist"})
    


        invitation.save()
        for i in list_users:
            try:
                i = str(i)
    
                new_user = User.objects.get(username=i ) 

                user_profile = UserProfile.objects.get(user = new_user)
                user_profile.invitation.add(invitation)
                user_profile.save()
              

            except User.DoesNotExist:
                return self.create_response(request, {'success': '0','feed_back':"the user with your username doesn't exist"})

        

        print "INVITEEE"

        #users = users.split(',')
        #for u in users:
        #    print u
        #print activity.name
        #objects = []

        self.log_throttled_access(request)
        return self.create_response(request, {'success': 1})

    def add_photo(self,request, **kwargs):
        self.method_check(request, allowed=['post','get'])
        self.is_authenticated(request)
        self.throttle_check(request)
        data=json.loads(request.body)
        photo_id= data['photo_id']
        activity_id = data['activity_id']
        

        
        try:
            new_photo = Photo.objects.get(photo_id = photo_id)
        except Activity.DoesNotExist:
            return self.create_response(request, {'success': '0','feed_back':"the photo with your id doesn't exist"})

        
        try:
            activity= Activity.objects.get(activity_id = activity_id)
        except Activity.DoesNotExist:
            return self.create_response(request, {'success': '0','feed_back':"the activity with your id doesn't exist"})

        
        activity.list_photos.add(new_photo)
        activity.save()
        return self.create_response(request, {'success': 1})




