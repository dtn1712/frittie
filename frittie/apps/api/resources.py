from tastypie.resources import ModelResource
from django.contrib.auth.models import User
from tastypie.authorization import Authorization, DjangoAuthorization
from tastypie.authentication import BasicAuthentication
from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from frittie.apps.main.models import Location,UserProfile

class LocationResource(ModelResource):
    create_by = fields.ForeignKey('frittie.apps.api.resources.UserResource', 'create_by')
    class Meta:
        queryset = Location.objects.all()
        resource_name = 'Location'
        allowed_methods = ['get','post','put']
        authorization= Authorization()


class UserResource(ModelResource):
    school = fields.ToOneField('frittie.apps.api.resources.LocationResource', 'user')
    class Meta:
        queryset = UserProfile.objects.all()
        resource_name = 'User'