from django.contrib.auth.models import User
from django.core.paginator import Paginator, InvalidPage
from django.db import models
from django.contrib.auth import authenticate, login  
from django.conf.urls.defaults import url

from tastypie.authorization import Authorization, DjangoAuthorization
from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.serializers import Serializer  
from tastypie.authentication import Authentication , ApiKeyAuthentication, BasicAuthentication
from tastypie.models import create_api_key
from tastypie.models import ApiKey
from tastypie.utils import trailing_slash

from frittie.apps.main.models import Location,UserProfile,Activity, Comment, Photo, Friendship

from haystack.query import SearchQuerySet

from frittie.apps.pubnub.Pubnub import Pubnub

import json
import logging
import math
from frittie.apps.pubnub.Pubnub import Pubnub
from frittie.settings import STATIC_URL

pubnub = Pubnub( 'pub-c-85c5f004-670f-44d6-83a1-845ef1ca747c', 'sub-c-8c317ce0-2a3a-11e3-afa9-02ee2ddab7fe', None, True)

class MultipartResource(object):
    def deserialize(self, request, data, format=None):
        if not format:
            format = request.META.get('CONTENT_TYPE', 'application/json')

        if format == 'application/x-www-form-urlencoded':
            return request.POST

        if format.startswith('multipart'):
            data = request.POST.copy()
            #data = request.body()
            print data
            data.update(request.FILES)
            print data
            return data

        return super(MultipartResource, self).deserialize(request, data, format)


class LocationResource(ModelResource):
    create_by = fields.OneToOneField('frittie.apps.api.api.UserResource', 'create_by' , null=True, blank=True)
    follow_by = fields.ManyToManyField('frittie.apps.api.api.UserResource', 'follow_by', null= True, blank = True)
    class Meta:
        queryset = Location.objects.all()
        resource_name = 'Location'
        allowed_methods = ['get','post','put','delete']
        serializer = Serializer(formats=['json', 'plist'])
        authorization= DjangoAuthorization()
        filtering = {
            'name': ALL,
        }
        #authentication = ApiKeyAuthentication()

    def prepend_urls(self):
        return [
        url(r"^(?P<resource_name>%s)/(?P<name>[\w\d_.-]+)/follow_by%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('get_follow_by'), name="api_get_follow_by"),
        url(r"^(?P<resource_name>%s)/(?P<name>[\w\d_.-]+)/create_by%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('get_create_by'), name="api_get_create_by"),
        url(r"^(?P<resource_name>%s)/(?P<name>[\w\d_.-]+)/$" % self._meta.resource_name, self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        url(r"^(?P<resource_name>%s)/d/search%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('search'), name="api_search"),
        url(r"^(?P<resource_name>%s)/d/nearby%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('nearby'), name="api_get_nearby"),
        url(r"^(?P<resource_name>%s)%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('emptyurl'), name="api_get_emptyurl"),

        ]

    def get_follow_by(self, request, **kwargs):
        basic_bundle = self.build_bundle(request=request)
        obj = self.cached_obj_get(bundle=basic_bundle, **self.remove_api_resource_names(kwargs))
        user_resource = UserResource()
        try:
            user_resource._meta.queryset = obj.follow_by.all()
        except IndexError:
            user_resource._meta.queryset = UserProfile.objects.none()
        return user_resource.get_list(request)


    def get_create_by(self, request, **kwargs):
        basic_bundle = self.build_bundle(request=request)
        obj = self.cached_obj_get(bundle=basic_bundle, **self.remove_api_resource_names(kwargs))
        user_resource = UserResource()
        try:
            user_resource._meta.queryset = obj.create_by.all()
        except IndexError:
            user_resource._meta.queryset = UserProfile.objects.none()
        return user_resource.get_list(request)        
        models.signals.post_save.connect(create_api_key, sender=User)
        log = logging.getLogger(__name__)
    
    def search(self, request, **kwargs):   
        self.method_check(request, allowed=['post','get'])
        self.is_authenticated(request)
        self.throttle_check(request)
        data=json.loads(request.body)
        querry= data['querry']
        qpage= data['page']
        # Do the query.
        sqs = SearchQuerySet().models(Location).load_all().auto_query(querry)
        paginator = Paginator(sqs, 1)

        try:
            rpage = paginator.page(qpage)
        except InvalidPage:
            raise Http404("Sorry, no results on that page.")

        objects = []

        for result in rpage.object_list:
            bundle = self.build_bundle(obj=result.object, request=request)
            bundle = self.full_dehydrate(bundle)
            objects.append(bundle)

        object_list = {
            'objects': objects,
        }

        self.log_throttled_access(request)
        return self.create_response(request, object_list)

    def nearby(self, request, **kwargs):   
        self.method_check(request, allowed=['post','get'])
        self.is_authenticated(request)
        self.throttle_check(request)
        locations = Location.objects.filter(**self.remove_api_resource_names(kwargs))
        data=json.loads(request.body)
        lat= data['lat']
        lng= data['lng']
        rad= int(data['rad'])
        username = data['username']
        objects = []
        for location in locations:
            rate = 0.0
            const = 0.00
            if rad <=501:
                const = 1.14
            elif rad <=2000:
                const = 3.0
            elif rad <=5000:
                const = 5.0
            elif rad <=10000:
                const = 8.0
            if (math.sqrt(pow(float(location.lat) - float(lat),2) + pow(float(location.lng) - float(lng),2))  < const ):
                listfollow = location.follow_by.all()
                for user1 in listfollow:
                    if user1.username==username:
                        rate = len(listfollow)*0.5
                        break
                user2 = User.objects.get(username__exact=username)
                friends = Friendship.objects.friends_of(user2).values_list('username', flat=True)
                #print friends
                for friend in friends:
                    print friend
                    if friend == act.user_create.username:
                        rate = rate + 0.3
                bundle = self.build_bundle(obj=location, request=request)
                bundle = self.full_dehydrate(bundle)
                objects.append(bundle)

        return self.create_response(request, objects)

    def emptyurl(self, request, **kwargs):   
        self.method_check(request, allowed=['post','get'])
        self.is_authenticated(request)
        self.throttle_check(request)
        data=json.loads(request.body)
        user_create= data['user_create']
        location = data['location']
        print user_create
        print location
        info = pubnub.publish({
        'channel' : 'activitychannel',
        'message' : {
        'user_create' : user_create ,
        'location' : location
        }
        })
        print(info)
    
        objects = []

        object_list = {
            'objects': objects,
        }

        self.log_throttled_access(request)
        return self.create_response(request, object_list)
class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        resource_name = 'User'
        allowed_methods = ['get','post']
        serializer = Serializer(formats=['json', 'plist'])
        authorization= DjangoAuthorization()
        #authentication = ApiKeyAuthentication()
        excludes = ['id', 'email', 'password', 'is_staff', 'is_superuser']
        filtering = {
            'username': ALL,
        }

    def prepend_urls(self):
        return [
        url(r"^(?P<resource_name>%s)/signin%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('signin'), name="api_signin"),
        url(r"^(?P<resource_name>%s)/(?P<username>[\w\d_.-]+)/$" % self._meta.resource_name, self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
        ]

    def signin(self, request, **kwargs):
        self.method_check(request, allowed=['post','get'])
        data=json.loads(request.body)
        username= data['username']
        password=  data['password']
        #print username
        #print password
        user = User.objects.get(username=username)

        # Time to get the apikey for the current user
        from tastypie.models import ApiKey

        try:
            api_key = ApiKey.objects.get(user=user)
        except ApiKey.DoesNotExist:
            # Create key if user doesn't have one (not tested yet)
            api_key = ApiKey.objects.create(user=user)
        print api_key.key
        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active:
                login(request, user)
                return self.create_response(request, {'success': 1, 'api_key': api_key.key})
            else:
                # Return a 'disabled account' error message
                return self.create_response(request, {'success': 0, 'api_key': None})
        else:
             #Return an 'invalid login' error message.
            return self.create_response(request, {'success': 0, 'api_key': None})
        #authentication = ApiKeyAuthentication()
    models.signals.post_save.connect(create_api_key, sender=User)

class ActivityResource(ModelResource):
    location = fields.OneToOneField('frittie.apps.api.api.LocationResource', 'location' , null=True, blank=True)
    user_create = fields.OneToOneField('frittie.apps.api.api.UserResource', 'user_create' , null=True, blank=True)
    

    class Meta:
        queryset = Activity.objects.all()
        resource_name = 'Activity'
        allowed_methods = ['get','post','put','delete']
        serializer = Serializer(formats=['json', 'plist'])
        authorization= Authorization()
        #authentication = ApiKeyAuthentication()
        models.signals.post_save.connect(create_api_key, sender=User)
        filtering = {
            'user_create': ALL_WITH_RELATIONS,
            'location':ALL_WITH_RELATIONS,
            'pub_date': ['exact', 'lt', 'lte', 'gte', 'gt'],
        }


    def prepend_urls(self):
        return [
        url(r"^(?P<resource_name>%s)/nearby%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('nearby'), name="api_get_nearby"),
        url(r"^(?P<resource_name>%s)%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('emptyurl'), name="api_get_emptyurl"),

        url(r"^(?P<resource_name>%s)/d/search%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('search'), name="api_search"),

        url(r"^(?P<resource_name>%s)/nearbysecond%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('nearby'), name="api_get_nearby"),
        ]

    def nearby(self, request, **kwargs):   
        self.method_check(request, allowed=['post','get'])
        self.is_authenticated(request)
        self.throttle_check(request)
        activities = Activity.objects.filter(**self.remove_api_resource_names(kwargs))
        data=json.loads(request.body)
        lat= data['lat']
        lng= data['lng']
        rad= int(data['rad'])
        username = data['username']
        objects = []
        for act in activities:
            print act.location.lat
            print act.location.lng
            print float(lat)
            print float(lng)
            rate = 0.0
            const = 0.00
            if rad <=501:
                const = 1.14
            elif rad <=2000:
                const = 3.0
            elif rad <=5000:
                const = 5.0
            elif rad <=10000:
                const = 8.0
            print 'test2'
            print str(math.sqrt(pow(float(act.location.lat) - float(lat),2) + pow(float(act.location.lng) - float(lng),2)) )
            if (math.sqrt(pow(float(act.location.lat) - float(lat),2) + pow(float(act.location.lng) - float(lng),2))  < const ):
                listfollow = act.location.follow_by.all()
                for user1 in listfollow:
                    if user1.username==username:
                        rate = len(listfollow)*0.5
                        break
                user2 = User.objects.get(username__exact=username)
                friends = Friendship.objects.friends_of(user2).values_list('username', flat=True)
                #print friends
                for friend in friends:
                    print friend
                    if friend == act.user_create.username:
                        rate = rate + 0.3
              
                print "rate"
                print rate

                bundle = self.build_bundle(obj=act, request=request)
                bundle = self.full_dehydrate(bundle)
                objects.append(bundle)

        return self.create_response(request, objects)


    def emptyurl(self, request, **kwargs):   
        self.method_check(request, allowed=['post','get'])
        self.is_authenticated(request)
        self.throttle_check(request)
        data=json.loads(request.body)
        user_create= data['user_create']
        location = data['location']
        print user_create
        print location
        info = pubnub.publish({
        'channel' : 'activitychannel',
        'message' : {
        'user_create' : user_create ,
        'location' : location
        }
        })
        print(info)
        objects = []
        object_list = {
            'objects': objects,
        }

        self.log_throttled_access(request)
        return self.create_response(request, object_list)

    def search(self, request, **kwargs):   
        self.method_check(request, allowed=['post','get'])
        self.is_authenticated(request)
        self.throttle_check(request)
        data=json.loads(request.body)
        querry= data['querry']
        qpage= data['page']
        print querry
        print qpage
        # Do the query.
        sqs = SearchQuerySet().models(Activity).load_all().auto_query(querry)
        paginator = Paginator(sqs, 1)

        try:
            rpage = paginator.page(qpage)
        except InvalidPage:
            raise Http404("Sorry, no results on that page.")

        objects = []

        for result in rpage.object_list:
            bundle = self.build_bundle(obj=result.object, request=request)
            bundle = self.full_dehydrate(bundle)
            objects.append(bundle)

        object_list = {
            'objects': objects,
        }

        self.log_throttled_access(request)
        return self.create_response(request, object_list)

class CommentResource(ModelResource):
    user = fields.OneToOneField('frittie.apps.api.api.UserResource','user', null= True, blank = True)
    class Meta:
        queryset = Comment.objects.all()
        resource_name = 'Comment'
        allowed_methods = ['get','post','put','delete']
        serializer = Serializer(formats=['json', 'plist'])
        authorization= DjangoAuthorization()
        authentication = ApiKeyAuthentication()
        models.signals.post_save.connect(create_api_key, sender=User)


class PhotoResource(MultipartResource,ModelResource):
    user_post = fields.OneToOneField('frittie.apps.api.api.UserResource','user_post', null=True, blank=True)
    object_id = fields.IntegerField('object_id', null=True, blank=True)

    class Meta:
        queryset = Photo.objects.all()
        resource_name = 'Photo'
        #allowed_methods = ['get','post','put','delete','patch']
        
        list_allowed_methods = ['get', 'post']
        detail_allowed_methods = ['get']

        serializer = Serializer(formats=['json', 'plist'])

        authorization= Authorization()

        models.signals.post_save.connect(create_api_key, sender=User)

        fields = ('filename', 'image','photo_id','photo_type','type_id')

        #authorization= Authorization()
        #authentication = ApiKeyAuthentication()

    def prepend_urls(self):
        return [
        url(r"^(?P<resource_name>%s)/get_photo_url%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('get_photo_url'), name="get_photo_url"),
        ]


    def get_photo_url(self, request, **kwargs):   
        self.method_check(request, allowed=['post','get'])
        self.is_authenticated(request)
        self.throttle_check(request)
        data=json.loads(request.body)
        photo_id= str(data['photo_id'])
    
        print photo_id
        
        # Do the query.
        photo = Photo.objects.get(photo_id = photo_id)

        

        #photo_url = "http://127.0.0.1:8000" + photo.image.url
        photo_url = STATIC_URL + photo.image.url

        return self.create_response(request, {'photo_url': photo_url})