from django.contrib.auth.models import User
from django.core.paginator import Paginator, InvalidPage
from django.db import models
from django.contrib.auth import authenticate, login  
from django.conf.urls import patterns, url, include

from tastypie.authorization import Authorization, DjangoAuthorization
from tastypie import fields
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from frittie.apps.api.multipartresource import MultipartResource 
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tastypie.serializers import Serializer  
from tastypie.authentication import Authentication , ApiKeyAuthentication, BasicAuthentication
from tastypie.models import create_api_key
from tastypie.models import ApiKey
from tastypie.utils import trailing_slash

from frittie.apps.main.models import Photo

from haystack.query import SearchQuerySet

from frittie.apps.pubnub.Pubnub import Pubnub

import json
import logging
import math
from frittie.apps.pubnub.Pubnub import Pubnub
from frittie.settings import STATIC_URL
class PhotoResource(MultipartResource,ModelResource):
    user_post = fields.OneToOneField('frittie.apps.api.userapi.UserResource','user_post', null=True, blank=True)
    object_id = fields.IntegerField('object_id', null=True, blank=True)

    class Meta:
        queryset = Photo.objects.all()
        resource_name = 'Photo'
        #allowed_methods = ['get','post','put','delete','patch']
        
        list_allowed_methods = ['get', 'post']
        detail_allowed_methods = ['get']

        serializer = Serializer(formats=['json', 'plist'])

        authorization= Authorization()
        #authentication = ApiKeyAuthentication()
        models.signals.post_save.connect(create_api_key, sender=User)

        fields = ('filename', 'image','photo_id','photo_type','type_id')

        #authorization= Authorization()
        #authentication = ApiKeyAuthentication()

    def prepend_urls(self):
        return [
        url(r"^(?P<resource_name>%s)/get_photo_url%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('get_photo_url'), name="get_photo_url"),
        ]


    def get_photo_url(self, request, **kwargs):   
        self.method_check(request, allowed=['post','get'])
        self.is_authenticated(request)
        self.throttle_check(request)
        data = request.POST.dict()
        photo_id= str(data['photo_id'])
    
        print photo_id
        
        # Do the query.
        photo = Photo.objects.get(photo_id = photo_id)

        

        #photo_url = "http://127.0.0.1:8000" + photo.image.url
        photo_url = photo.image.url

        return self.create_response(request, {'photo_url': photo_url})