from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError, Http404
from django.core.urlresolvers import reverse
from django.utils.html import strip_tags
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404, get_list_or_404
from django.views.decorators.csrf import csrf_exempt, requires_csrf_token
from django.contrib.auth.models import User
from django.utils import simplejson, timezone
from django.core import serializers
from django.contrib.auth.decorators import login_required

from frittie.apps.member.helper import get_user_common_info
from frittie.apps.activity.helper import get_activity_common_info, generate_confirmation_id
from frittie.apps.ticket.helper import get_ticket_transactions_common_info, export_ticket_file
from frittie.apps.mailer.tasks import send_email, send_mass_email
from frittie.apps.app_helper import get_user_login_object, generate_unique_id, convert_ampm_to_24hr
from frittie.apps.app_helper import convert_24hr_to_ampm, make_two_numbers, get_current_time_zone

from frittie.apps.main.models import Activity, Comment, Photo, ActivityRequest, Location , ConfirmationActivity, Ticket, TicketTransaction, EmailTracking

from frittie.apps.activity.forms import ActivityForm

from frittie.apps.main.constants import PUBLIC, PRIVATE, REQUEST_RESERVED, REQUEST_INVITED, UNLIMITED, DEFAULT_TIMEZONE, TICKET_CLOSE, TICKET_OPEN

from frittie.settings import WEBSITE_HOMEPAGE

import datetime, logging

logger = logging.getLogger(__name__)

APP_NAME = "ticket"

@login_required
@requires_csrf_token
def reserve_ticket(request):
	if request.method == "POST":
		user_login = get_user_login_object(request)
		request.session['is_show_request_message'] = True
		try:
			activity_unique_id = request.POST['activity_unique_id']
			activity = Activity.objects.get(unique_id=activity_unique_id)
			total_ticket_bought = 0
			transactions = []
			for ticket in activity.tickets.all():
				number = 0
				select_quantity_ticket = 'select_quantity_ticket_' + str(ticket.pk)
				if select_quantity_ticket in request.POST:
					number = int(request.POST[select_quantity_ticket])
				total_ticket_bought += number
				if number > 0 and number <= ticket.total_available_quantity:
					ticket.total_available_quantity -= int(number)
					if ticket.total_available_quantity == 0:
						ticket.ticket_status = TICKET_CLOSE
					ticket.save()
					transaction = TicketTransaction(activity=activity,ticket=ticket,user=user_login,quantity=number,total_cost=ticket.price * int(number))
					transaction.save()
					transactions.append(transaction)
			if total_ticket_bought > 0:		
				new_confirmation = ConfirmationActivity.objects.create(user_confirm=user_login,confirmation_id=generate_confirmation_id(user_login),is_confirmed="0",activity=activity)
				activity.confirmations.add(new_confirmation)
				
				activity_requests = ActivityRequest.objects.filter(activity=activity,user_request=user_login)
				if len(activity_requests) == 0:
					activity_request = ActivityRequest.objects.create(activity=activity,user_request=user_login,introduction="Hello",request_status=REQUEST_RESERVED)
					activity.activity_requests.add(activity_request)
				else:
					activity_request = activity_requests[0]
					activity_request.request_status = REQUEST_RESERVED
					activity_request.save()

				activity.save()

				context1 = {
					"user": get_user_common_info(user_login),
					"activity" : get_activity_common_info(activity),
					"url": WEBSITE_HOMEPAGE + "activity/" + activity_unique_id,
					"confirmation_id": new_confirmation.confirmation_id,
					"ticket_transactions": get_ticket_transactions_common_info(transactions),
				}
				if "total_price" in request.POST and "currency_used" in request.POST:
					context1['total_price'] = request.POST['total_price']
					context1['currency'] = request.POST['currency_used']
				email_tracking1 = EmailTracking.objects.create(to_emails=user_login.email,email_template='reserve_ticket_confirmation',context_data=str(context1))
				send_email.delay(email_tracking1.id)

				context2 = {
					"user": get_user_common_info(user_login),
					"activity" : get_activity_common_info(activity),
					"url": WEBSITE_HOMEPAGE + "activity/" + activity_unique_id,
				}
				email_tracking2 = EmailTracking.objects.create(to_emails=activity.user_create.email,email_template='user_reserve_ticket_notice',context_data=str(context2))
				send_email.delay(email_tracking2.id)

				return HttpResponseRedirect("/activity/" + activity_unique_id + "?action=reserve_ticket&result=success" )
		except Exception as e:
			logger.exception(e)
			return HttpResponseRedirect("/activity/" + activity_unique_id + "?action=reserve_ticket&result=error") 
	return HttpResponseRedirect("/")

@login_required
@requires_csrf_token
def add_ticket(request):
	if request.method == "POST":
		user_login = get_user_login_object(request)
		request.session['is_show_request_message'] = True
		try:
			activity_unique_id = request.POST['activity_unique_id']
			activity = Activity.objects.get(unique_id=activity_unique_id)
			ticket_type = request.POST['ticket_type']
			price = request.POST['price']
			quantity = request.POST['max_quantity']
			available = request.POST['total_available']
			currency = request.POST['currency']
			new_ticket = Ticket.objects.create(price=float(price),fee=0,ticket_name=ticket_type,currency=currency,total_available_quantity=available,maximum_quantity_transaction=quantity)
			activity.tickets.add(new_ticket)
			activity.save()
			return HttpResponseRedirect("/activity/" + activity_unique_id + "?show_ticket=True")
		except Exception as e:
			logger.exception(e)
			return HttpResponseRedirect("/activity/" + activity_unique_id + "?action=add_ticket&result=error")
	return HttpResponseRedirect("/") 


@login_required
@requires_csrf_token
def edit_ticket(request):
	if request.method == "POST":
		request.session['is_show_request_message'] = True
		try:
			activity_unique_id = request.POST["activity_unique_id"]
			ticket_id = request.POST["ticket_id"]
			ticket = Ticket.objects.get(id=ticket_id)
			ticket.ticket_name = request.POST['ticket_type']
			ticket.currency = request.POST['currency']
			ticket.price = float(request.POST['price'])
			ticket.total_available_quantity = int(request.POST['total_available'])
			ticket.maximum_quantity_transaction = int(request.POST['max_quantity'])
			ticket.save()
			return HttpResponseRedirect("/activity/" + activity_unique_id + "?show_ticket=True")
		except Exception as e:
			logger.exception(e)
			return HttpResponseRedirect("/activity/" + activity_unique_id + "?action=edit_ticket&result=error")
	return HttpResponseRedirect("/")


@login_required
@requires_csrf_token
def delete_ticket(request):
	if request.method == "POST":
		activity_unique_id = request.POST["activity_unique_id"]
		ticket_id = request.POST["ticket_id"]
		request.session['is_show_request_message'] = True
		try:
			ticket = Ticket.objects.get(id=ticket_id)
			ticket.delete()
			return HttpResponseRedirect("/activity/" + activity_unique_id + "?show_ticket=True")
		except Exception as e:
			logger.exception(e)
			return HttpResponseRedirect("/activity/" + activity_unique_id + "?action=delete_ticket&result=error")
	return HttpResponse("/")

@login_required
def export_ticket(request):
	if "type" and "activity_unique_id" in request.GET:
		activity = get_object_or_404(Activity,unique_id=request.GET['activity_unique_id'])
		user_login = get_user_login_object(request)
		if activity.user_create != user_login:
			return HttpResponseRedirect("/")
		output_type = request.GET['type']
		if output_type.lower() != "csv" and output_type.lower() != "xlsx":
			return HttpResponseRedirect("/")
		else:
			output = export_ticket_file(activity,output_type)
			output.seek(0)
			response = HttpResponse(output.read(), mimetype="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
			response['Content-Disposition'] = 'attachment; filename='+ activity.name.replace(" ","_") + "." + output_type.lower()
			return response
	return HttpResponseRedirect("/")




