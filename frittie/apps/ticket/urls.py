from django.conf.urls import *

urlpatterns = patterns('frittie.apps.ticket.views',
    url(r'^create/$','add_ticket',name="add_ticket"),
    url(r'^delete/$', 'delete_ticket', name="delete_ticket"),
    url(r"^edit/$","edit_ticket",name="edit_ticket"),
    url(r"^reserve/$","reserve_ticket", name="reserve_ticket"),
    url(r'^export/$',"export_ticket",name='export_ticket')
)

