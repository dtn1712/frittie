from frittie.apps.main.models import TicketTransaction, Ticket, Activity
from frittie.apps.member.helper import get_user_common_info

from frittie.apps.app_helper import get_display_time

import logging, xlsxwriter

try:
    import cStringIO as StringIO
except ImportError:
    import StringIO

logger = logging.getLogger(__name__)

def get_ticket_common_info(ticket):
	try:
		results = {
			"ticket_name": ticket.ticket_name,
			"ticket_status": ticket.ticket_status,
			"price": float(ticket.price),
			"currency": ticket.currency,
			"total_available_quantity": int(ticket.total_available_quantity),
			"maximum_quantity_transaction": int(ticket.maximum_quantity_transaction)
		}
		return results
	except Exception as e:
		logger.exception(e)
	return None

def get_ticket_transactions_common_info(transactions):
	try:
		results = []
		for transaction in transactions:
			data = {
				"ticket": get_ticket_common_info(transaction.ticket),
				"user": get_user_common_info(transaction.user),
				"quantity": int(transaction.quantity),
				"total_cost": float(transaction.total_cost),
				"transaction_date": get_display_time(transaction.transaction_date),
			}
			results.append(data)
		return results
	except Exception as e:
		logger.exception(e)
	return None


def export_ticket_file(activity,output_type):
	output = StringIO.StringIO()
	workbook = xlsxwriter.Workbook(output)
	worksheet = workbook.add_worksheet()
	bold = workbook.add_format({'bold': True})
	money = workbook.add_format({'num_format': '$#,##0'})
	worksheet.write('A1', 'Attendee', bold)
	worksheet.write('B1', 'Email', bold)
	worksheet.write('C1', 'Ticket Type', bold)
	worksheet.write('D1', 'Quantity', bold)
	worksheet.write("E1", "Transaction Date", bold)
	worksheet.write('F1', 'Cost', bold)
	row = 1
	col = 0
	for ticket_buyer in activity.get_ticket_buyers():
	    worksheet.write(row, col, ticket_buyer.get_profile().get_user_fullname())
	    worksheet.write(row, col + 1, ticket_buyer.email)
	    ticket_transactions = activity.get_ticket_transactions()
	    total_cost = 0
	    for ticket_transaction in ticket_transactions:
	    	worksheet.write(row, col + 2, ticket_transaction.ticket.ticket_name)
	    	worksheet.write(row, col + 3, ticket_transaction.quantity)
	    	worksheet.write(row, col + 4, ticket_transaction.transaction_date)
	    	worksheet.write(row, col + 5, ticket_transaction.total_cost)
	    	total_cost += ticket_transaction.total_cost
	    	row +=1
	    worksheet.write(row, col + 4, "Total:" + str(total_cost),bold)
	    row += 1
	workbook.close()
	return output




