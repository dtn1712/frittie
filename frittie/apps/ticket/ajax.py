from dajax.core import Dajax
from dajaxice.decorators import dajaxice_register

from django.core import serializers
from django.contrib.auth.models import User
from django.db import connection

from frittie.apps.app_helper import generate_html_snippet, get_user_login_object, generate_message
from frittie.apps.main.models import Activity, Ticket

import json, logging

logger = logging.getLogger(__name__)


@dajaxice_register
def add_ticket(request,activity_unique_id,ticket_type,price,max_quantity,total_available,currency):
	results = {}
	try:
		activity = Activity.objects.get(unique_id=activity_unique_id)
		new_ticket = Ticket.objects.create(price=float(price),fee=0,ticket_name=ticket_type,currency=currency,total_available_quantity=available,maximum_quantity_transaction=quantity)
		activity.tickets.add(new_ticket)
		activity.save()
		num_ticket = len(activity.tickets.all())
		results['success'] = True
		results['num_ticket'] = num_ticket
		results['snippet_return'] = generate_html_snippet(request,"ticket_item",{"ticket": new_ticket, "num_ticket": num_ticket})
	except Exception as e:
		logger.exception(e)
		results['success'] = False
	return json.dumps({"results": results})

@dajaxice_register
def edit_ticket(request,ticket_id,ticket_type,currency,price,total_available,max_quantity):
	results = {}
	try:
		ticket = Ticket.objects.get(id=ticket_id)
		ticket.ticket_name = ticket_type
		ticket.currency = currency
		ticket.price = float(price)
		ticket.total_available_quantity = int(total_available)
		ticket.maximum_quantity_transaction = int(max_quantity)
		ticket.save()
		results['success'] = True
		results['snippet_return'] = generate_html_snippet(request,"ticket_item",{"ticket": ticket})
	except Exception as e:
		logger.exception(e)
		results['success'] = False
	return json.dumps({"results": results})

@dajaxice_register
def calculate_preview_ticket_price(request,activity_unique_id,ticket_pks,ticket_quantities):
	results = {}
	try:
		tickets = []
		quantities =[]
		for i in ticket_quantities:
			quantities.append(int(i))
		j = 0 
		total_price = 0 
		currency = ""
		for i in ticket_pks:
			n_ticket = Ticket.objects.get(pk=int(i))
			currency = n_ticket.currency
			tickets.append(n_ticket)
			tickets[len(tickets)-1].quantity = quantities[j]
			total_price = total_price + n_ticket.price*quantities[j]
			j = j + 1 
		user_login = get_user_login_object(request)
		activity = Activity.objects.get(unique_id=activity_unique_id)
		results['success'] = True
		results['snippet_return'] = generate_html_snippet(request,"preview_calculate_ticket",{"tickets": tickets, "total_price":total_price, "currency":currency})        
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})   
	return json.dumps({"results": results})


	