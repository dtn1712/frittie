from dajax.core import Dajax
from dajaxice.decorators import dajaxice_register
from django.core import serializers
from django.core.cache import cache

from frittie.apps.main.models import Location, Comment, Report, Activity, LocationBusinessHour, LocationDetailInfo
from frittie.apps.app_helper import get_user_login_object, generate_html_snippet
from frittie.apps.app_helper import generate_unique_id, generate_message
from frittie.apps.notification.tasks import send_notification
from frittie.apps.feed.tasks import create_feed

from frittie.apps.main.constants import LIST_ACTIVITY, COMMENT_ITEM, LOCATION_PRIVATE_PLACE
from frittie.apps.main.constants import DEFAULT_LIST_LOCATION_HTML_TEMPLATE, DEFAULT_CITY

from frittie.apps.app_settings import LOCATION_PAGE

from helper import get_location_common_info

import demjson, json, datetime, logging

logger = logging.getLogger(__name__)

@dajaxice_register
def get_location_overlays(request,data):
	results = {}
	try:
		overlays = []
		for item in data:
			try:
				location_unique_id = item['location_unique_id']
				location = Location.objects.get(unique_id=location_unique_id)
				location_number = item['location_number']
				location_data = get_location_common_info(location)
				overlays.append({
					"location_data": location_data,
					"location_unique_id": location_unique_id,
					"location_number": location_number,
				})
			except:
				pass
		results['success'] = True
		results['overlays'] = overlays
	except Exception as e:
		logger.exception(e)
		results['success'] = False
	return json.dumps({"results": results})

@dajaxice_register
def filter_location(request,filter_category_value,current_city,html_template):
	results = {}
	try:
		if filter_category_value == None: filter_category_value = DEFAULT_FILTER_CATEGORY
		if html_template == None: html_template = DEFAULT_LIST_LOCATION_HTML_TEMPLATE
		if current_city == None: current_city = DEFAULT_CITY

		user_login = get_user_login_object(request)
		locations = None         
		if filter_category_value == "-1":
			locations = Location.objects.filter(city=current_city).exclude(category=LOCATION_PRIVATE_PLACE)
		else:
			locations = Location.objects.filter(city=current_city,category=filter_category_value)       
		results['success'] = True
		results['snippet_return'] = generate_html_snippet(request,html_template,{"locations": locations})
		results['num_locations'] = len(locations)
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})   
	return json.dumps({"results": results})

@dajaxice_register
def load_activity(request,location_unique_id,select_date,sort_type):
	"""We need to cache this request because it's not user-specific
		Therefore, it's likely that different users will get the same data
	"""
	unique_key = str(location_unique_id) + str(select_date) + str(sort_type)
	results = cache.get(unique_key) if cache != None else None
	if results == None:
		results = {}
		try:
			date = datetime.datetime(int(select_date[0:4]),int(select_date[5:7]),int(select_date[8:10]))
			location = Location.objects.get(unique_id=location_unique_id)
			activities = []
			if sort_type == "hot":
				activities = location.get_hot_activities(date)
			else:
				activities = location.get_upcoming_activities(date)
			snippet_return = generate_html_snippet(request,LOCATION_PAGE[LIST_ACTIVITY],{"activities": activities})
			results['success'] = True
			results['snippet_return'] = snippet_return
			results['sort_type'] = sort_type
			print sort_type
			results['num_activity'] = len(activities)
			results = json.dumps({"results": results})
			cache.set(unique_key, results)
		except Exception as e:
			logger.exception(e)
			results['success'] = False
			results['error_message'] = generate_message("ajax_request","fail",{})
	return results


@dajaxice_register
def follow_location(request,location_unique_id):
	results = {}
	try:
		user_login = get_user_login_object(request)
		location = Location.objects.get(unique_id=location_unique_id)
		location.follow_by.add(user_login)
		location.save()
		results['success'] = True
		results['num_follow'] = location.follow_by.count()

		# Generate Notification and Feed
		subj = { "main": user_login,"display": "user","related": user_login.get_profile().followers.all()}
		action = "follow"
		obj = { "main": location, "object_type": "location", "display": "location", "related": location.follow_by.all()}
		generate_feed(subj,action,obj)
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})	
	return json.dumps({"results": results})

	
@dajaxice_register
def unfollow_location(request,location_unique_id):
	results = {}
	try:
		user_login = get_user_login_object(request)
		location = Location.objects.get(unique_id=location_unique_id)
		location.follow_by.remove(user_login)
		results['success'] = True
		results['num_follow'] = len(location.follow_by.all())
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})		
	return json.dumps({"results": results})


@dajaxice_register
def add_comment(request,location_unique_id,comment_content):
	results = {}
	try:
		user_login = get_user_login_object(request)
		location = Location.objects.get(unique_id=location_unique_id)
		new_comment = Comment.objects.create(comment_type='location',content=comment_content,
											 user=user_login,unique_id=generate_unique_id("comment"))
		location.comments.add(new_comment)
		location.save()
		results['success'] = True
		results['snippet_return'] = generate_html_snippet(request,LOCATION_PAGE[COMMENT_ITEM],{"comment": new_comment})
		results['num_comment'] = location.comments.count()

		# Generate Notification and Feeds
		subj = { "main": user_login,"display": "user","related": user_login.get_profile().followers.all()}
		action = "comment"
		obj = { "main": location, "object_type": "location", "display": "location", "related": location.follow_by.exclude(username=user_login.username)}
		
		send_notification.delay(subj,action,obj)
		#create_feed.delay(subj,action,obj)
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})	
	return json.dumps({"results": results})

@dajaxice_register
def edit_comment(request,comment_unique_id,comment_content):
	results = {}
	try:
		user_login = get_user_login_object(request)
		comment = Comment.objects.get(unique_id=comment_unique_id)
		comment.content = comment_content
		comment.edit_date = datetime.datetime.now()
		comment.save()
		results['success'] = True
		results['comment_unique_id'] = comment_unique_id
		results['snippet_return'] = generate_html_snippet(request,LOCATION_PAGE[COMMENT_ITEM],{"comment": comment})
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})
	return json.dumps({"results": results})



@dajaxice_register
def delete_comment(request,location_unique_id,comment_unique_id):
	results = {}
	try:
		comment = Comment.objects.get(unique_id=comment_unique_id)
		location = Location.objects.get(unique_id=location_unique_id)
		location.comments.remove(comment)
		comment.delete()
		results['success'] = True
		results['comment_unique_id'] = comment_unique_id
		results['num_comment'] = location.comments.count()
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})
	return json.dumps({"results": results})

@dajaxice_register
def report_comment(request,comment_unique_id,report_content):
	results = {}
	try:
		user_login= get_user_login_object(request)
		comment = Comment.objects.get(unique_id=comment_unique_id)
		report = Report.objects.create(report_type='comment',user_report=user_login,
									   report_content=report_content,unique_id=generate_unique_id('report'))
		report.save()
		comment.reports.add(report)
		comment.save()
		results['success'] = True
		results['success_message'] = generate_message("report","success",{})
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})
	return json.dumps({"results": results})


@dajaxice_register
def report_location(request,location_unique_id,report_content):
	results = {}
	try:
		user_login = get_user_login_object(request)
		location = Location.objects.get(unique_id=location_unique_id)
		report = Report.objects.create(report_type='location',user_report=user_login,
									   report_content=report_content,unique_id=generate_unique_id('report'))
		report.save()
		location.reports.add(report)
		location.save()
		results['success'] = True
		results['success_message'] = generate_message("report","success",{})
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})
	return json.dumps({"results": results})


@dajaxice_register
def add_business_hour(request,location_unique_id,weekday,hour_start,hour_end):
	results = {}
	try:
		location = Location.objects.get(unique_id=location_unique_id)
		new_business_hour = LocationBusinessHour.objects.create(location=location,weekday=weekday,hour_start=hour_start,hour_end=hour_end)
		new_business_hour.save()
		results['success'] = True
		results['snippet_return'] = generate_html_snippet(request,'business_hours_location',{'business_hours': location.get_business_hours()})
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})
	return json.dumps({"results": results})


@dajaxice_register
def delete_business_hour(request,business_hour_id):
	results = {}
	try:
		business_hour = LocationBusinessHour.objects.get(pk=business_hour_id)
		location = business_hour.location
		business_hour.delete()
		results['success'] = True
		results['snippet_return'] = generate_html_snippet(request,'business_hours_location',{'business_hours': location.get_business_hours()})
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})
	return json.dumps({"results": results})



@dajaxice_register
def add_detail_info(request,location_unique_id,info,value):
	results = {}
	try:
		location = Location.objects.get(unique_id=location_unique_id)
		new_detail_info = LocationDetailInfo.objects.create(location=location,info=info,value=value)
		new_detail_info.save()
		results['success'] = True
		results['snippet_return'] = generate_html_snippet(request,'detail_info_location',{'detail_info': location.get_detail_info()})
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})
	return json.dumps({"results": results})


@dajaxice_register
def delete_detail_info(request,detail_info_id):
	results = {}
	try:
		detail_info = LocationDetailInfo.objects.get(pk=detail_info_id)
		location = detail_info.location
		detail_info.delete()
		results['success'] = True
		results['snippet_return'] = generate_html_snippet(request,'detail_info_location',{'detail_info': location.get_detail_info()})
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})
	return json.dumps({"results": results})


@dajaxice_register
def check_google_place_exist(request,google_place_id,address):
	results = {}
	results['address'] = address
	results['google_place_id'] = google_place_id
	try:
		exist_locations = Location.objects.filter(google_place_id=google_place_id)
		if len(exist_locations) == 0:
			results['is_exist'] = False
		else:
			results['is_exist'] = True
			results['location_unique_id'] = exist_locations[0].unique_id
	except Exception as e:
		logger.exception(e)
		results['is_exist'] = False
	return json.dumps({"results": results})


