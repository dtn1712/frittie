from django.conf.urls import *

urlpatterns = patterns('frittie.apps.location.views',  
   url(r"^create/$","create_location",name="create_location"),
   url(r"^delete/$","delete_location",name="delete_location"),
   url(r"^(?P<location_unique_id>\w+)/edit/$","edit_location",name="edit_location"),
   url(r"^(?P<location_unique_id>\w+)/update/picture/$","update_picture",name="update_picture_location"),
   url(r"^(?P<location_unique_id>\w+)/$","main_page",name="location_main"),
)

