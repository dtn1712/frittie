from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.sessions.models import Session
from django.contrib.auth.models import User
from django.contrib.gis.geoip import GeoIP
from django.core import serializers
from django.utils import simplejson

from frittie.apps.main.models import Notification, Location
from frittie.apps.main.models import Location, Photo

from frittie.apps.app_helper import convert_queryset_to_list, get_user_login_object
from frittie.apps.app_helper import get_any_admin_object, json_encode_decimal
from frittie.apps.app_helper import get_client_ip

from frittie.apps.app_settings import DEFAULT_IMAGE_PATH_MAPPING, DEFAULT_IMAGE_UNIQUE_ID
from frittie.apps.main.constants import LOCATION_PRIVATE_PLACE, DEFAULT_CITY

import logging

logger = logging.getLogger(__name__)

def get_current_city(request,city):
	if city == None:
		g = GeoIP()
		client_ip = get_client_ip(request)	
		geodata = g.city(client_ip)
		if geodata != None:
			return geodata['city']
		else:
			return DEFAULT_CITY
	else:
		return city


def get_nearby_location(request,city=None,start=0,end=20):
	locations = []
	try:
		current_city = get_current_city(request,city)
		locations = list(Location.objects.filter(city=current_city).exclude(category=LOCATION_PRIVATE_PLACE,main_picture=None).order_by("name").select_related().prefetch_related()[start:end])
	except Exception as e:
		logger.exception(e)
	return locations

def get_location_common_info(location):
	try:
		result = {
				"unique_id": location.unique_id,
				"name": location.name,
				"description": location.description,
				"lat": str(location.lat),
				"lng": str(location.lng),
				"type": location.category,
				"address1": location.address1,
				"address2": location.address2,
				"city": location.city,
				"state": location.state,
				"zip": location.zip_code,
				"country": location.country,
				"main_picture": location.main_picture.image.url,
				"num_follower": location.follow_by.count(),
				"creator_username": location.create_by.username,
				"creator_fullname": location.create_by.get_profile().get_user_fullname(),
		}
		return result
	except Exception as e:
		logger.exception(e)
	return None

def get_location_common_info_json(location):
	data = get_location_common_info(location)
	if data == None:
		return json.dumps({})
	else:
		return json.dumps(data)
		

def get_location_category_photo(category):
	key = "location_category_" +  category.lower()
	try:
		photo = Photo.objects.get(unique_id=DEFAULT_IMAGE_UNIQUE_ID[key])
		return photo
	except Photo.DoesNotExist:
		try:
			admin = get_any_admin_object()
			new_photo = Photo.objects.create(caption=key,user_post=admin,
							unique_id=DEFAULT_IMAGE_UNIQUE_ID[key],
							image=DEFAULT_IMAGE_PATH_MAPPING[key])
			return new_photo
		except Exception as e:
			logger.exception(e)
	return None


	