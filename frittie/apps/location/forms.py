from django import forms
from frittie.apps.main.constants import LOCATION_CATEGORY, USA_STATES, DEFAULT_COUNTRIES_FORMAT

class LocationForm(forms.Form):
    name = forms.CharField(max_length=200,widget=forms.TextInput(attrs={'class' : 'form-control', "required": "required"}))
    description = forms.CharField(widget=forms.Textarea(attrs={'cols': 50, 'rows': 3, "class": "form-control","required": "required"}))
    category = forms.ChoiceField(choices=LOCATION_CATEGORY,widget=forms.Select(attrs={'class' : 'form-control'}))
    address1 = forms.CharField(max_length=100,widget=forms.TextInput(attrs={'class' : 'form-control address-field', "required": "required"}))
    address2 = forms.CharField(required=False,max_length=100,widget=forms.TextInput(attrs={'class' : 'form-control address-field'}))
    city = forms.CharField(max_length=100,widget=forms.TextInput(attrs={'class' : 'form-control address-field',"required": "required"}))
    state = forms.ChoiceField(choices=USA_STATES,widget=forms.Select(attrs={'class' : 'form-control address-field'}))
    zip_code = forms.CharField(max_length=10,widget=forms.TextInput(attrs={'class' : 'form-control address-field',"numeric":"numeric"}))
    country = forms.ChoiceField(choices=DEFAULT_COUNTRIES_FORMAT,initial='US',widget=forms.Select(attrs={'class' : 'form-control address-field'})) 
    website = forms.URLField(required=False,widget=forms.TextInput(attrs={'class' : 'form-control',"valid-url":"valid-url"}))
    phone = forms.CharField(max_length=20,required=False,widget=forms.TextInput(attrs={'class' : 'form-control',"valid-phone-number":"valid-phone-number"}))
