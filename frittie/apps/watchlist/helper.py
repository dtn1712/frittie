from datetime import datetime, timedelta

from frittie.apps.app_helper import get_user_login_object, get_current_time_at_client_time_zone 
from frittie.apps.app_helper import get_current_time_zone, get_different_hours_from_timezones

import pytz, calendar

def check_today(first, second,current_timezone):
    different_hours =  get_different_hours_from_timezones(first.timezone,current_timezone)
    second = second - timedelta(hours=different_hours)
    return ( (first.start_time.day == second.day) & (first.start_time.month == second.month) & ( first.start_time.year == second.year) )


def check_yesterday(first,second,current_timezone):
    different_hours =  get_different_hours_from_timezones(first.timezone,current_timezone)
    second = second - timedelta(hours=different_hours)
    if ( ( first.end_time.month == 12 ) & (second.month == 1 ) & (first.end_time.year + 1 == second.year) ) :
        if ( first.end_time.day == 30) & ( second.day == 1 ):
            return True

    if ( (first.end_time.month == second.month) & ( first.end_time.year == second.year) & (first.end_time.day + 1 == second.day) ):
        return True

    odd_month = [1,3,5,7,9,11]
    if first.end_time.month in odd_month:
        if ( (first.end_time.month + 1 == second.month) & ( first.end_time.year == second.year) & (first.end_time.day == 30 ) & (second.day == 1)):
            return True
    else:
        if first.end_time.month != 2 :
            if ( (first.end_time.month + 1 == second.month) & ( first.end_time.year == second.year) & (first.end_time.day == 31 ) & (second.day == 1)):
                return True
        else:
            if (first.end_time.year % 4 == 0 ):
                if ( (first.end_time.month + 1 == second.month) & ( first.end_time.year == second.year) & (first.end_time.day == 29 ) & (second.day == 1)):
                    return True
            else:
                if ( (first.end_time.month + 1 == second.month) & ( first.end_time.year == second.year) & (first.end_time.day == 28 ) & (second.day == 1)):
                    return True
    return False


def check_tomorrow(first,second,current_timezone):
    different_hours =  get_different_hours_from_timezones(first.timezone,current_timezone)
    second = second - timedelta(hours=different_hours)
    if first.start_time.year == second.year:
        if first.start_time.month == second.month:
            if first.start_time.day == (second.day+1): # same year same month
                return True
            return False
        elif first.start_time.month == (second.month+1): # last day of this month and 1st day of next month
            if (first.start_time.day == 1) & (second.day == calendar.monthrange(second.year, second.month)[1]):
                return True
            return False
    elif first.start_time.year == (second.year+1): # last day of this year and 1st day of next year
        if (first.start_time.month == 1 & second.month == 12 & first.start_time.day == 1 & second.day == 31):
            return True
        return False
    return False


def check_happen_in(first, second, dates):
    if first.year == second.year:
        if first.month == second.month:
            if first.day == (second.day+dates): # same year same month
                return True
            return False
        elif first.month == (second.month+1):
            monthdates = calendar.monthrange(second.year, second.month)[1]
            if (first.day == monthdates - second.day + dates):
                return True
            return False
    elif first.year == (second.year+1):
        if (first.month == 1 & second.month == 12 & first.day == 31 - second.day + dates):
            return True
        return False
    return False


def check_last_week(first, second,current_timezone):
    different_hours =  get_different_hours_from_timezones(first.timezone,current_timezone)
    begin_last_week = second - timedelta(second.weekday()) - timedelta(7)
    end_last_week = begin_last_week + timedelta(7)
    # convert time for timezone
    begin_last_week =  begin_last_week - timedelta(hours=different_hours)
    end_last_week = end_last_week  - timedelta(hours=different_hours)
    if (first.start_time.replace(tzinfo=None) > begin_last_week.replace(tzinfo=None)) & (first.start_time.replace(tzinfo=None) < end_last_week.replace(tzinfo=None)):
        return True
    return False


def check_next_week(first, second,current_timezone):
    different_hours =  get_different_hours_from_timezones(first.timezone,current_timezone)
    next_week = second - timedelta(second.weekday()) + timedelta(7)
    end_next_week = next_week + timedelta(7)
    if (first.start_time.replace(tzinfo=None) > next_week.replace(tzinfo=None)) & (first.start_time.replace(tzinfo=None) < end_next_week.replace(tzinfo=None)):
        return True
    return False


def check_next_month(first,second):
    if first.year == second.year:
        if first.month == (second.month+1):
            return True
    elif first.year == (second.year + 1) & first.month == 1 & second.month == 12:
        return True
    return False


# check upcoming event. return true if upcoming event is within 2 months from now
def check_upcoming(first,second):
    if (first.year == second.year):
        if (first.month == second.month):
            if (first.day > second.day):
                return True
        elif (first.month > second.month and first.month <= second.month+2):
            if (first.day <= second.day):
                return True
            return False
        else:
            return False
    else:
        if (first.year == second.year+1):
            if (first.month + (12-second.month) <=2):
                if (first.day <= second.day):
                    return True
                return False
            return False
        return False
    return False


def check_past(first,second):
    if (first.year < second.year):
        return True
    elif (first.year == second.year):
        if (first.month < second.month):
            return True
        elif (first.month == second.month):
            if (first.day < second.day):
                return True
    return False


def get_activities_by_filter(request,filter_argument,user_login):
    current_time = get_current_time_at_client_time_zone(request)
    current_timezone = get_current_time_zone(request)
    activities = []
    if (filter_argument == "all"):
        return user_login.get_profile().watchlist.all().order_by("start_time")

    for activity in user_login.get_profile().watchlist.all():  
        
        if (check_today(activity,current_time,current_timezone)) and (filter_argument=="today"):
            activities.append(activity)
        elif (activity.end_time != None) and ((check_yesterday(activity,current_time,current_timezone))) and (filter_argument=="yesterday"):
            activities.append(activity)
        elif ((check_last_week(activity,current_time,current_timezone))) and (filter_argument=="last_week"):
            activities.append(activity)
        elif ((check_tomorrow(activity,current_time,current_timezone))) and (filter_argument=="tomorrow"):
            activities.append(activity)
        elif ((check_next_week(activity,current_time,current_timezone))) and (filter_argument=="next_week"):
            activities.append(activity)
        elif ((check_next_month(activity.start_time,current_time))) and (filter_argument=="next_month"):
            activities.append(activity)
        elif activity.category == filter_argument:
            activities.append(activity)

    return activities
