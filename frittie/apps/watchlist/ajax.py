from django.utils import timezone
from dajaxice.core import dajaxice_autodiscover
from dajaxice.decorators import dajaxice_register
from dajax.core import Dajax

from frittie.apps.app_helper import generate_html_snippet, get_user_login_object
from frittie.apps.app_settings import WATCH_LIST_PAGE
from frittie.apps.main.constants import  WATCH_LIST_ITEM, ACTIVITY_CATEGORY_MAP
from frittie.apps.watchlist.helper import get_activities_by_filter

import json, logging

logger = logging.getLogger(__name__)


@dajaxice_register
def get_watch_list_activities(request,filter_argument):
	results = {}
	try:
		user_login = get_user_login_object(request)
		activities = get_activities_by_filter(request,filter_argument,user_login)
		filter_argument_final_value = ""
		if filter_argument.isdigit() == True:
			filter_argument_final_value = ACTIVITY_CATEGORY_MAP[filter_argument]
		else:
			filter_argument_final_value = filter_argument.replace("_"," ")
		results['success'] = True
		results['snippet_return'] = generate_html_snippet(request,WATCH_LIST_PAGE[WATCH_LIST_ITEM],{"filter_argument": filter_argument_final_value,"activities":activities})   
		results['num_activities_load'] = len(activities) 
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})   
	return json.dumps({"results": results})


