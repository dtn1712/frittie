from django.conf.urls import *

urlpatterns = patterns('frittie.apps.message.views',
	url(r"^$", "main_page",name="message_main"),
	url(r'^(?P<conversation_unique_id>\w+)/$', "show_conversation",name="conversation_detail")
)
