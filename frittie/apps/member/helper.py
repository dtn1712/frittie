from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _, ugettext
from django.shortcuts import get_object_or_404

from frittie.apps.app_helper import get_user_login_object

from allauth.utils import email_address_exists

import re, json, logging

alnum_re = re.compile(r"^\w+$")

logger = logging.getLogger(__name__)

# Helper for member app. Input: dtn1712@gmail.com -> Output: dtn1712
def get_prefix_email(email):
	result = ""
	for i in range(0,len(email)):
		if email[i] == "@":
			break
		result = result + email[i]
	return result.replace('.','_')

# Set a readable username created from analyze the first name, last name
# and email. Use this when create new member since it is just required
# user to enter email for registration, not username
def set_readable_username(email):
	name = get_prefix_email(email)
	u = User.objects.filter(username=name)
	if len(u) == 0:
		return name
	else:
		i = 1
		while len(u) != 0:
			test_username = name + str(i)
			u = User.objects.filter(username=test_username)
			if len(u) == 0: 
				return test_username
			i = i + 1

# This is used for settings function to validate the username
# Value:
#		-1 ->  user contain incorrect symbol
#		 0 ->	user already exist
#		 1 ->	good
def validate_username_setting(request):
	value = request.POST["username"]
	username = get_user_login_object(request).username
	if not alnum_re.search(value):
		return -1
	if value != username:
		try:
			User.objects.get(username__iexact=value)
		except User.DoesNotExist:
			return 1
		return 0
	else:
		return 1

# This is used for settings function to validate the email
def is_email_settings_valid(request):
	user_login = get_user_login_object(request)
	value = request.POST['email']
	email = user_login.email
	if value != email:
		if value and email_address_exists(value):
			return False
		else:
			return True
	else:
		return True

# Check if the user has confirmed email yet
def check_confirm_email(request):
	user_login = get_user_login_object(request)
	try:
		if EmailAddress.objects.get(user=user_login).verified:
			return True
		else:
			return False
	except:
		return False

def get_user_common_info(user):
	try:
		data = {
			"username": user.username,
			"avatar": user.get_profile().avatar.image.url,
			"user_fullname": user.get_profile().get_user_fullname()
		}
		return data
	except Exception as e:
		logger.exception(e)
	return None

def get_user_common_info_json(user):
	data = get_user_common_info(user)
	if data == None:
		return json.dumps({})
	else:
		return json.dumps(data)


