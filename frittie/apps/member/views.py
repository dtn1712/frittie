# Views for the main app
import datetime

from django.template import Context, loader
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound, Http404
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404, get_list_or_404
from django.db.models import Q
from django.utils import simplejson
from django.core import serializers
from django.contrib.auth.decorators import login_required

# from frittie.apps.member.forms import SettingForm
from frittie.apps.app_helper import get_user_login_object, convert_time
from frittie.apps.activity.helper import get_today_activity
from frittie.apps.main.models import Location, Activity, Photo

from helper import is_email_settings_valid, validate_username_setting

from forms import SettingsForm

import logging

logger = logging.getLogger(__name__)

@login_required
def main_page(request, username):
	user_login = get_user_login_object(request)
	user_view = user_login
	template = "apps/member/page/admin/main_page.html"
	is_following = False
	if user_login.username != username:
		template = "apps/member/page/normal/main_page.html"
		user_view = get_object_or_404(User,username=username)
		is_following = user_login in user_view.get_profile().followers.all()

	happening_activities = []
	upcoming_activities = []
	past_activities = []

	join_activities = user_view.get_profile().get_join_activities()
	for activity in join_activities:
		activity_status = activity.get_activity_status()
		if activity_status == "happening":
			happening_activities.append(activity)
		elif activity_status == "upcoming":
			upcoming_activities.append(activity)
		else:
			past_activities.append(activity)

	user_view_upload_photos = user_view.get_profile().get_upload_photos(0,6)

	return render_to_response(template,{
			"user_view": user_view,
			"user_view_upload_photos": user_view_upload_photos,
			"join_activities_count": len(join_activities),
			"happening_activities": happening_activities,
			"upcoming_activities": upcoming_activities,
			"past_activities": past_activities,
			"is_following": is_following,
		},context_instance=RequestContext(request))

@login_required
def settings_page(request):
	user_login = get_user_login_object(request)
	if request.method == 'POST': 
		form = SettingsForm(request.POST,request.FILES) 
		if form.is_valid(): 
			# Check if the new username and email already
			# exist and raise the appropriate error message
			flag = False
			check_username = validate_username_setting(request)
			if check_username == -1:
				form._errors["username"] = ErrorList([u"Usernames can only contain letters, numbers and underscores."])
			elif check_username == 0:
				form._errors["username"] = ErrorList([u"This username is already taken. Please choose another."])
			else:
				flag = True
			if is_email_settings_valid(request) == False:
				form._errors["email"] = ErrorList([u"A user is registered with this e-mail address."])
				flag = False
				
			if flag:
				request.session['is_show_request_message'] = True
				try:
					# Save the new data to the object user and member (model Member)
   					user_login.username = request.POST['username']
					user_login.first_name = request.POST['first_name']
					user_login.last_name = request.POST['last_name']
					user_login.email = request.POST['email']
					user_login.get_profile().basic_info = request.POST['basic_info']
					user_login.get_profile().gender = request.POST['gender']
					day = int(request.POST['birthday_day'])
					month = int(request.POST['birthday_month'])
					year = int(request.POST['birthday_year'])

					user_login.get_profile().date_of_birth  = datetime.date(year,month,day)
					if "avatar" in request.FILES:
						photo = user_login.get_profile().avatar
						photo.image = request.FILES['avatar']
						photo.save()
					user_login.get_profile().city = request.POST['city']
					user_login.get_profile().state = request.POST['state']	
					user_login.get_profile().country = request.POST['country']
					user_login.get_profile().privacy_status = request.POST['privacy_status']				
					user_login.get_profile().save()
					user_login.save()
				except Exception as e:
					logger.exception(e)
					return HttpResponseRedirect("/settings/?action=edit_profile&result=fail")
				# redirect to user main page if the edit process is successful
				return HttpResponseRedirect("/people/" + user_login.username + "/?action=edit_profile&result=success")
	else:
		# The initial data of the form taken from the user database 
		default_data = {
		       	"username": user_login.username, 
		       	"first_name": user_login.first_name,
				"last_name": user_login.last_name,
		       	"email": user_login.email,
		       	"basic_info": user_login.get_profile().basic_info,
		       	"avatar": user_login.get_profile().avatar.image.url,
		       	"city": user_login.get_profile().city,
		    	"state": user_login.get_profile().state,
		    	"country": user_login.get_profile().country,
		    	"privacy_status": user_login.get_profile().privacy_status,
		   	}
		if user_login.get_profile().date_of_birth != None:
			default_data["birthday_day"] = user_login.get_profile().date_of_birth.day
			default_data["birthday_month"] = user_login.get_profile().date_of_birth.month
			default_data["birthday_year"] = user_login.get_profile().date_of_birth.year

		form = SettingsForm(default_data) 

	return render_to_response("apps/member/page/admin/settings.html",{
			"form":form,
		},context_instance=RequestContext(request)
	)


@login_required
def update_picture(request):
	if request.method == "POST":
		user_login = get_user_login_object(request)
		picture_type = request.POST['picture_type']
		if picture_type.lower() == "avatar":
			photo = user_login.get_profile().avatar
			photo.image = request.FILES['picture']
			photo.save()
		elif picture_type.lower() == "cover_picture":
			photo = user_login.get_profile().cover_picture
			photo.image = request.FILES['picture']
			photo.save()
		return HttpResponseRedirect("/people/" + user_login.username)
	return HttpResponseRedirect("/")


