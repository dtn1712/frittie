from django import forms
from django.contrib.auth.models import User
from django.utils import html

from frittie.apps.main.constants import USA_STATES, GENDER, PRIVACY_STATUS, DEFAULT_COUNTRIES_FORMAT
from frittie.apps.app_helper import setup_constant_day, setup_constant_month, setup_constant_year

DAY = setup_constant_day()
MONTH = setup_constant_month()
YEAR = setup_constant_year()

class ChoiceField(forms.ChoiceField):
    def __init__(self, *args, **kwargs):
        self.blank_choice = kwargs.pop('blank_choice', None)
        super(ChoiceField, self).__init__(*args, **kwargs)

    def _get_choices(self):
        return self._choices

    def _set_choices(self, value):
        choices = list(value)
        if self.blank_choice:
            choices = [('', self.blank_choice)] + choices
        self._choices = self.widget.choices = choices

    choices = property(_get_choices, _set_choices)



class SettingsForm(forms.Form):
	username = forms.CharField(label='Username',max_length=50,widget=forms.TextInput(attrs={'class':'form-control'}))
	first_name = forms.CharField(label='FirstName',max_length=50,widget=forms.TextInput(attrs={'class':'form-control'}))
	last_name = forms.CharField(label='LastName',max_length=50,widget=forms.TextInput(attrs={'class':'form-control'}))
	email = forms.EmailField(label='Email',max_length=100,widget=forms.TextInput(attrs={'class':'form-control'}))
	gender = forms.CharField(label='Gender',required=False,max_length=10,widget=forms.Select(choices=GENDER))
	basic_info = forms.CharField(label='Basic_Info',required=False,max_length=200,widget=forms.Textarea(attrs={'cols': 50, 'rows': 3, "class": "form-control"}))
	birthday_day = forms.CharField(label='Birthday_Day',required=False,max_length=2,widget=forms.Select(choices=DAY,attrs={'class':'form-control'}))
	birthday_month = forms.CharField(label='Birthday_Month',required=False,max_length=2,widget=forms.Select(choices=MONTH,attrs={'class':'form-control'}))
	birthday_year = forms.CharField(label='Birthday_Year',required=False,max_length=4,widget=forms.Select(choices=YEAR,attrs={'class':'form-control'}))
	avatar = forms.ImageField(label='Avatar',required=False)
	city = forms.CharField(label='City',required=False,max_length=100,widget=forms.TextInput(attrs={'class':'form-control'}))
	state = forms.CharField(label='State',required=False,max_length=2,widget=forms.Select(choices=USA_STATES,attrs={'class':'form-control'}))
	country = forms.CharField(label="Country",required=False,max_length=100,widget=forms.Select(choices=DEFAULT_COUNTRIES_FORMAT,attrs={'class':'form-control'}))
	privacy_status = forms.CharField(label="Privacy Status",required=False,max_length=1,widget=forms.Select(choices=PRIVACY_STATUS,attrs={'class':'form-control'}))