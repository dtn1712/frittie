from dajax.core import Dajax
from dajaxice.decorators import dajaxice_register

from django.core import serializers
from django.contrib.auth.models import User
from django.db import connection

from frittie.apps.main.models import Location, Activity, Report, TicketTransaction
from frittie.apps.app_helper import generate_html_snippet, generate_message
from frittie.apps.app_helper import generate_unique_id, get_user_login_object

from frittie.apps.app_settings import MEMBER_PAGE, NUM_PHOTO_PER_LOAD
from frittie.apps.main.constants import LIST_ACTIVITY, LIST_LOCATION
from frittie.apps.main.constants import LIST_USER, LIST_MEDIA, LIST_MEDIA_THUMBNAIL, LIST_TICKET

import logging, json, time

logger = logging.getLogger(__name__)

@dajaxice_register
def load_activity(request,load_type,user_view_username):
	results = {}
	try:
		user_view = User.objects.get(username=user_view_username)
		activities = []
		if load_type == "host":
			activities = user_view.get_profile().get_create_activities()
		elif load_type == "attending":
			activities = user_view.get_profile().get_join_activities()
		elif load_type == "watchlist":
			activities = user_view.get_profile().watchlist.all()
		happening_activities = []
		upcoming_activities = []
		past_activities = []
		for activity in activities:
			activity_status = activity.get_activity_status()
			if activity_status == "happening":
				happening_activities.append(activity)
			elif activity_status == "upcoming":
				upcoming_activities.append(activity)
			else:
				past_activities.append(activity)
		results['success'] = True
		results['load_type'] = load_type
		results['snippet_return'] =  generate_html_snippet(request,MEMBER_PAGE[LIST_ACTIVITY],{
				"happening_activities": happening_activities,
				"upcoming_activities": upcoming_activities,
				"past_activities": past_activities,
				"activities_count": len(activities),
				"load_type": load_type
			})
	except Exception as e:
		raise
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})
	return json.dumps({"results": results})


@dajaxice_register
def load_location(request,user_view_username,location_type):
	results = {}
	try:
		user_view = User.objects.get(username=user_view_username)
		locations = user_view.get_profile().get_follow_locations()
		if location_type == "create":
			locations = user_view.get_profile().get_create_locations()
			results['load_type'] = "my_places"
		else:
			results['load_type'] = "interest_places"
		results['success'] = True
		results['snippet_return'] =  generate_html_snippet(request,MEMBER_PAGE[LIST_LOCATION],{
				"locations": locations,
				"locations_count": len(locations),
			})
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})
	return json.dumps({"results": results})


@dajaxice_register
def load_related_user(request,user_type,user_view_username):
	results = {}
	try:
		user_view = User.objects.get(username=user_view_username)
		users = []
		if user_type == "followers":
			users = user_view.get_profile().followers.all()
		elif user_type == "following":
			users = user_view.get_profile().following.all()
		results['success'] = True
		results['load_type'] = user_type
		results['snippet_return'] =  generate_html_snippet(request,MEMBER_PAGE[LIST_USER],{
				"users": users,
				"user_type": user_type,
				"users_count": len(users),
			})
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})
	return json.dumps({"results": results})


@dajaxice_register
def load_media(request,user_view_username):
	results = {}
	try:
		user_view = User.objects.get(username=user_view_username)
		photos = user_view.get_profile().get_upload_photos(0,NUM_PHOTO_PER_LOAD)
		results['success'] = True
		results['load_type'] = "media"
		results['snippet_return'] =  generate_html_snippet(request,MEMBER_PAGE[LIST_MEDIA],{
				"photos": photos,
				"photos_count": len(photos)
			})
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})
	return json.dumps({"results": results})

@dajaxice_register
def load_tickets(request):
	results = {}
	try:
		user_login = get_user_login_object(request)
		ticket_transactions = TicketTransaction.objects.filter(user=user_login).select_related().prefetch_related()
		activities_table = {}
		activities_list = []
		for ticket_transaction in ticket_transactions:
			if ticket_transaction.activity.unique_id not in activities_table:
				activities_table[ticket_transaction.activity.unique_id] = None
				activities_list.append(ticket_transaction.activity)
		results['success'] = True
		results['load_type'] = "my_tickets"
		results['snippet_return'] =  generate_html_snippet(request,MEMBER_PAGE[LIST_TICKET],{
				"ticket_transactions": ticket_transactions,
				"activities": activities_list
			})
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})
	return json.dumps({"results": results})


@dajaxice_register
def load_more_media(request,user_view_username,num_media_load):
	results = {}
	try:
		user_view = User.objects.get(username=user_view_username)
		photos = user_view.get_profile().get_upload_photos(num_media_load,num_media_load + NUM_PHOTO_PER_LOAD)
		results['snippet_return'] = generate_html_snippet(request,MEMBER_PAGE[LIST_MEDIA],{"photos":photos})
		results['num_media_load'] = num_media_load + NUM_PHOTO_PER_LOAD
		results['success'] = True
	except Exception as e:
		results['success'] = False
	return json.dumps({"results": results})


@dajaxice_register
def follow_user(request,user_view_username):
	results = {}
	try:
		user_login = get_user_login_object(request)
		user_view = User.objects.get(username=user_view_username)
		user_login.get_profile().following.add(user_view)
		user_view.get_profile().followers.add(user_login)
		results['success'] = True
		results['user_view_username'] = user_view_username
		results['user_login_num_following'] = user_login.get_profile().following.count()
		results['user_view_num_followers'] = user_view.get_profile().followers.count()
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})
	return json.dumps({"results": results})


@dajaxice_register
def unfollow_user(request,user_view_username):
	results = {}
	try:
		user_login = get_user_login_object(request)
		user_view = User.objects.get(username=user_view_username)
		user_login.get_profile().following.remove(user_view)
		user_view.get_profile().followers.remove(user_login)
		results['success'] = True
		results['user_view_username'] = user_view_username
		results['user_login_num_following'] = user_login.get_profile().following.count()
		results['user_view_num_followers'] = user_view.get_profile().followers.count()
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})
	return json.dumps({"results": results})


@dajaxice_register
def report_user(request,username,report_content):
	results = {}
	try:
		user_login = get_user_login_object(request)
		user_reported = User.objects.get(username=username)
		report = Report.objects.create(report_type='user',user_report=user_login,
									   report_content=report_content,unique_id=generate_unique_id('report'))
		report.save()
		user_reported.get_profile().reports.add(report)
		user_reported.get_profile().save()
		user_reported.save()
		results['success'] = True
		results['success_message'] = generate_message("report","success",{})
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})
	return json.dumps({"results": results})





