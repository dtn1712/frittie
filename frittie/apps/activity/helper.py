from django.contrib.auth.models import User
from django.utils import simplejson
from django.core import serializers
from django.db.models import Q

from django.contrib.gis.geoip import GeoIP

from frittie.apps.main.constants import PUBLIC, REQUEST_ACCEPT, REQUEST_INVITED
from frittie.apps.main.models import Activity, Location, ActivityRequest
from frittie.apps.app_helper import  get_current_time_at_client_time_zone, get_user_login_object
from frittie.apps.app_helper import check_position_within_distance, get_client_ip, get_display_time
from frittie.apps.location.helper import get_location_common_info

from frittie.apps.app_settings import NUM_ITEM_PER_PAGE

from random import randint

import pytz, datetime, json
import logging, uuid, math

logger = logging.getLogger(__name__)

def check_user_activity_creator(user,activity):
	if user == None: return False
	if activity.user_create.username == user.username:
		return True
	return False

# Check if current login user have joined this activity or not
def check_user_activity_join(request,activity_unique_id):
	current_user = get_user_login_object(request)
	user_join = Activity.objects.get(unique_id=activity_unique_id).user_join.all()
	for user in user_join:
		if user.username == current_user.username:
			return True
	return False

def check_activity_seeable(user,activity,activity_request):
	if activity.activity_type == PUBLIC: return True
	if user == activity.user_create: return True
	if activity_request == None:
		return False
	else:
		if user == activity_request.user_request and (activity_request.request_status == REQUEST_ACCEPT or activity_request.request_status == REQUEST_INVITED):
			return True
	return False

def get_activity_unjoinable(request,activity_unique_id):
	activity = Activity.objects.get(unique_id=activity_unique_id)
	user_login = get_user_login_object(request)
	if len(activity.user_join.all()) == activity.limit:
		return "There is no more available spot for this activity "
	if activity.activity_type == 'blind_date':
		if user_login.gender == activity.user_create.gender:
			return "You are not qualified to join this activity. You and the host need to have different sex in the blind date activity"
	return None


# Check if this activity is today activity
def is_today_activity(activity_unique_id):
	now = datetime.datetime.now()
	activity = Activity.objects.filter(unique_id=activity_unique_id,start_time__year=now.year,start_time__month=now.month,start_time__day=now.day)
	if len(activity) != 0:
		return True
	return False

# Check if this activity is upcomming
def is_upcoming_activity(activity_unique_id):
	now = datetime.datetime.now()
	if len(Activity.objects.filter(unique_id=activity_unique_id,start_time__gt=now)) != 0:
		return True
	return False

# Check if this activity is already past
def is_past_activity(activity_unique_id):
	now = datetime.datetime.now()
	if len(Activity.objects.filter(unique_id=activity_unique_id,end_time__lt=now)) != 0:
		return True 
	return False

# Check if this activity is happening
def is_happening_activity(activity_unique_id):
	now = datetime.datetime.now()
	if len(Activity.objects.filter(unique_id=activity_unique_id,start_time__lte=now,end_time__gte=now)) != 0:
		return True
	return False

# Get all activity happen today
def get_today_activity():
	now = datetime.datetime.now()
	return Activity.objects.filter(start_time__year=now.year,start_time__month=now.month,start_time__day=now.day).order_by('start_time')

# Get all upcoming activity
def get_upcoming_activity(location_id):
	location = Location.objects.get(unique_id=location_id)
	now = datetime.datetime.now()
	return Activity.objects.filter(location=location,start_time__gt=now).order_by('start_time')

# Get all past activity
def get_past_activity(location_id):
	location = Location.objects.get(unique_id=location_id)
	now = datetime.datetime.now()
	return Activity.objects.filter(location=location,end_time__lt=now).order_by('end_time') 

# Get all happening activity
def get_happening_activity(location_id):
	location = Location.objects.get(unique_id=location_id)
	now = datetime.datetime.now()
	return Activity.objects.filter(location=location,start_time__lte=now,end_time__gte=now).order_by('start_time')

# Get the relationship between the current login user and the activity
# Return none if there is no initial relationship between them
def get_user_activity_request(request,activity_unique_id):
	activity = Activity.objects.get(unique_id=activity_unique_id)
	user_login = get_user_login_object(request)
	activity_user_request = ActivityRequest.objects.filter(activity=activity,user_join=user_login,user_create=activity.user_create)
	if len(activity_user_request) != 0:
		return activity_user_request[0]
	return None	

			

def get_nearby_activity(request,lat=None,lng=None,radius=None,activities=None):
	g = GeoIP()
	now = datetime.datetime.now()
	today = now + datetime.timedelta(days=-1)
	try:
		now = get_current_time_at_client_time_zone(request) 
	except Exception as e:
		logger.exception(e)

	if activities == None:
		activities = Activity.objects.filter(start_time__gte=today,activity_type=PUBLIC,is_publish=True).select_related().prefetch_related().order_by("start_time")
	try:
		if lat == None and lng == None:
			client_ip = get_client_ip(request)	
			lat, lng = g.lat_lon(client_ip)
		results = []
		if radius == None:
			radius = 10000
		for activity in activities:
			if activity.is_location_model():
				location = activity.get_location_object()
				
				#check if a location is within 1 km from client location
				if check_position_within_distance(math.radians(float(lat)), math.radians(float(lng)),math.radians(float(location.lat)), math.radians(float(location.lng)), radius): 
					results.append(activity)
		return results
	except Exception as e:
		logger.exception(e)
	return activities



def get_activity_common_info(activity):
	try:
		results = {
				"name": activity.name,
				"description": activity.description,
				"start_time": get_display_time(activity.start_time),
				"end_time": get_display_time(activity.end_time),
				"logo": activity.logo.image.url,
				"host_username": activity.user_create.username,
				"host_firstname": activity.user_create.first_name,
				"host_lastname": activity.user_create.last_name,
				"host_fullname": activity.user_create.get_profile().get_user_fullname(),
				"activity_status": activity.get_activity_status(),
				"activity_type": activity.activity_type,
				"limit": activity.limit,
				"location": get_location_common_info(activity.location),
		}
		return results
	except Exception as e:
		logger.exception(e)
	return None

def get_activity_common_info_json(activity):
	data = get_activity_common_info(activity)
	if data == None:
		return json.dumps({})
	else:
		return json.dumps(data)


def generate_confirmation_id(user):
	remain = 7 - len(str(user.pk))
	return str(user.pk) + str(randint(pow(10,remain-1),pow(10,remain)-1)) 
	

def convert_unicode_string(s):
   if type(s) == type(u""):
      return s
   return unicode(s, "utf8").upper().encode("utf8")
