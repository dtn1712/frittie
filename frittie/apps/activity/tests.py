data = [
	["1","A"],
	["2","A"],
	["3","B"],
	["4","B"],
	["5","B"],
	["6","C"],
	["7","D"],
	["8","D"],
	["9","E"],
	["10","E"],
]

def groupActivityOverlays(data):
	location_table = {}
	for item in data:
		try:
			location_unique_id = item[1]
			activity_unique_id = item[0]
			if location_unique_id not in location_table:
			 	location_table[location_unique_id] = [activity_unique_id]
			else:
				activities = location_table[location_unique_id]
				activities.append(activity_unique_id)
				location_table[location_unique_id] = activities
		except:
			pass
	return location_table


print groupActivityOverlays(data)