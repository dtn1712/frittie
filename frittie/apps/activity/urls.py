from django.conf.urls import *

urlpatterns = patterns('frittie.apps.activity.views',
   url(r'^create/$','create_activity',name="create_activity"),
   url(r"^delete/$","delete_activity",name="delete_activity"),
   url(r"^(?P<activity_unique_id>\w+)/publish/$","publish_activity"),
   url(r"^(?P<activity_unique_id>\w+)/updatelogo/$","update_activity_logo"),
   url(r"^(?P<activity_unique_id>\w+)/edit/$","edit_activity",name="edit_activity"),
   url(r"^(?P<activity_unique_id>\w+)/updatelogo/$","update_activity_logo"),
   url(r"^(?P<activity_unique_id>\w+)/addcalendar/$","add_to_calendar",name="add_to_calendar"),
   url(r"^(?P<activity_unique_id>\w+)/$","main_page",name="activity_main"),

)
