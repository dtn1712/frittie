from django.contrib.auth.models import User
from dajaxice.utils import deserialize_form
from dajaxice.core import dajaxice_autodiscover
from dajaxice.decorators import dajaxice_register
from dajax.core import Dajax
from django.core.cache import cache

from frittie.apps.app_helper import get_this_week_range, get_next_week_range, get_this_month_range
from frittie.apps.app_helper import generate_unique_id, get_user_login_object
from frittie.apps.app_helper import generate_message, generate_html_snippet, generate_token

from frittie.apps.member.helper import get_user_common_info
from frittie.apps.notification.tasks import send_notification
from frittie.apps.feed.tasks import create_feed
from frittie.apps.mailer.tasks import send_email, send_mass_email

from frittie.apps.main.models import UserProfile, Activity, Photo, Comment , Ticket, Report, Invitation, GeneralToken
from frittie.apps.main.models import ConfirmationActivity, ActivityCancelAttending, ActivityRequest, Location, EmailTracking


from frittie.apps.main.constants import  COMMENT_ITEM, DEFAULT_END_DATE, DEFAULT_FILTER_WHEN, WATCHING, NOT_WATCH
from frittie.apps.main.constants import DEFAULT_FILTER_HOW, DEFAULT_FILTER_CATEGORY, DEFAULT_LIST_ACTIVITY_HTML_TEMPLATE
from frittie.apps.main.constants import  REQUEST_WAITING, REQUEST_ACCEPT, REQUEST_DECLINE, PREVIEW_CALCULATE_TICKET_ITEM
from frittie.apps.main.constants import YES_VALUE, NO_VALUE, REQUEST_INVITED

from frittie.apps.app_settings import ACTIVITY_PAGE
from frittie.settings import WEBSITE_HOMEPAGE

from helper import generate_confirmation_id, get_activity_common_info
from helper import get_nearby_activity as get_nearby_activity_helper

from validate_email import validate_email

import json, random, logging, datetime


logger = logging.getLogger(__name__)

now = datetime.datetime.now()
today = datetime.datetime(now.year,now.month,now.day,0,0)
this_week = get_this_week_range()
next_week = get_next_week_range()
this_month = get_this_month_range()

# THE CODE IS AT CONSTANT FILTER_WHEN_MAP
range_filter_start_map = {
	0: today,										# default
	1: today,										# today
	2: today + datetime.timedelta(days=1),			# tomorrow
	3: get_this_week_range()['start'],				# this week
	4: get_next_week_range()['start'],				# next week
	5: get_this_month_range()['start'],				# this month
}

range_filter_end_map = {
	0: DEFAULT_END_DATE,
	1: today + datetime.timedelta(days=1),
	2: today + datetime.timedelta(days=2),
	3: get_this_week_range()['end'],
	4: get_next_week_range()['end'],
	5: get_this_month_range()['end'],
}


@dajaxice_register
def get_preview_activity(request,unique_id,prev_unique_id,next_unique_id,current_pos,total_count):
	results = cache.get(unique_id) if cache != None else None
	if results == None:
		print "Adding to cache unique id %s" %(unique_id)	
		results = {}
		try:
			user_login = get_user_login_object(request)
			previous_activity, next_activity = None, None
			if prev_unique_id != None: 
				previous_activity = Activity.objects.get(unique_id=prev_unique_id)
			if next_unique_id != None:
				next_activity = Activity.objects.get(unique_id=next_unique_id)
			activity = Activity.objects.get(unique_id=unique_id)
			is_watching = False
			is_joining = False
			is_waiting = False
			is_invited = False
			if user_login != None:
				is_watching = activity.is_watching(user_login)
				is_joining = activity.is_joining(user_login)
				is_waiting = activity.is_waiting(user_login)
				is_invited = activity.is_invited(user_login)
			results['success'] = True
			results['snippet_return'] = generate_html_snippet(request,"preview_activity_modal",{
				"activity": activity, 
				"is_watching": is_watching,
				"is_joining": is_joining,
				"is_waiting": is_waiting,
				"is_invited": is_invited,
				"current_activity_position": current_pos,
				"total_display_activity_count": total_count,
				"previous_activity": previous_activity,
				"next_activity": next_activity,
			})
			results = json.dumps({"results": results})
			cache.set(unique_id, results, timeout=60*20)
		except Exception as e:
			logger.exception(e)
			results['success'] = False
			results['error_message'] = generate_message("ajax_request","fail",{})
	else:
		print "Found in cache %s" %(unique_id)
	return results 

@dajaxice_register
def get_nearby_activity(request,lat,lng,radius):
	results = {}
	try: 
		activities = get_nearby_activity_helper(request,lat,lng,radius)
		nearby_activity_data = []
		for i in range(0,len(activities)):
			if activities[i].is_location_model():
				location = activities[i].get_location_object()
				data = {
					"location_unique_id": location.unique_id,
					"activity_unique_id": activities[i].unique_id,
					"activity_number": i
				}
				nearby_activity_data.append(data)
		results['nearby_activity_data'] = nearby_activity_data
		results['snippet_return'] = generate_html_snippet(request,"list_activity_map",{"activities":activities})
		results['success'] = True
	except Exception as e:
		logger.exception(e)
		results['success'] = False
	return json.dumps({"results": results})

@dajaxice_register
def get_activity_overlays(request,data):
	results = {}
	try:
		group_overlays = []
		single_overlays = []
		location_table = {}
		for item in data:
			try:
				location_unique_id = item['location_unique_id']
				location = Location.objects.get(unique_id=location_unique_id)
				activity_unique_id = item['activity_unique_id']
				activity_number = item['activity_number']
				overlay_data = {
					"lat": str(location.lat),
					"lng": str(location.lng),
					"activity_unique_id": activity_unique_id,
					"activity_number": activity_number,
				}
				if location_unique_id not in location_table:
					location_table[location_unique_id] = [overlay_data]
				else:
					overlays = location_table[location_unique_id]
					overlays.append(overlay_data)
					location_table[location_unique_id] = overlays
			except:
				pass
		for item in location_table:
			if len(location_table[item]) == 1:
				single_overlays.append(location_table[item][0])
			else:
				group_overlays.append(location_table[item])
		
		results['success'] = True
		results['single_overlays'] = single_overlays
		results['group_overlays'] = group_overlays
	except Exception as e:
		logger.exception(e)
		results['success'] = False
	return json.dumps({"results": results})



# See constants FILTER_HOW_MAP and FILTER_WHEN_MAP for reference about the dict key
@dajaxice_register
def filter_activity(request,filter_when_value,filter_how_value,filter_category_value,html_template):
	results = {}
	try:
		if filter_when_value == None: filter_when_value = DEFAULT_FILTER_WHEN
		if filter_how_value == None: filter_how_value = DEFAULT_FILTER_HOW
		if filter_category_value == None: filter_category_value = DEFAULT_FILTER_CATEGORY
		if html_template == None: html_template = DEFAULT_LIST_ACTIVITY_HTML_TEMPLATE
	
		user_login = get_user_login_object(request)
		activities = None 
		range_start = range_filter_start_map[int(filter_when_value)]
		range_end = range_filter_end_map[int(filter_when_value)]
		
		if filter_how_value == 2:
			if filter_category_value == "-1":
				activities = get_nearby_activity_helper(request,None,None,None,Activity.objects.filter(start_time__range=(range_start,range_end),activity_type="1").annotate(hot=Count("activity_requests")).order_by("-hot").select_related())
			else:
				activities = get_nearby_activity_helper(request,None,None,None,Activity.objects.filter(start_time__range=(range_start,range_end),category=filter_category_value,activity_type="1").annotate(hot=Count("activity_requests")).order_by("-hot").select_related())             
		else:
			if filter_category_value == "-1":
				activities = get_nearby_activity_helper(request,None,None,None,Activity.objects.filter(start_time__range=(range_start,range_end),activity_type="1").order_by("-start_time").select_related())
			else:
				activities = get_nearby_activity_helper(request,None,None,None,Activity.objects.filter(start_time__range=(range_start,range_end),category=filter_category_value,activity_type="1").order_by("-start_time").select_related())        
		results['success'] = True
		results['snippet_return'] = generate_html_snippet(request,html_template,{"activities": activities})
		results['num_activities'] = len(activities)
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})   
	return json.dumps({"results": results})


@dajaxice_register
def accept_joining_activity_request(request,unique_id,username):
	results = {}
	try:
		activity = Activity.objects.get(unique_id=unique_id)
		user_login = get_user_login_object(request)
		user = User.objects.get(username=username)
		activity_request = ActivityRequest.objects.get(activity=activity,user_request=user)
		confirmation = ConfirmationActivity.objects.create(user_confirm=user,confirmation_id=generate_confirmation_id(user),is_confirmed=NO_VALUE,activity=activity)
		activity.confirmations.add(confirmation)
		activity_request.request_status = REQUEST_ACCEPT
		activity_request.save()
		activity.save()

		# Generate email which sending the confimation id
		context = {
			"user": get_user_common_info(user),
			"activity" : get_activity_common_info(activity),
			"url": WEBSITE_HOMEPAGE + "activity/" + unique_id,
			"confirmation_id": confirmation.confirmation_id
		}
		email_tracking = EmailTracking.objects.create(to_emails=user.email,email_template='host_approve_send_confirmation',context_data=str(context))
		send_email.delay(email_tracking.id)

		results['success'] = True
		#results['email_tracking_ids'] = email_tracking.id
		results['action'] = "accept_request"
		results['user_info'] = get_user_common_info(user)
		results['snippet_return'] = generate_html_snippet(request,"user_going_item",{"user":user})

		results['going_type'] = "others"
		if user in user_login.get_profile().followers.all():
			results['going_type'] = "followers"
		elif user in user_login.get_profile().following.all():
			results['going_type'] = "following"

		# Generate Notification
		subj_notification = { "main": user_login,"display": "host","related": None}
		action_notification = "accept_request"
		obj = { "main": activity, "object_type": "activity", "display": "activity", "related": [user] }
		send_notification.delay(subj_notification,action_notification,obj,True)
		#generate_notification(subj_notification,action_notification,obj,True)

		# Generate Feed
		# subj_feed = {"main": user, "display": "user", "related": user.get_profile().followers.all()}
		# action_feed = "join"
		# create_feed.delay(subj_feed,action_feed,obj)
		#generate_feed(subj_feed,action_feed,obj)
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{}) 
	return json.dumps({"results": results})

@dajaxice_register
def decline_joining_activity_request(request,unique_id,username):
	results = {}
	try:
		activity =  Activity.objects.get(unique_id=unique_id)
		user = User.objects.get(username=username)
		activity_request = ActivityRequest.objects.get(activity=activity,user_request=user)
		activity_request.request_status = REQUEST_DECLINE
		activity_request.save()
		activity.save()  
		results['success'] = True
		results['action'] = "decline_request" 
		results['user_info'] = get_user_common_info(user)  
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{}) 
	return json.dumps({"results": results})

@dajaxice_register
def accept_invite_activity(request,unique_id):
	results = {}
	try:
		activity = Activity.objects.get(unique_id=unique_id)
		user_login = get_user_login_object(request)
		activity_request = ActivityRequest.objects.get(activity=activity,user_request=user_login)
		confirmation = ConfirmationActivity.objects.create(user_confirm=user_login,confirmation_id=generate_confirmation_id(user_login),is_confirmed=NO_VALUE,activity=activity)
		activity.confirmations.add(confirmation)
		activity_request.request_status = REQUEST_ACCEPT
		activity_request.save()
		activity.save()

		# Generate email which sending the confirmation id
		context1 = {
			"user": get_user_common_info(user_login),
			"activity" : get_activity_common_info(activity),
			"url": WEBSITE_HOMEPAGE + "activity/" + unique_id,
			"confirmation_id": confirmation.confirmation_id
		}
		email_tracking1 = EmailTracking.objects.create(to_emails=user_login.email,email_template="accept_invite_send_confirmation",context_data=str(context1))

		# Generate email which notify the host
		context2 = {
      		"user_send": get_user_common_info(user_login),
 			"activity": get_activity_common_info(activity),
 			"url": WEBSITE_HOMEPAGE + "activity/" + unique_id,
		}
		email_tracking2 = EmailTracking.objects.create(to_emails=activity.user_create.email,email_template="accept_invite_notify_host",context_data=str(context2))

		send_mass_email.delay([email_tracking1.id,email_tracking2.id])

		results['success'] = True
		#results['list_email_tracking_ids'] = [email_tracking1.id,email_tracking2.id]
		results['success_message'] = generate_message("accept_invite_activity","success",{"activity":activity})
		results['snippet_return'] = generate_html_snippet(request,"attending_activity_button",{})

		# results['action'] = "accept_invite"
		# results['user_info'] = get_user_common_info(user_login)

		# Define this user relationship with the activity host
		results['going_type'] = "others"
		if user_login in activity.user_create.get_profile().followers.all():
			results['going_type'] = "followers"
		elif user_login in activity.user_create.get_profile().following.all():
			results['going_type'] = "following"

		# Generate notification and feeds
		subj = { "main": user_login,"display": "user","related": user_login.get_profile().followers.all()}
		action_notification = "accept_invite"
		action_feed = "join"
		obj = { "main": activity, "object_type": "activity", "display": "activity", "related": [activity.user_create] }
		send_notification.delay(subj,action_notification,obj,True)
		#create_feed.delay(subj,action_feed,obj)
		# generate_notification(subj,action_notification,obj,True)
		# generate_feed(subj,action_feed,obj)
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{}) 
	return json.dumps({"results": results})


@dajaxice_register
def decline_invite_activity(request,unique_id):
	results = {}
	try:
		activity =  Activity.objects.get(unique_id=unique_id)
		user_login = get_user_login_object(request)
		activity_request = ActivityRequest.objects.get(activity=activity,user_request=user_login)
		activity_request.request_status = REQUEST_DECLINE
		activity_request.save()
		activity.save()  

		context = {
      		"user_send": get_user_common_info(user_login),
 			"activity": get_activity_common_info(activity),
 			"url": WEBSITE_HOMEPAGE + "activity/" + activity.unique_id,
		}
		email_tracking = EmailTracking.objects.create(to_emails=activity.user_create.email,email_template="decline_invite_notify_host",context_data=str(context))
		send_email.delay(email_tracking.id)

		results['success'] = True
		#results['list_email_tracking_ids'] = [email_tracking.id]
		results['success_message'] = generate_message("decline_invite_activity","success",{"activity":activity})  
		results['snippet_return'] = generate_html_snippet(request,"request_join_activity_button",{"activity":activity})

		# results['action'] = "decline_invite" 
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{}) 
	return json.dumps({"results": results})


@dajaxice_register
def request_join_activity(request,unique_id,introduction):
	results = {}
	try:
		activity =  Activity.objects.get(unique_id=unique_id)
		is_allow_join = True
		if activity.limit != -1 and activity.get_total_people_going() >= activity.limit:
			is_allow_join = False
			results['success'] = False
			results['error_message'] = generate_message("activity_limit","fail",{'activity':activity})

		if is_allow_join:   
			user_login = get_user_login_object(request)
			activity_requests = ActivityRequest.objects.filter(activity=activity,user_request=user_login)
			
			# if the request not in the database create the new one
			if len(activity_requests) == 0:
				activity_request = ActivityRequest.objects.create(activity=activity,user_request=user_login,introduction=introduction,request_status=REQUEST_WAITING)
				activity.activity_requests.add(activity_request)
				results['success'] = True
			else:

				# If the request is already in the database and is waiting
				# or accepted status, it should be error. If it is decline
				# change it to waiting to allow the request be sent again
				activity_request = activity_requests[0]
				if activity_request.request_status == REQUEST_DECLINE:
					activity_request.request_status = REQUEST_WAITING
					activity_request.is_invite_request = False
					activity_request.introduction = introduction
					activity_request.save()
					if activity_request not in activity.activity_requests.all():
						activity.activity_requests.add(activity_request)
					results['success'] = True
				elif activity_request.request_status == REQUEST_WAITING:
					results['success'] = False
					results['error_message'] = generate_message("request_join_activity","already",{})
				elif activity_request.request_status == REQUEST_ACCEPT:
					results['success'] = False
					results['error_message'] = generate_message("request_accepted","already",{})

			# Successful request should come with message and 
			# send notification to the activity host
			if results['success']:
				activity.save()

				context = {
		      		"user_send": get_user_common_info(user_login),
		 			"activity": get_activity_common_info(activity),
		 			"url": WEBSITE_HOMEPAGE + "activity/" + activity.unique_id,
		 			"introduction_message": introduction,
				}
				email_tracking = EmailTracking.objects.create(to_emails=activity.user_create.email,email_template="request_join_activity",context_data=str(context))
				send_email.delay(email_tracking.id)

				#results['email_tracking_id'] = email_tracking.id
				results['success_message'] = generate_message("request_join_activity","success",{"activity":activity})
				results['action'] = "request_join"

				# Generate notification for the activity host
				subj = { "main": user_login,"display": "user","related": None}
				action = "request_join"
				obj = { "main": activity, "object_type": "activity", "display": "activity", "related": [activity.user_create] }
				send_notification.delay(subj,action,obj,True)
				#generate_notification(subj,action,obj,True)
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})   
	return json.dumps({"results": results})



@dajaxice_register
def cancel_attending_activity(request,unique_id,reason):
	results = {}
	try:
		user_login = get_user_login_object(request)
		activity = Activity.objects.get(unique_id=unique_id)
		activity_requests = ActivityRequest.objects.filter(activity=activity,user_request=user_login,request_status=REQUEST_ACCEPT)
		if len(activity_requests) == 0:
			results['success'] = False
			results['error_message'] = generate_message("request_join_activity","not_exist",{"activity":activity})
		else:
			activity_request = activity_requests[0]
			activity_request.delete()
			activity_cancel_record = ActivityCancelAttending.objects.create(activity=activity,user_cancel=user_login,reason=reason)

			confirmation_activities = ConfirmationActivity.objects.filter(activity=activity,user_confirm=user_login)
			for confirmation_activity in confirmation_activities:
				confirmation_activity.delete()

			context = {
	      		"user_send": get_user_common_info(user_login),
	 			"activity": get_activity_common_info(activity),
	 			"reason": reason,
	 			"url": WEBSITE_HOMEPAGE + "activity/" + activity.unique_id,
			}
			email_tracking = EmailTracking.objects.create(to_emails=activity.user_create.email,email_template="cancel_attending_activity",context_data=str(context))
			send_email.delay(email_tracking.id)

			results['success'] = True
			#results['email_tracking_id'] = email_tracking.id
			results['success_message'] = generate_message("cancel_request_join_activity","success",{"activity":activity})
			results['snippet_return'] = generate_html_snippet(request,"request_join_activity_button",{"activity":activity})
			results['action'] = "cancel_attending"

			# Generate notification for the activity host
			subj = { "main": user_login,"display": "user","related": None}
			action = "cancel_attending"
			obj = { "main": activity, "object_type": "activity", "display": "activity", "related": [activity.user_create] }
			extra_context = { "reason": reason }
			send_notification.delay(subj,action,obj,True,extra_context)
			#generate_notification(subj,action,obj,True,extra_context)
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})   
	return json.dumps({"results": results})



@dajaxice_register
def cancel_request_join_activity(request,unique_id):
	results = {}
	try:
		user_login = get_user_login_object(request)
		activity =  Activity.objects.get(unique_id=unique_id)
		activity_requests = ActivityRequest.objects.filter(activity=activity,user_request=user_login,request_status=REQUEST_WAITING)
		if len(activity_requests) == 0:
			results['success'] = False
			results['error_message'] = generate_message("request_join_activity","not_exist",{"activity":activity})
		else:
			activity_request = activity_requests[0]
			activity_request.delete()
			results['success'] = True
			results['success_message'] = generate_message("cancel_request_join_activity","success",{"activity":activity})
			results['action'] = "cancel_request"
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})   
	return json.dumps({"results": results})


@dajaxice_register
def watch_activity(request,unique_id):
	results = {}
	try:
		user_login = get_user_login_object(request)
		activity =  Activity.objects.get(unique_id=unique_id)
		user_login.get_profile().watchlist.add(activity)    
		activity.users_watch.add(user_login)
		activity.save()
		user_login.save()
		results['success'] = True
		results['watch_value'] = WATCHING
		results['num_watcher'] = activity.users_watch.count()
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})   
	return json.dumps({"results": results})


@dajaxice_register
def unwatch_activity(request,unique_id):
	results = {}
	try:
		user_login = get_user_login_object(request)
		activity =  Activity.objects.get(unique_id=unique_id)
		user_login.get_profile().watchlist.remove(activity) 
		activity.users_watch.remove(user_login)
		user_login.save()
		activity.save()
		results['success'] = True
		results['watch_value'] = NOT_WATCH
		results['num_watcher'] = activity.users_watch.count()
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})   
	return json.dumps({"results": results})


@dajaxice_register
def add_comment(request,activity_unique_id,comment_content):
	results = {}
	try:
		user_login = get_user_login_object(request)
		activity = Activity.objects.get(unique_id=activity_unique_id)
		new_comment = Comment.objects.create(comment_type='activity',content=comment_content,
											 user=user_login,unique_id=generate_unique_id("comment"))
		activity.comments.add(new_comment)
		activity.save()
		results['success'] = True
		results['snippet_return'] = generate_html_snippet(request,ACTIVITY_PAGE[COMMENT_ITEM],{"comment": new_comment}) 
		results['num_comment'] = activity.comments.count()

		# Generate Notification and Feeds
		subj = { "main": user_login,"display": "user","related": user_login.get_profile().followers.all()}
		action = "comment"
		obj = { "main": activity, "object_type": "activity", "display": "activity", "related": list(activity.get_watchers()) + list(activity.get_people_going())}
		send_notification.delay(subj,action,obj)
		#create_feed.delay(subj,action,obj)
		# generate_notification(subj,action,obj)
		# generate_feed(subj,action,obj)
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})   
	return json.dumps({"results": results})


@dajaxice_register
def edit_comment(request,comment_unique_id,comment_content):
	results = {}
	try:
		user_login = get_user_login_object(request)
		comment = Comment.objects.get(unique_id=comment_unique_id)
		comment.content = comment_content
		comment.edit_date = datetime.datetime.now()
		comment.save()
		results['success'] = True
		results['comment_unique_id'] = comment_unique_id
		results['snippet_return'] = generate_html_snippet(request,ACTIVITY_PAGE[COMMENT_ITEM],{"comment": comment})
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})
	return json.dumps({"results": results})


@dajaxice_register
def delete_comment(request,activity_unique_id,comment_unique_id):
	results = {}
	try:
		comment = Comment.objects.get(unique_id=comment_unique_id)
		activity = Activity.objects.get(unique_id=activity_unique_id)
		activity.comments.remove(comment)
		comment.delete()
		results['success'] = True
		results['comment_unique_id'] = comment_unique_id
		results['num_comment'] = activity.comments.count()
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})
	return json.dumps({"results": results})

@dajaxice_register
def report_comment(request,comment_unique_id,report_content):
	results = {}
	try:
		user_login= get_user_login_object(request)
		comment = Comment.objects.get(unique_id=comment_unique_id)
		report = Report.objects.create(report_type='comment',user_report=user_login,
									   report_content=report_content,unique_id=generate_unique_id('report'))
		comment.reports.add(report)
		comment.save()
		results['success'] = True
		results['success_message'] = generate_message("report","success",{})
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})
	return json.dumps({"results": results})


@dajaxice_register
def report_activity(request,activity_unique_id,report_content):
	results = {}
	try:
		user_login= get_user_login_object(request)
		activity = Activity.objects.get(unique_id=activity_unique_id)
		report = Report.objects.create(report_type='activity',user_report=user_login,
									   report_content=report_content,unique_id=generate_unique_id('report'))
		activity.reports.add(report)
		activity.save()
		results['success'] = True
		results['success_message'] = generate_message("report","success",{})
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})
	return json.dumps({"results": results})

@dajaxice_register
def check_confirmation_id(request,activity_unique_id,confirmation_id):
	results = {}
	try:
		activity = Activity.objects.get(unique_id=activity_unique_id)
		results['confirm_message'] = generate_message("confirmation","not_match",{})
		for i in activity.confirmations.all():
			if i.confirmaton_id == confirmation_id:
				if i.is_confirmed == "0":
					results['confirm_message'] = generate_message("activity_confirmation","success",{"username": i.user_confirm.username}) 
					i.is_confirmed = "1"
					i.save()
				else:
					results['confirm_message'] = generate_message("ticket_confirmed","already",{"username": i.user_confirm.username})
		results['success'] = True
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{})   
	return json.dumps({"results": results})

@dajaxice_register
def invite_guests_by_username(request,unique_id,content,usernames):
	results = {}
	try:
		user_login = get_user_login_object(request)
		activity = Activity.objects.get(unique_id=unique_id)
		guest_emails = []
		invitations = []
		for username in usernames:
			try:
				user = User.objects.get(username=username)
				email = user.email
				guest_emails.append(email)
				activity_requests = activity.activity_requests.filter(user_request=user)
				if len(activity_requests) == 0:
					activity_request = ActivityRequest.objects.create(activity=activity,user_request=user,request_status=REQUEST_INVITED)
					activity.activity_requests.add(activity_request)
					activity.save()
				else:
					activity_request = activity_requests[0]
					activity_request.request_status = REQUEST_INVITED
					activity_request.is_invite_request = True
					activity_request.save()
					if activity_request not in activity.activity_requests.all():
						activity.activity_requests.add(activity_request)
						activiy.save()
				invitation = Invitation(unique_id=generate_unique_id("invitation"),message=content,activity=activity,send_from=user_login,send_to_email=email)
				invitations.append(invitation)

				subj = { "main": user_login,"display": "host","related": None}
				action = "invite"
				obj = { "main": activity, "object_type": "activity", "display": "activity", "related": [user]}
				send_notification.delay(subj,action,obj,True)
				#generate_notification(subj,action,obj,True)
			except User.DoesNotExist:
				pass

		Invitation.objects.bulk_create(invitations)
		context = {
			"url": WEBSITE_HOMEPAGE + "activity/" + unique_id,
			"user": get_user_common_info(user_login),
			"activity": get_activity_common_info(activity),
			"invitation_message": content
		}
		email_tracking = EmailTracking.objects.create(to_emails=",".join(guest_emails),email_template="invite_activity",context_data=str(context))
		send_email.delay(email_tracking.id)

		results['success'] = True
		#results['email_tracking_id'] = email_tracking.id
		results['success_message'] =  generate_message("send_invitation","success",{}) 
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{}) 
	return json.dumps({"results": results})


@dajaxice_register
def invite_guests_by_email(request,unique_id,content,emails):
	results = {}
	try:
		user_login = get_user_login_object(request)
		activity = Activity.objects.get(unique_id=unique_id)
		guest_emails = []
		invitations = []
		context = {
			"user": get_user_common_info(user_login),
			"activity": get_activity_common_info(activity),
			"invitation_message": content
		}
		for email in emails:
			if email != activity.user_create.email and validate_email(email):
				guest_emails.append(email)
				invitation = Invitation(unique_id=generate_unique_id("invitation"),message=content,activity=activity,send_from=user_login,send_to_email=email)
				invitations.append(invitation)
				
				try:
					user = User.objects.get(email=email)
					activity_requests = activity.activity_requests.filter(user_request=user)
					context['url'] = WEBSITE_HOMEPAGE + "activity/" + unique_id
					if len(activity_requests) == 0:
						activity_request = ActivityRequest.objects.create(activity=activity,user_request=user,request_status=REQUEST_INVITED)
						activity.activity_requests.add(activity_request)
						activity.save()
					else:
						activity_request = activity_requests[0]
						activity_request.request_status = REQUEST_INVITED
						activity_request.is_invite_request = True
						activity_request.save()
						if activity_request not in activity.activity_requests.all():
							activity.activity_requests.add(activity_request)
							activiy.save()
							
					subj = { "main": user_login,"display": "host","related": None}
					action = "invite"
					obj = { "main": activity, "object_type": "activity", "display": "activity", "related": [user]}
					send_notification.delay(subj,action,obj,True)
					#generate_notification(subj,action,obj,True)
				except User.DoesNotExist:
					data_token = {'key':email,'unique_id':unique_id }
					options_token = {"expires": today + datetime.timedelta(days=15)}
					token = generate_token(data_token,options_token)
					new_token = GeneralToken.objects.create(key=email,token=token,token_type="activity")
					activity.tokens.add(new_token)
					activity.save()
					context['url'] = WEBSITE_HOMEPAGE + "activity/" + unique_id + "?token=" + token		
				
		Invitation.objects.bulk_create(invitations)
		email_tracking = EmailTracking.objects.create(to_emails=",".join(guest_emails),email_template="invite_activity",context_data=str(context))
		send_email.delay(email_tracking.id)

		results['success'] = True
		#results['email_tracking_id'] = email_tracking.id
		results['success_message'] =  generate_message("send_invitation","success",{}) 
	except Exception as e:
		logger.exception(e)
		results['success'] = False
		results['error_message'] = generate_message("ajax_request","fail",{}) 
	return json.dumps({"results": results})






