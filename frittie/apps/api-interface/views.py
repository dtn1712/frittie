from django.contrib.auth.decorators import login_required
from frittie.apps.app_helper import convert_time, get_user_login_object
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext

@login_required
def show_interface(request):
   
    user_login = get_user_login_object(request)
    if "error" in request.GET:
        return render_to_response(
                "apps/location/page/edit_error.html",
                {
                    "user_login": user_login,
                },
                context_instance=RequestContext(request)
            )
    
    return render_to_response(
                    "apps/api/api_interface.html", 
                    {
                        
                    }, 
                    context_instance=RequestContext(request))
