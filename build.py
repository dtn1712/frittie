import sys, os

STAGE_NAME = {"dev":None, "prod": None, "beta": None}

if len(sys.argv) < 2 or sys.argv[1] not in STAGE_NAME:
	valid_stage = str(STAGE_NAME.keys()).replace("[","").replace("]","")
	print "Usage: python " + __file__ + " argv[1]" 
	print "argv[1] can be one of the following: " + valid_stage
	sys.exit()

PROJECT_ROOT = os.path.dirname(__file__)
PROJECT_NAME = "frittie"
PROJECT_SCRIPT = "scripts"
PROJECT_PATH = os.path.join(PROJECT_ROOT, PROJECT_NAME, PROJECT_SCRIPT)

argument = " ".join(sys.argv) 
scripts = os.listdir(PROJECT_PATH)
scripts.sort()

for script in scripts:
	cmd = "python " + PROJECT_PATH + "/" + script + argument[argument.find(" "):]
	os.system(cmd)

